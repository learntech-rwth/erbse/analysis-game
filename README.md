# All sorts of Phish (Analysis Game)

The game is automatically deployed to https://lernspiele.informatik.rwth-aachen.de/games/analysis-game/

## Gameplay 

All sorts of Phish is focussed on explaining different types of phishing URLs. These different types are introduced in short tutorials followed by time-based challenge rounds where the player has to sort URLs into the different categories.

## MTLG-Framework 

The game was created using the MTLG-Framework (Multitouch-Learning-Game-Framework). It was developed as an aid to create platform-independent educational games using JavaScript with support for large Multitouch-Displays. The framework can be found [here](https://gitlab.com/mtlg-framework/mtlg-gameframe). 

## Building the game 

A tutorial on how to build the game yourself from this repository can be found [here](https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/mtlg/installation).
