
MTLG.lang.define({
  'en': {
    'back' : 'Back',
    'backToMenu' : 'Back to Menu',
    'backToMainMenu' : 'Back to Mainmenu',
    'containerCount' : 'Number of containers',
    'content' : 'content',
    'continue' : 'Continue',
    'difficulty' : 'Difficulty',
    'easy' : 'Easy',
  'repeatLevel': "Repeat level",
    'end_1' : 'This was: All sorts of Phish\n\n Thank you for participating in our study! Please return to the questionnaire now and answer the remaining questions. \n\n Thank you and good luck!',
    'end_2' : '\n ',
    'end_3' : '\n ',
    'endIntro' : 'End Intro',
    'endIntro_1' : 'Get read! The big fight is about to begin!',
    'endIntro_2' : 'This time you will compete against the hardest and best filter ever! You must win. Otherwise ' +
        'the human kind is lost!',
    'endIntro_3' : 'But if you win, the age coined by fear and terror is over and the people will be more confident ' +
        'dealing with phishing-URLs. Everybody counts on you! If anyone can do, it is you!',
    'endIntro_4' : 'Are you ready? Then, let´s go! Go in and win! Good luck!',
    'endLevel' : 'Endlevel',
    'english' : 'English',
      'evaluation_1_lost' : 'What a pity!\n\nYou did not reach my score.',
      'evaluation_1_lost_percentage' : 'What a pity!\n\nYou beat my score, but you also made to many mistakes.',
      'evaluation_1_won' : 'Congratulations!\n\nYou were able to beat my score.',
      'evaluation_2' : 'You have got',
      'evaluation_3' : 'points.',
      'evaluation_4_lost' : 'Therefore you unfortunately lost against the filter!',
      'evaluation_4_won' : 'Therefore you won against the filter!',
      'evaluation_5' : 'Let us see in detail, what you have done.',
      'evaluation_6' : 'You have assigned this URL of type',
      'evaluation_6_nothing_right' : 'What a pity! You have not assigned any URL correctly.',
      'evaluation_6_nothing_wrong' : 'Well done! You did not make any mistake.',
      'evaluation_7' : 'to the container of type',
      'evaluation_8' : '. This is absolutely right. Well done!',
      'evaluation_9_1' : 'You have recognized correctly, that this URL is no phishing at all and that it transfers you to the right server.',
    'evaluation_9_2' : 'Thereby you did notice, that it was about a randomly composed URL.',
    'evaluation _9_3' : 'Thereby you have recognized correctly, that the host-part is composed of an IP address.',
      'evaluation_9_4' : 'Thereby you did not fall for the deceptive terms in the path-part.',
      'evaluation_9_5' : 'Thereby you did not fall for the deceptive subdomains.',
      'evaluation_9_6' : 'Thereby you did not fall for the domain, which sounds similar to the original.',
      'evaluation_10' : '. Unfortunately this is wrong. What a pity!',
      'evaluation_11_1' : 'Look at the host-part again. The URL transfers you to the right server. Therefore no phishing!',
    'evaluation_11_2' : 'Pay attention, if the URL makes sense or if it is only randomly composed, like here.',
    'evaluation_11_3' : 'Have a look, what the host-part is composed of. If it contains an IP address, that does not make clear where it transfers you.',
      'evaluation_11_4' : 'Have a detailed look where the host-part and where the path-part is. Only the host-part identifies the server!',
      'evaluation_11_5' : 'Pay attention to all subdomains! The later the subdomain appears in the URL, the more important it is.',
      'evaluation_11_6' : 'Look exactly at the URL again. The host-part just looks like the one in the original URL.',
      'evaluation_12' : 'In total, you have assigned ',
      'evaluation_13' : '% of all URLs correctly,',
      'evaluation_14' : '% of all URLs with the right tendency and',
      'evaluation_15' : '% of all URLs incorrectly.',
      'evaluation_16' : 'We need to practise a little more. Let´s go, give it another try!',
      'evaluation_17' : 'I think you are ready for the next step. Come, let´s see, what we will get next!',
    'evaluation_18' : 'We are already practicing for quite a while. You could need some more practice, but you have gained ' +
        'enough experience to go to the next step. Are you ready? Then click on Skip! Otherwise just click on Continue ' +
        'to repeat the current level again.',
    'evaluationLevel' : 'Evaluation',
    'filter' : 'Phil\'s Score',
    'filterPoints' : 'Points the filter will get',
    'final_lost_1' : 'Oh no, oh no! I have lost the big fight against the filter.',
    'final_lost_2' : 'But how could that happen? You were so good...',
    'final_lost_3' : 'The human kind is right before total chaos!',
    'final_lost_4' : 'I´m so sorry. I was just a bit nervous, but i can do better!',
    'final_lost_5' : 'We spend too much time talking. Go and try to beat the filter again, while I am going to calm the people.',
    'final_won_1' : 'John, John! I have done it! I have won the big fight and beaten the filter.',
    'final_won_2' : 'Great! Congratulations! You have really reached the status of an expert in phishing.',
    'final_won_3' : 'And apropos of nothing, you have saved the human kind from a tragedy. You are a true hero!',
    'final_won_4' : 'Thank you John! Without you I would have neven been able to achieve this.',
    'final_won_5' : 'Enough talk. Let\'s celebrate...',
    'gameName' : 'All sorts of Phish',
    'german' : 'German',
    'hard' : 'Hard',
    'infoText_0' : '"No-Phish"\n\nThese URLs are benign and legitimate.',
    'infoText_1' : '"Random"\n\nThese URLs are entirely random and arbitrary.',
    'infoText_2' : '"IP"\n\n These URLs use an IP address as host to confuse users.',
    'infoText_3' : '"Path"\n\nThese URLs have an arbitrary host and try to deceive you with a trustworthy looking path.',
    'infoText_5' : '"Subdomain"\n\nThese URLs use a long list of subdomains to confuse you.',
    'infoText_4' : '"RegDomain"\n\nThese URLs have a host part very similar to the original host.',
    'intro1' : 'Hey you!',
    'intro2' : 'Yes! Exactly you!',
    'intro3' : 'Me?',
    'intro4' : 'You behind the screen! I`ve been waiting for you.',
    'intro5' : 'Finally you`re here... You are the chosen one!',
    'intro6' : 'The chosen one? There must be a mistake.',
    'intro7' : 'Quit joking now! The world is faced with ruin! The time for jokes is over.',
    'intro8' : 'Ruin? What happened?',
    'intro9' : 'The people are confused. They can not discern, if a URL is Phishing or not',
    'intro10' : 'And what exactly is my task now?',
    'intro11' : 'They need someone who leads them! One, who can say them, what is Phishing and what not.',
    'intro12' : 'And this someone is me?',
    'intro13' : 'Yes exactly.',
    'intro14' : 'But i have also no clue about Phishing at all...',
    'intro15' : 'And therefore we can not loose time. Don\'t worry! I am going to teach you all you need to know.',
    'intro16' : 'Who exactly are you?',
    'intro17' : 'I am John, your Phishing-Coach. I am going to train you as an expert, so that you can win the big fight.',
    'intro18' : 'The big fight?',
    'intro19' : 'Everything at the proper time... Just come with me.',
    'intro20' : 'But... but..., where am I?',
    'intro21' : 'At the training arena! We are going to practice here. I will explain it to you...',

    // Tutorial Section 1 - Basics, No-Phish + Random
    // Basics
    'introduction_1_0' : 'Welcome! My name is Phil.\n\n In this game you will learn to distinguish phishing URLs from benign URLs.',
    'introduction_1_0.5': 'After finishing the game you should be able to check URLs in e.g. emails or social networks before clicking on them.\n\nAlso, when entering sensitive data, you should always check the URL of the current website.',
    'introduction_1_1' : 'In the course of the game you will get to know a total of six types of URLs.\n\nAmong them are five different types of phishing URLs that will try to mislead you. \n\nThe sixth type ' +
        'is called "No-Phish". It includes all legitimate URLs that will take you to a benign website.',
    'introduction_1_1.5' : 'Let\'s first look at the structure of a URL (also called internet address).\n\nMove your cursor over a shown URL to discover its components.',
    'introduction_1_2' : 'A URL consists of three parts: \n Scheme, host and path.\n\n' +
        'The host starts after the double slash (//) and ends with the first single slash (/).\n\n The path extends up to the end of the URL',
    'introduction_1_2_2': "URLs always start with a scheme.\n\n"+
        "For websites, this is typically HTTPS or HTTP, though this game only uses HTTPS.\n\n",
    'introduction_1_2_3':"Websites that do not use HTTPS do not encrypt your input. As such, you should generally avoid entering your personal information on websites using HTTP.\n\n"+
        "Furthermore, note that your browser might not always show you the scheme, though we will always include it in this game.",
    'introduction_1_3' : 'The host part specifies the address of the web server on which the web page is offered. ' +
        '\n\n Essentially, an IP address is hidden behind the host part, but it  ' +
        'is usually replaced by a more understandable string for our daily usage.',
    'introduction_1_3.5' : 'An IP address consists of four numbers (between 0 and 255) separated by dots.\n\n You should be especially careful with such URLs, as you don\'t know which website it leads you to.',
    'introduction_1_4' : 'The host consists of a number of different domains, which specify areas '+
        'of the Internet.\n\n The further right a domain is in the host, the more important it is.\n\n '+
        'At the very end of the host is the Top-Level Domain (TLD), e.g. \".com\" or \".de\".',
    'introduction_1_4_5': 'As I already told you, the last domain of the host is called Top-Level Domain (TLD).\nThe two last domains together are called "Registrable Domain" (RD).\n\n'+
        'The registrable domain is the most important part of the host, because it has to be unique.\n' +
        'As such, phishers cannot choose an existing registrable domain (e.g., amazon.de), and have to resort to other tricks.',
    'introduction_1_4_10': "Special characters (e.g. ?, +, %, !)  other than hyphens [-] are not allowed in domains!\n"+
        "This also includes the Slash [/], which marks the end of the host part.\n"+
        "There is no differentiation between uppercase and lowercase letters in domains.",
    // No-Phish
    'introduction_1_5' : 'Let us first take a closer look at URLs of the type "No-Phish".\n\n You can recognize them by the fact that the domain to the left of the top-level domain corresponds to the address of the desired destination.',
    // Random
    'introduction_1_6' : 'Next, let\'s look at the first phishing type: "Random".\n\n It contains all apparently random URLs where no connection to a specific target can be recognized. \n\n If you see such a URL, you should be careful!',
    'introduction_1_6.5' : 'Another type of Phishing URL is "IP address".\n\n It includes all URLs that use an IP address in the host part instead of a string of letters and numbers.',
    'introduction_1_7' : 'That was quite a lot of information at once, right? \n\n But do not worry, it is not as complicated as it sounds. You can do it!',
    'introduction_1_8' : 'In the following level you are going to practice distinguishing URLs.\n\n For this you have to put coins into the right barrels. Each coin shows a URL when you click on it.\n\n Can you manage to beat my score?',

    //Tutorial Section 2 - Regdomain
    'introduction_2_0' : 'Good job, let us continue! In this level you will meet another phishing type.',
    'introduction_2_1' : 'The next phishing type is called "RegDomain".\n\n Here phishers use a URL that is very similar to the original URL and often differs only in one word or sometimes just one letter from the original.',
    // RegDomain
    'introduction_2_2' : 'Most of the time phishers add terms to the original URL which suggest a sense of security.\n\nThe URL often looks deceptively real, but leads to a completely different website.',
    'introduction_2_2_5': 'Small changes to the URL can also be very dangerous if you don\'t pay attention!\n\nIn this case one character is removed, replaced, added or swapped with another character.',
    'introduction_2_2_10': 'Alternatively phishers can change the TLD (Top-Level Domain) if the new registrable domain is still available!',
    'introduction_2_3' : 'To recognise this type of website, you need to pay a lot of attention!',
    'introduction_2_4' : 'A tip for outside the game:\n\nIf you are unsure whether a URL is malicious ' +
        'or legitimate, use a search engine to find the allegedly related website! In most ' +
        'cases the correct web page will be shown as the first result and you can compare the URLs.',
    'introduction_2_5' : 'But in this game you will not have the time to search and will have to decide quickly! \n\n '+
        'Click "Continue" when you are ready to practice with me and think you can beat me!',


  // Section 3 - Subdomain
    // subdomain
    'introduction_3_0' : 'Oh, you beat me again! You are really getting better and better. \n\n You are definitely ready for the next type.',
    'introduction_3_1' : 'The name of this type is "Subdomain". \n\n In the host part a series of domains can be strung together. ' +
        'The subordinate domains are called subdomains. \nAnd they can also be abused by phishers to deceive you!',
    'introduction_3_2': 'Take a closer look at this example. \n\nDoes not seem to dangerous at first glance ' +
        ', right?',
    'introduction_3_2.5': 'At first you might think we are redirected to the official site of Apple.\n\n ' +
        'But the last part before the TLD is decisive. So in reality, we will end up on tayawldma.com. ' +
        '\n\nThis does not sound like Apple!',
    'introduction_3_3' : 'So pay attention where the host part really ends and to which server you are ' +
        'forwarded. \n\nPhishers often use this technique on mobile devices with a small screen. ' +
        'This way you only see the beginning of the URL in the browser and not the true identity behind the URL.',
    'introduction_3_4' : 'Now you already know the first five types.\nI hope you still remember everything! \n\nLet\'s compete against each other again!',

    // Tutorial Section 4 - Path
    'introduction_4_0' : 'Do you notice how you are slowly becoming better and better at recognising phishing URLs? \n\n You are already ' +
        'really good, but there is more to learn! \n\n One last type is still missing. ',
    // Path
    'introduction_4_1' : 'It is called a path and ' +
        'uses a misleading path to deceive. \n\n Don\'t worry! With a little practice, it\'s no problem to recognise this type.',
    'introduction_4_2' : 'The path given is the exact ' +
        'location of the requested website. \n\nSometimes the path also contains certain parameters.',
    'introduction_4_3' : 'But remember the most important part: \n\nThe address in the host part is crucial and determines the wesbite a URL leads to! ' +
        'So your focus should always be on this part of the URL!',
    'introduction_4_4' : 'The path part contains terms that refer to a legitimate ' +
        'URL, eBay in this case. \n\n But the host part seems completely arbitrary and has nothing to do with ' +
        'eBay. \n\nThis is a trick to deceive inattentive users!',
    'introduction_4_5' : 'You almost did it! \n\nNow you know all the phishing types.\n\n Can you beat me one last time?',

    // Evaluation texts

    // No-Phish
    'eval_nophish_correct': 'You correctly decided that this URL belongs to the type "No-Phish". Well done!\n\n'
        +' It really leads to the given target.',
    'eval_nophish_wrong': 'You assumed this URL was dangerous, but it really leads to the given target.\n\nThat is too bad! Take a look at the registrable domain again!',
    'eval_nophish_skip': 'This URL was not dangerous so you could have trusted yourself and sorted it into "No-Phish"!',

    // Random
    'eval_random_correct': 'You associated this URL correctly with the type "Random". It does not contain any reference to the target and consists solely of random parts. Well done!',
    'eval_random_wrong': 'Take a look at this URL again. It does not lead to the given target nor does it have any relation to it. This means that it is actually an URL of the type "Random"!',
    'eval_random_skip': 'Sadly you skipped this URL. In this case, the URL was of the type "Random". You can recognize this by taking a look at the URL and recognizing that no relation exists between the URL and the given target.',

    // IP
    'eval_ip_correct': 'Great, you correctly assigned this URL to the type "IP address"! You were attentive and noticed that the host-part was replaced by an IP address.',
    'eval_ip_wrong': 'You should have taken a closer look at this URL. Instead of having a host-part consisting of domains, it starts with an IP address. This is a clear indicator, that this could be a URL of the type "IP address". ',
    'eval_ip_skip': 'You could have trusted yourself and assigned this URL to the type "IP address" since the host part was replaced by an IP address.',

    // Path
    'eval_path_correct': 'Well done! You sorted this URL correctly. The deception is placed in the path.',
    'eval_path_wrong': 'This URL is actually of type "Path". You can see this from the fact that the host part does not refer to the given target. Instead the path is used to deceive the user',
    'eval_path_skip': 'You could have recognized the type of this URL by the fact that the host part does not belong to the given target, but at the same time the target is contained in the path. Pay attention to this next time!',
    // subdomain
    'eval_subdomain_correct': 'Perfect, you have assigned this URL to the type "Subdomain"! The subdomains wanted to lead you astray, but you saw through the deception!',
    'eval_subdomain_wrong': 'You must be more careful here. In this case the subdomain was modified to deceive you. You have to assign such URLs to the type "Subdomain" in the future',
    'eval_subdomain_skip': 'If you see a URL of this type again, pay more attention to the subdomains. If you recognize the given target in the subdomains and the host part leads you to another page, then it belongs to the type "Subdomain"!',
    // RegDomain
    'eval_regdomain_correct': 'Well done, recognizing a modified URL of the type "RegDomain" is not that easy. But you correctly recognized and assigned it to the type "RegDomain". Great!',
    'eval_regdomain_wrong': 'Pity, you made a mistake here. The registrable domain of the URL has been modified. Therefore this URL should have been assigned to the type "RegDomain"!',
    'eval_regdomain_skip': 'You skipped this URL. In the future look at the host part more closely, hopefully see the changes in the registrable domain!',


    'introduction_1Level' : 'Introduction 1',
    'introduction_2Level' : 'Introduction 2',
    'introduction_3Level' : 'Introduction 3',
    'introLevel_1' : 'Before you can begin with exercising, you must know, how the game works. First, let us see the ' +
        'playing field in detail. The golden coin is a URL-element, which contains the name of the website and the ' +
        'allegedly corresponding service\'s name. Currently, the content is still concealed. To open the URL, click it! Give it a try.',
    'introLevel_1_fail' : 'Click on the URL to open it',
    'introLevel_2' : 'Great! Now close the URL again with another click.',
    'introLevel_2_fail' : 'Click on the opened URL to close it.',
    'introLevel_3' : 'Really easy, isn´t it? Let us go ahead. You can see three wooden barrels, on which a classification type' +
        'is written. These are the URL-containers, in which you need to "throw" the right coins. The classification shows, ' +
        'which URL-type you should drop here. Let´s give it a try! Open the URL again.',
    'introLevel_3_fail' : 'Click on the coin to open the URL.',
    'introLevel_4' : 'Here we have to deal with a URL of type "No-Phish". To deposit a URL in a container, ' +
        'press and hold the URL and drag it into the container. Release the URL, if it is above the container. ' +
        'Now try to allocate the URL of type "No-Phish" to the container with the classification of type "No-Phish".',
    'introLevel_4_fail' : 'Please follow the instructions and lay down the URL in the container of type "No-Phish"!',
    'introLevel_5' : 'This already workes fine! You will see a smiley every time you deposit a URL in a container, ' +
        'which shows you, if your allocation was correct or not. For a rightly allocated "No-Phish"-URL you get 200 Points, ' +
        'for a rightly allocated Phishing-URL you get as much as 500 points! Moreover, every time ' +
        'you deposit a URL in a container, a new URL will appear on the playing field. Now open the new URL.',
    'introLevel_5_fail' : 'Click on the new URL, to open it.',
    'introLevel_6' : 'This is a URL of type "IP". This time, try to deposit it in the container of type "Random".',
    'introLevel_6_fail' : 'Please follow the instructions and deposit this URL in the container of type "Random".',
    'introLevel_7' : 'What happened now? You have allocated a phishing-URL to another phishing-type. Though this is ' +
        'not completely correct, you at least recognized, that this was a malicious URL. Therefore you don´t ' +
        'get the whole 500 points, but at least you get 200 points and you see the yellow smiley. Next, open the new URL.',
    'introLevel_7_fail' : 'Click on the new URL to open it.',
    'introLevel_8' : 'This is a URL of type "Random". Try to deposit this URL in the container of type "No-Phish".',
    'introLevel_8_fail' : 'Please follow the instructions and deposit this URL in the container of type "No-Phish".',
    'introLevel_9' : 'Oh dear! This should not happen to you in the game. You just allocated a URL completely wrong ' +
        'and therefore get 0 points. This shows you the red smiley. But don´t worry if such a thing happens to you while you play, ' +
        'you are here to learn, after all! Should it happen, that you forget, which URLs a specific type contains, you can ' +
        'get additional info about it again. Try this now and click on the "i" in the left bottom corner of the container of type "No-Phish".',
      'introLevel_9_fail' : 'Click on the "i" in the left bottom corner of the container of type "No-Phish"!',
      'introLevel_10' : 'Now the info of type "No-Phish" is displayed. During the running game you can display as much infos of ' +
          'containers as you want. While the information is displayed, the game is paused and you can read calmly trough the ' +
          'text. As soon as you have closed all infos, the game goes on and the timer starts to run again. Now click on ' +
          'the "i" again, to close the info.',
      'introLevel_10_fail' : 'Click on the "i" again, to close the info!',
    'introLevel_11' : 'To conclude, your goal is to achieve as many points as possible, to be better than your opponent, the ' +
        'filter. You can always see your currently achieved points on the top left, whereas the points you need to ' +
        'defeat the filter are displayed on the right. You have to achieve more points than the filter. In the case of a draw, you loose. ' +
        'To achieve your goal, you only get a limited amount of time. In the top middle you see a timer, which counts down during the ' +
        'game. If it reaches 0, the level is over and you will see an evaluation. Now click on Continue.',
      'introLevel_11_fail' : 'Click on Continue!',
    'introLevel_12' : 'At the beginning of the game there are only 3 URL-types available. To unlock the other types ' +
        'and reach the big fight, you need to accomplish a few practice iterations per round. How many exactly, ' +
        'depends on how good you perform in the practice iterations. The better you perform, the faster you will get ' +
        'to the next round. If you are ready to play the first round, click on Continue.',
    'introLevel_12_fail' : 'Click on Continue!',
    'language' : 'Language',
    'level1' : 'Level One',
    'options' : 'Options',
    'skip' : 'No\nidea',
    'InfoText_Skip' : 'An URL that is skipped, will not be counted',
    'InfoText_Skip_fail' : 'An URL that is skipped, will not be counted',
    'skipIntro' : 'Skip Intro',
    'startGame' : 'Start Game',
    'startNewGame' : 'Start New Game',
    'resumeGame' : 'Resume Game',
    'you' : 'Points',
    'no-Phish':'No-Phish',
    'random':'Random',
    'ip':'IP address',
    'path':'Path',
    'subDomain':'Subdomain',
    'regDomain':'RegDomain',
    // Tutorial URL parts
    'url-scheme': "Scheme",
    'url-rd': "Registrable Domain",
    'url-host': "Host",
    'url-domain': 'Domain',
    'url-subdomain': "Subdomain",
    'url-path': "Path",
    'url-query': "Query",
    'url-fragment': "Fragment",
    'url-tld': "Top-Level Domain (TLD)",
    'url-etld': "effective Top-Level Domain (eTLD)",
    'url-questionMark': "?",
    'copy-button': "Copy",
    'copy-button-success': "Copied!",
    'playerid' : "Player-ID",
    'personalization': "Game personalization",
    'enabled': "Enabled",
    'disabled': "Disabled"
  },
  'de': {
    'back' : 'Zurück',
    'backToMenu' : 'Zum Menü',
    'backToMainMenu' : 'Zum Hauptmenü',
    'containerCount' : 'Anzahl an Behältern',
    'content' : 'Inhalt',
    'continue' : 'Weiter',
    'difficulty' : 'Schwierigkeitsgrad',
    'easy' : 'Einfach',
    'end_1' : 'Das war: All sorts of Phish\n\n Danke für deine Teilnahme an unserer Studie!\n\nKehre jetzt bitte zurück zum Fragebogen und beantworte die verbleibenden Fragen. \n\nVielen Dank und viel Erfolg!',
    'end_2' : '\n ',
    'end_3' : '\n ',
    'endIntro' : 'Ende Intro',
    'endIntro_1' : 'Jetzt ist es soweit! Der große Kampf steht an!',
    'endIntro_2' : 'Dieses Mal trittst du gegen den härtesten und besten Filter an, der jemals gesehen wurde! Du musst ' +
        'gewinnen. Ansonsten ist die Menschheit verloren!',
    'endIntro_3' : 'Wenn du aber gewinnst, dann hat die Zeit geprägt von Angst und Schrecken ein Ende und die Menschen ' +
        'werden im Umgang mit Phishing-URLs endlich wieder sicherer. Alles zählt auf dich! Wenn es jemand schaffen kann, ' +
        'dann du!',
    'endIntro_4' : 'Bist du bereit? Dann los! Auf in den Kampf! Viel Glück!',
    'endLevel' : 'Endlevel',
    'english' : 'Englisch',
      'evaluation_1_lost' : 'Schade!\n\nDu hast meinen Score leider nicht übertroffen.',
      'evaluation_1_lost_percentage' : 'Schade!\n\nDu hast zwar meinen Score geschlagen, aber trotzdem zu viele Fehler gemacht.',
      'evaluation_1_won' : 'Herzlichen Glückwunsch!\n\nDu hast meinen Score geschlagen.',
      'evaluation_2' : 'Du hast',
      'evaluation_3' : 'Punkte erzielt.',
      'evaluation_4_lost' : 'Damit hast du leider gegen den Filter verloren!',
      'evaluation_4_won' : 'Damit hast du gegen den Filter gewonnen!',
      'evaluation_5' : 'Lass uns mal im Detail schauen, was du gemacht hast.',
      'evaluation_6' : 'Du hast diese URL vom Typ',
      'evaluation_6_nothing_right' : 'Schade! Du hast leider keine URL richtig zugewiesen.',
      'evaluation_6_nothing_wrong' : 'Sehr gut! Du hast dieses Mal keinen Fehler gemacht.',
      'evaluation_7' : 'dem Container vom Typ',
      'evaluation_8' : 'zugewiesen. Das ist absolut richtig. Super!',
    'evaluation_9_1' : 'Du hast richtig erkannt, dass diese URL kein Phishing ist und sie dich auf den richtigen Webserver leitet.',
    'evaluation_9_2' : 'Dabei ist dir aufgefallen, dass es sich um eine zufällig zusammengestellte URL handelt',
    'evaluation_9_3' : 'Dabei hast du richtig erkannt, dass der Host-Teil aus einer IP-Adresse besteht.',
      'evaluation_9_4' : 'Dabei bist du nicht auf die irreführenden Begriffe im Pfad-Teil reingefallen.',
      'evaluation_9_5' : 'Dabei bist du nicht auf die irreführenden Subdomains reingefallen',
      'evaluation_9_6' : 'Dabei bist du nicht auf eine, ähnlich wie das Original klingende, Domain reingefallen',
      'evaluation_10' : 'zugewiesen. Das ist leider falsch. Schade!',
      'evaluation_11_1' : 'Schau dir den Host-Teil nochmal genau an. Die URL leitet dich auf den richtigen Webserver weiter. Also kein Phishing!',
    'evaluation_11_2' : 'Achte darauf, ob die URL Sinn ergibt oder nur zufällig zusammengewürfelt ist, wie hier.',
    'evaluation_11_3' : 'Schau dir nochmal genau an, woraus der Host-Teil besteht. Er besteht aus einer IP-Adresse, bei der nicht klar wird, wohin sie dich genau leitet.',
      'evaluation_11_4' : 'Schaue genau hin, wo sich der Host- und wo der Pfad-Teil befindet. Nur der Host-Teil entscheidet darüber, auf welchem Webserver du landest!',
      'evaluation_11_5' : 'Achte auf alle Subdomains! Je weiter hinten die Subdomain in der URL steht, desto größer ist ihre Bedeutung.',
      'evaluation_11_6' : 'Schaue dir die URL nochmal genau an. Der Host-Teil sieht nur so ähnlich aus, wie bei der originallen URL.',
      'evaluation_12' : 'Insgesamt hast du ',
      'evaluation_13' : '% aller URLs richtig zugewiesen,',
      'evaluation_14' : '% aller URLs mit der richtigen Tendenz zugewiesen und',
      'evaluation_15' : '% aller URLs falsch zugewiesen.',
      'evaluation_16' : 'Wir müssen noch ein wenig üben. Los, versuch es gleich nochmal!',
      'evaluation_17' : 'Ich glaube du bist nun bereit für den nächsten Schritt. Komm, sehen wir uns an, was als nächstes kommt!',
    'evaluation_18' : 'Wir üben jetzt schon seit einer ganzen Weile. Du könntest noch etwas Übung gebrauchen, hast aber ' +
        'schon genug Erfahrung gesammelt, um den nächsten Schritt zu machen. Bist du bereit? Dann klicke auf Überspringen! ' +
        'Ansonsten klicke einfach auf Weiter und mache das Level nochmal.',
    'evaluationLevel' : 'Evaluation',
    'filter' : 'Phil\'s Punkte',
    'filterPoints' : 'Punktzahl des Filters',
    'final_lost_1' : 'Oh nein, oh nein! Ich habe den großen Kampf gegen den Filter verloren.',
    'final_lost_2' : 'Aber wie konnte das passieren? Du warst doch so gut...',
    'final_lost_3' : 'Die Menschheit steht kurz vor dem totalen Chaos!',
    'final_lost_4' : 'Es tut mir so leid. Ich war einfach etwas nervös, aber ich kann das besser!',
    'final_lost_5' : 'Wir verlieren zu viel Zeit mit quatschen. Geh du und versuche es nochmal, ich beruhige währenddessen ' +
        'die Leute!',
    'final_won_1' : 'John, John! Ich habe es geschafft! Ich habe den großen Kampf gewonnen und den Filter geschlagen.',
    'final_won_2' : 'Klasse! Herzlichen Glückwunsch! Du hast beim Phishing wirklich einen wahren Expertenstatus erreicht.',
    'final_won_3' : 'Und ganz nebenbei hast du noch die Menschheit vor einer Tragödie bewahrt. Du gehörst zu den Helden!',
    'final_won_4' : 'Danke John! Ohne dich hätte ich das niemals geschafft.',
    'final_won_5' : 'Genug der Worte jetzt. Lass uns feiern gehen....',
    'gameName' : 'All sorts of Phish',
    'german' : 'Deutsch',
    'hard' : 'Schwer',
    'infoText_0' : '"No-Phish"\n\nDiese URLs sind gutartig und legitim.',
    'infoText_1' : '"Random"\n\nDiese URLs sind vollkommen willkürlich und zufällig.',
    'infoText_2' : '"IP"\n\n Diese URLs nutzen als Host eine IP-Adresse um Nutzer zu verwirren.',
    'infoText_3' : '"Pfad"\n\nDiese URLs besitzen einen zufälligen Host-Teil ' +
        'und versuchen dich mit einem legitim aussehendem Pfad zu verwirren.',
    'infoText_5' : '"Subdomain"\n\nDiese URLs nutzen eine lange Kette von Subdomains zur Verwirrung.',
    'infoText_4' : '"RegDomain"\n\nDiese URLs besitzen einen Host-Teil, der einem gutartigen Host sehr ähnlich ist.',

    // Old intro text
    'intro1' : 'Hey du!',
    'intro2' : 'Ja! Genau du!',
    'intro3' : 'Wer ich?',
    'intro4' : 'Du hinterm Bildschirm! Ich habe dich erwartet.',
    'intro5' : 'Endlich bist du da... Du bist AUSERWÄHLT!',
    'intro6' : 'Auserwählt? Da muss eine Verwechslung vorliegen.',
    'intro7' : 'Mach jetzt ja keine Witze! Die Welt steht vor dem Untergang! Die Zeit für Späße ist vorbei.',
    'intro8' : 'Untergang? Was ist denn passiert?',
    'intro9' : 'Die Menschen sind verwirrt. Sie können nicht sicher unterscheiden, ob es sich bei einer URL um Phishing handelt oder nicht',
    'intro10' : 'Und was genau ist jetzt meine Aufgabe?',
    'intro11' : 'Sie brauchen jemanden, der sie anführt! Einer, der Ihnen sagt, was Phishing ist und was nicht.',
    'intro12' : 'Und das bin ich?',
    'intro13' : 'Ja genau.',
    'intro14' : 'Aber ich habe doch selber keine Ahnung von Phishing...',
    'intro15' : 'Und genau deshalb dürfen wir keine Zeit verlieren. Keine Sorge! Ich bringe dir das nötige bei.',
    'intro16' : 'Wer bist du eigentlich?',
    'intro17' : 'Ich bin John, dein Phishing-Trainer. Ich werde dich exzellent ausbilden, damit du den großen Kampf gewinnen kannst.',
    'intro18' : 'Den großen Kampf?',
    'intro19' : 'Alles zu seiner Zeit... Komm jetzt erstmal mit.',
    'intro20' : 'Aber... aber..., wo bin ich überhaupt?',
    'intro21' : 'In der Trainingsarena! Hier werden wir üben. Ich erkläre es dir erstmal...',

    // Tutorial Section 1 - Basics, No-Phish + Random
    // Basics
    'introduction_1_0' : 'Willkommen! Mein Name ist Phil.\n\n In diesem Spiel wirst du lernen Phishing URLs von gutartigen URLs zu unterscheiden.',
    'introduction_1_0.5': 'Nach Abschluss des Spiels solltest du in der Lage sein URLs in z.B. E-Mails oder sozialen Netzwerken zu untersuchen bevor du auf sie klickst.\n\nAuch bei der Eingabe sensibler Daten solltest du immer die URL der aktuellen Webseite überprüfen.',
    'introduction_1_1' : 'Im Laufe des Spiels wirst du insgesamt sechs Typen von URLs kennenlernen.\n\nDarunter sind fünf verschiedene Phishing URL-Typen, die versuchen werden, dich in die Irre zu führen. \n\nDer sechste Typ ' +
        'heißt "No-Phish". Er beinhaltet alle legitimen URLs, die dich auf eine gutartige Webseite führen.',
    'introduction_1_1.5' : 'Schauen wir uns zunächst den Aufbau einer URL (auch Internetadresse genannt) an.\n\nBewege deinen Cursor über eine gezeigte URL um ihre Bestandteile zu entdecken.',
    'introduction_1_2' : 'Eine URL besteht aus drei Teilen: \n Schema, Host und Pfad.\n\n' +
        'Der Host beginnt nach dem doppelten Slash (//) und endet mit dem ersten einfachen Slash (/).\n\n Der Pfad erstreckt sich bis zum Ende der URL.',
    'introduction_1_2_2': "URLs starten immer mit einem Schema.\n\nFür Webseiten sind dies typischerweise HTTPS oder HTTP.\n\n",
    'introduction_1_2_3':"Webseiten, die kein HTTPS verwenden, verschlüsseln deine Eingaben nicht. Daher solltest du auf Webseiten mit HTTP generell vermeiden, private Informationen einzugeben.\n\n"+
        "Beachte weiterhin, dass dein Browser das Schema nicht immer anzeigt, auch wenn wir es hier im Spiel immer mit angeben.\n\n",
    'introduction_1_3' : 'Durch den Host-Teil wird die Adresse des Webservers angegeben, auf dem die Webseite angeboten ' +
        'wird.\n\nIm Wesentlichen versteckt sich hinter dem Host-Teil eine IP-Adresse, die allerdings im alltäglichen ' +
        'Gebrauch meist durch eine verständlichere Zeichenkette ersetzt wird.',
    'introduction_1_3.5' : 'Eine IP-Adresse besteht aus vier Zahlen (zwischen 0 und 255), die durch Punkte abgetrennt werden.\n\n Bei solchen URLs solltest du besonders vorsichtig sein, da du nicht weißt auf welche Webseite sie dich führt.',
    'introduction_1_4' : 'Der Host besteht aus einer Reihe von verschiedenen Domains, die Bereiche '+
        'im Internet angeben.\n\n Je weiter rechts eine Domain im Host steht, desto wichtiger ist sie.\n\n '+
        'Ganz am Ende des Hosts steht die Top-Level Domain (TLD), z. B. \".com\" oder \".de\".',
    'introduction_1_4_5': 'Wie bereits erklärt, wird die letzte Domain des Host-Teils Top-Level-Domain (TLD) genannt. Die letzten zwei Domains zusammen werden als "Registrierte Domain" (RD) bezeichnet.\n\n'+
        'Die RD ist der wichtigste Teil des Hosts, da er weltweit einzigartig sein muss.\n'+
        'Folglich können Phisher keine bereits existierende RDs (z.B. amazon.de) wählen und müssen auf andere Tricks zurückgreifen.',
    'introduction_1_4_10': "Sonderzeichen (z.B. ?, #, %, !) sind in der Domain nicht erlaubt, außer Bindestriche [-]!\n"+
        "Das beinhaltet auch den Slash [/], der das Ende des Host-Teils angibt.\n\n"+
        "In Domains gibt es keinen Unterschied zwischen Groß- und Kleinbuchstaben.",
    // No-Phish
    'introduction_1_5' : 'Zuerst wollen wir uns URLs vom Typ "No-Phish" etwas genauer ansehen.\n\n Man erkennt sie daran, dass die Domain links neben der Top-Level Domain der Adresse des gewünschten Ziels entspricht.',
    // Random
    'introduction_1_6' : 'Als nächstes schauen wir uns den ersten Phishing-Typen an: "Random".\n\n Er beinhaltet alle scheinbar zufälligen URLs, wo keine Verbindung zu einem bestimmen Ziel erkennbar ist. \n\n Wenn du solch eine URL siehst, solltest du vorsichtig sein!',
    'introduction_1_6.5' : 'Ein weiterer Typ von Phishing URLs ist "IP-Adresse".\n\nEr umfasst alle URLs, die im Host-Teil statt einer Zeichenkette eine IP-Adresse verwenden.',
    'introduction_1_7' : 'Das waren ganz schön viele Informationen auf einmal, oder?\n\n Aber keine Sorge, es ist nicht so kompliziert wie es klingt. Du schaffst das!',
    'introduction_1_8' : 'Im folgenden Level sollst du üben URLs zu unterscheiden.\n\n Dafür sollst du Münzen in die richtigen Fässer einordnen. Jede Münze zeigt eine URL, wenn du Sie anklickst.\n\n Schaffst du es meine Punktzahl zu überbieten?',

    //Tutorial Section 2 - Regdomain
    'introduction_2_0' : 'Lass uns weitermachen!\n\nIn diesem Level wirst du einen weiteren Phishing-Typ kennenlernen.',
    'introduction_2_1' : 'Der nächste Phishing-Typ heißt "RegDomain".\n\nHierbei nutzen Phisher eine URL, die der originalen URL sehr ähnlich ist und sich oftmals nur in einem Wort oder manchmal auch nur einem Buchstaben vom Original unterscheidet.',
    // RegDomain
    'introduction_2_2' : 'Meistens fügen die Phisher Begriffe zur originalen URL hinzu, die ein Gefühl von Sicherheit suggerieren sollen.\n\nDie URL sieht so oft täuschend echt aus, sie führt jedoch zu einer ganz anderen Seite.',
    'introduction_2_2_5': 'Auch das leichte Abändern einer URL kann gefährlich sein wenn du nicht aufpasst!\n\nDabei wird ein Buchstabe weggelassen, ersetzt, hinzugefügt oder die Position von zwei Buchstaben geändert.',
    'introduction_2_2_10': 'Alternativ kann der Phisher die TLD (Top-Level Domain) ändern, sofern diese neue Registrierte Domain dann noch nicht vergeben ist!',
    'introduction_2_3' : 'Um diesen Typ zu erkennen ist also besonders viel Aufmerksamkeit erforderlich!',
    'introduction_2_4' : 'Ein Tipp für außerhalb des Spiels:\n\nBist du unsicher, ob eine URL bösartig ' +
        'oder legitim ist, suche mit einer Suchmaschine nach der angeblich zugehörigen Webseite!\n\n In den meisten ' +
        'Fällen wird dir dann die richtige Webseite als erstes Ergebnis angezeigt und du kannst die URLs ' +
        'vergleichen.',
    'introduction_2_5' : 'Im Spiel wirst du diese Zeit jedoch nicht bekommen und musst schnell entscheiden!\n\n '+
        'Klicke auf "Weiter", wenn du bereit bist, mit mir zu üben und glaubst, mich schlagen zu können.',

    // Section 3 - Subdomain
    // Subdomain
    'introduction_3_0' : 'Oh, da hast du mich wohl schon wieder geschlagen! Du wirst wirklich immer besser. \n\n Du bist definitiv bereit für den nächsten Typ.',
    'introduction_3_1' : 'Dieser Typ heißt "Subdomain". \n\n Im Host-Teil kann eine Reihe von Domains aneinandergereiht ' +
        'werden. Die untergeordneten Domains heißen dabei Subdomains. \Und auch diese können von Phishern zur Täuschung verwendet werden!',
    'introduction_3_2': 'Schau dir dieses Beispiel einmal genauer an. \n\nSieht auf den ersten Blick gar nicht ' +
        'so gefährlich aus, oder?',
    'introduction_3_2.5': 'Zunächst könnte man meinen, wir werden auf die offizielle Seite von Apple weitergeleitet.\n\n ' +
        'Aber entscheidend ist die letzte Kennung vor der TLD. Wir werden also in Wirklichkeit zu tayawldma.com ' +
        'weitergeleitet. \n\nDas klingt nicht nach Apple!',
    'introduction_3_3' : 'Achte also darauf, wo der Host-Teil wirklich endet und auf welche Seite du ' +
        'weitergeleitet wirst. \n\nPhisher nutzen diese Technik oft bei mobilen Geräten mit einem kleinen Bildschirm.\n' +
        'So sieht man nur den Beginn der URL in der Browserleiste und nicht die wahre Identität hinter der URL.',
    'introduction_3_4' : 'Jetzt kennst du bereits die ersten fünf Typen.\nIch hoffe, du erinnerst dich noch an alles! \n\nLass uns noch einmal gegeneinander antreten!',

    // Tutorial Section 4 - Path
    'introduction_4_0' : 'Merkst du schon, wie du langsam sicherer wirst im Erkennen von Phishing-URLs? \n\n Du bist schon ' +
        'wirklich gut, aber es gibt noch mehr zu lernen! \n\n Es fehlt noch ein letzter Typ. ',
    // Path
    'introduction_4_1' : 'Dieser Phishing-Typ wird "Pfad" genannt und ' +
        'verwendet einen irreführenden Pfad zur Täuschung. \n\n Keine Sorge! Mit ein bisschen Übung ist es auch kein Problem, diesen Typen zu erkennen.\n',
    'introduction_4_2' : 'Bei der Angabe des Pfads handelt es sich um den genauen ' +
        'Speicherort einer angefragten Webseite. \n\nManchmal werden im Pfad auch bestimmte Parameter angegeben. ',
    'introduction_4_3' : 'Erinnere dich aber an den wichtigsten Teil: \n\nDie Adresse im Host-Teil ist entscheidend und bestimmt, auf welche Webseite ' +
        'du weitergeleitet wirst.\n\n Dein Fokus sollte also immer dem Host-Teil der URL liegen!',
    'introduction_4_4' : 'Im Pfad-Teil verstecken sich Begriffe, die auf eine legitime ' +
        'URL hinweisen.\nAber der Host-Teil scheint vollkommen willkürlich zu sein und hat nichts mit ' +
    'eBay zu tun. \n\nDies ist ein Trick, um unaufmerksame Nutzerinnen und Nutzer in die Falle zu locken!',
    'introduction_4_5' : 'Du hast es fast geschafft! \nJetzt kennst du alle Phishing-Typen.\n\nSchaffst du es mich auch noch ein letztes Mal zu schlagen?',

    // Evaluation texts

    // No-Phish
    'eval_nophish_correct': 'Diese URL hast du korrekt als ungefährliche URL dem Typen "No-Phish" zugeordnet, sehr gut!\n\n ' +
        'Sie verweist tatsächlich auf das zugehörige Ziel.',
    'eval_nophish_wrong': 'Diese URL hast du als gefährlich eingeschätzt, auch wenn sie eigentlich zur richtigen Seite führt. Schade! Betrachte die Registrierte Domain noch einmal genauer!',
    'eval_nophish_skip': 'Bei dieser URL hättest du dich ruhig trauen können, sie als korrekt einzuordnen! ' +
        'Hierbei handelt es sich um eine korrekte URL vom Typen "No-Phish".',
    // Random
    'eval_random_correct': 'Hier hast du die Phishing-URL korrekt dem Typen "Random" zuordnen können. Die URL hat keinerlei Verbindung ' +
        'zu dem angeblichen Ziel und besteht nur aus zufälligen Zeichen. Gut gemacht!',
    'eval_random_wrong': 'Schau dir diese URL noch einmal ganz genau an. Sie verweist nicht auf das angebliche Ziel und stellt ' +
        'auch keinerlei Verbindung zu diesem her. Also handelt es sich eigentlich um eine Phishing-URL des Typs "Random"!',
    'eval_random_skip': 'Diese URL hast du leider übersprungen. Im echten Leben ist es auch besser, zweifelhafte Links besser zu ignorieren. ' +
        'In diesem Fall handelete es sich um eine Phishing-URL des Typs "Random", denn sie hat keine Verbindung zum angeblichen Ziel.',
    // IP
    'eval_ip_correct': 'Sehr gut, du hast die URL korrekt dem Typen "IP-Adresse" zugeordnet! Du warst aufmerksam und hast erkannt, dass ' +
        'der Host-Teil hier durch eine IP-Adresse ersetzt wurde.',
    'eval_ip_wrong': 'Diese URL hättest du dir noch genauer anschauen müssen. Der Host wird durch eine IP-Adresse dargestellt. Das ist ein klarer Indikator, dass es sich um eine Phishing-URL handelt. Sortiere solche URLs in die Kategorie "IP-Adresse"!',
    'eval_ip_skip': 'Schade, hier hättest du dich ruhig trauen können, diese URL dem Typ "IP-Adresse" zuordnen zu können, da bei diesem Typen der Host durch eine IP-Adresse ersetzt wird.',
    // Path
    'eval_path_correct': 'Super gemacht, diese URL hast du richtig eingeordnet. Der Täuschungsversuch befindet sich hier im Pfad-Teil der URL.',
    'eval_path_wrong': 'Diese URL ist eigentlich vom Typ "Pfad". Das kannst du daran erkennen, dass der Host-Teil nicht auf das angebliche Ziel verweist, stattdessen wir der Pfad zur Täuschung verwendet.',
    'eval_path_skip': 'Den Typ dieser URL hättest du daran erkennen können, dass der Host-Teil nicht zum angeblichen ' +
        'Ziel führt, aber gleichzeitig das Ziel im Pfad enthalten ist. Achte beim nächsten Mal darauf!',
    // Subdomain
    'eval_subdomain_correct': 'Perfekt, diese URL hast du genau richtig zugeordnet! Die Subdomains wollten dich in die Irre führen, '+
        'du hast die Täuschung aber durchschaut!',
    'eval_subdomain_wrong': 'Hier musst du besser aufpassen. In diesem Fall wurde die Subdomain modifziert, um dich zu täuschen. ' +
        'Solche URLs musst du in Zukunft dem Typen "Subdomain" zuordnen.',
    'eval_subdomain_skip': 'Wenn du wieder eine URL dieser Art siehst, achte genauer auf die Subdomains. ' +
        'Wenn du darin das angebliche Ziel erkennst und der Host-Teil dich auf eine andere Seite führt, dann handelt es sich um den Typ "Subdomain"!',
    // RegDomain
    'eval_regdomain_correct': 'Gut gemacht, eine so modifizierte URL zu erkennen ist gar nicht so einfach. Du hast sie dem richtigen Typen "RegDomain" zugeordnet. Super!',
    'eval_regdomain_wrong': 'Schade, hier hast du einen Fehler gemacht. Die Registrierte Domain der URL wurde modifiziert. Daher hätte diese URL dem Typen "RegDomain" zugeordnet werden müssen!',
    'eval_regdomain_skip': 'Diese URL hast du übersprungen. Betrachte in Zukunft den Host-Teil genauer, um die Veränderung in der Registrierte Domain zu erkennen! ',

    'introduction_1Level' : 'Einführung 1',
    'introduction_2Level' : 'Einführung 2',
    'introduction_3Level' : 'Einführung 3',


    'introLevel_1' : 'Bevor du mit dem Üben anfangen kannst, musst du wissen, wie das Spiel funktioniert. Schauen wir uns ' +
        'einmal das Spielfeld etwas genauer an. Die goldene Münze ist ein URL-Element, welches den Namen der ' +
        'Webseite und die angeblich dazugehörige URL enthält. Aktuell ist der Inhalt noch verborgen. Um die URL zu öffnen, ' +
        'klicke sie! Versuch es mal.',
    'introLevel_1_fail' : 'Klicke auf die URL, um sie zu öffnen',
    'introLevel_2' : 'Super! Jetzt schließe die URL wieder mit einem weiteren Klick.',
    'introLevel_2_fail' : 'Klicke erneut auf die URL, um sie zu schließen.',
    'introLevel_3' : 'Ganz einfach, oder? Schauen wir uns nun weiter um. Du siehst ebenfalls drei Holzfässer, auf ' +
        'denen eine Typbezeichnung steht. Das sind die URL-Container, in die du beim Spiel die richtigen Münzen "hineinwerfen" musst. ' +
        'Die Typbezeichnung zeigt an, welche URL-Typen du hier ablegen sollst. Probieren wir das doch einmal aus! Öffne ' +
        'jetzt nochmal die URL.',
    'introLevel_3_fail' : 'Klicke erneut auf die URL, um sie wieder zu öffnen.',
    'introLevel_4' : 'Wir haben es hier mit einer URL vom Typ 0, also "No-Phish" zu tun. Um eine URL in einen Container ' +
        'abzulegen, klicke auf die URL, halte gedrückt und ziehe die URL auf den Container. Lasse die URL los, wenn sie ' +
        'sich über dem Container befindet. Versuche jetzt die URL vom Typ 0 dem Container mit der Aufschrift Typ 0 zuzuweisen.',
    'introLevel_4_fail' : 'Folge bitte den Anweisungen und lege die URL in den Container vom Typ 0!',
    'introLevel_5' : 'Das hat doch schonmal super funktioniert! Du siehst jedes Mal, wenn du eine URL in einem Container ' +
        'ablegst, einen Smiley, der dir anzeigt, ob deine Zuweisung korrekt war oder nicht. Für eine richtig zugewiesene ' +
        '"No-Phish"-URL erhälst du 200 Punkte, für eine richtig zugewiesene Phishing-URL sogar 500 und in jedem Fall den ' +
        'grünen Smiley! Außerdem erscheint jedes Mal, wenn du eine URL in einem Container abgelegt hast, eine neue URL ' +
        'auf dem Spielfeld. Öffne jetzt die neue URL.',
    'introLevel_5_fail' : 'Klicke auf die neue URL, um sie zu öffnen.',
    'introLevel_6' : 'Dies ist eine URL vom Typ 2. Versuche diese URL mal in den Container von Typ 1 zu legen.',
    'introLevel_6_fail' : 'Folge bitte den Anweisungen und lege diese URL in den Container von Typ 1.',
    'introLevel_7' : 'Was ist nun passiert? Du hast eine Phishing-URL einem anderen Phishing-Typ zugewiesen. Das ist ' +
        'zwar nicht hundertprozentig richtig, aber immerhin hast du schon erkannt, dass es sich um Phishing handelt. ' +
        'Dafür bekommst du zwar nicht die vollen 500 Punkte, aber immerhin erhälst du dafür 200 Punkte und siehst den ' +
        'gelben Smiley. Öffne nun wieder die neue URL.',
    'introLevel_7_fail' : 'Klicke auf die neue URL, um sie zu öffnen.',
    'introLevel_8' : 'Nun haben wir eine URL vom Typ 1 vorliegen. Lege diese URL jetzt in den Container von Typ 0.',
    'introLevel_8_fail' : 'Folge bitte den Anweisungen und lege diese URL in den Container von Typ 0.',
    'introLevel_9' : 'Auweia! Das sollte dir im Spiel natürlich nicht passieren. Du hast eine URL komplett falsch zugeordnet ' +
        'und erhälst dafür 0 Punkte. Das zeigt dir der rote Smiley. Aber keine Sorge, wenn dir sowas mal passiert, du bist ' +
        'ja hier zum Üben! Solltest du mal vergessen haben, welche URLs ein bestimmter Typ enthält, kannst du dir nochmal ' +
        'eine kleine Info ansehen. Klicke dazu auf das "i" in der linken unteren Ecke des Containers von Typ 0.',
    'introLevel_9_fail' : 'Klicke auf das "i" in der linken unteren Ecke des Containers von Typ 0!',
    'introLevel_10' : 'Jetzt wird dir eine Info zum Typ 0 angezeigt. Im Spiel kannst du dir so viele Infos von Containern, ' +
        'wie du willst anzeigen lassen. Während dir die Infos angezeigt werden, pausiert das Spiel und du kannst dir ' +
        'in Ruhe den Text durchlesen. Sobald du alle Infotexte geschlossen hast, geht das Spiel wieder los und der Timer ' +
        'läuft weiter. Schließe jetzt die Info wieder, indem du erneut auf das "i" klickst.',
    'introLevel_10_fail' : 'Klicke auf das "i", um die Info wieder zu schließen!',
    'introLevel_11' : 'Dein Ziel ist es also möglichst viele Punkte zu erzielen, um besser als dein Gegner, der Filter, ' +
        'zu sein. Deine aktuelle eigene Punktzahl siehst du immer oben links, dagegen die Punktzahl, die du schlagen ' +
        'musst, rechts. Du musst mehr Punkte erzielen, als der Filter. Bei gleicher Punktzahl hast du verloren. Allerdings ' +
        'hast du dafür nur begrenzt Zeit. Oben in der Mitte siehst du einen Timer, der während des Spieles runterläuft. ' +
        'Wenn er bei 0 ist, ist das Level vorbei und du gelangst zur Auswertung. Klicke jetzt auf Weiter.',
        'introLevel_11_fail' : "Klicke auf Weiter!",
    'introLevel_12' : 'Zu Beginn des Spieles stehen erstmal nur drei URL-Typen zur Verfügung. Um auch die anderen Typen ' +
        'kennenzulernen und den großen Kampf zu erreichen, musst du eine gewisse Anzahl an Übungsdurchläufen pro Runde ' +
        'schaffen. Wie viele das genau sind, hängt davon ab, wie gut du in den Übungsdurchläufen abschneidest. Je besser ' +
        'du abschneidest, desto schneller kommst du eine Runde weiter. Wenn du bereit bist, die erste Runde zu spielen, ' +
        'klicke auf Weiter.',
      'introLevel_12_fail' : 'Klicke auf Weiter!',
    'language' : 'Sprache',
    'level1' : 'Level Eins',
    'options' : 'Einstellungen',
    'skip' : 'Keine\nAhnung',
    'InfoText_Skip' : 'Überspringe die URL, wenn du keine Ahnung hast.',
    'InfoText_Skip_fail' : 'Eine übersprungene URL wird nicht bewertet',
    'skipIntro' : 'Intro überspringen',
    'startGame' : 'Spiel starten',
    'startNewGame' : 'Neues Spiel Starten',
    'resumeGame' : 'Spiel fortsetzen',
    'you' : 'Punkte',
  'no-Phish':'No-Phish',
  'random':'Random',
  'ip':'IP-Adresse',
  'path':'Pfad',
  'subDomain':'Subdomain',
  'regDomain':'RegDomain',
  'repeatLevel': "Level wiederholen",
      // Tutorial URL parts
    'url-scheme': "Schema",
    // Eventuell hier andere Namen für verwenden?
    'url-domain': 'Domain',
    'url-rd': "Registrierte Domain",
    'url-host': "Host",
    'url-subdomain': "Subdomain",
    'url-path': "Pfad",
    'url-query': "Query",
    'url-fragment': "Fragment",
    'url-tld': "Top-Level Domain (TLD)",
    'url-etld': "effective Top-Level Domain (eTLD)",
    'url-questionMark': "?",
    'copy-button': "Kopieren",
    'copy-button-success': "Kopiert!",
    'playerid' : "Player-ID",
    'personalization': "Spielpersonalisierung",
    'enabled': "Aktiviert",
    'disabled': "Deaktiviert"
  }
});
