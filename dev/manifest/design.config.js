/**
 * @Date:
 * @Last modified time:
 */



MTLG.loadOptions({
  "bgColor": "#DCC08B", // background color
  "button_bgColor1": "#2494a2",
  "button_txtColor1": "#083878",
  "button_bgColor2": "#cc0000",
  "button_txtColor2": "#ffffff",
  "box_bgColor": "#ffffff",
  "box_alpha": 1.0,
  "text_txtColor": "#9E9E9E",
  "text_bgColor": "#ffffff",
  "text_padding": {
    lr: 25, //left-right padding
    tb: 8 //top-bottom padding
  },
  "text_fontSize": "30px",
  "text_font": "Arial",
  "text_lineWidth": 900,
  "textbox_txtColor": "#000000",
  "textbox_bgColor": "#ffffff",
  "textbox_strkColor": "#000000" ,
  "textbox_padding" : {
    lr: 25, //left-right padding
    tb: 8 //top-bottom padding
  },
  "textboxTutorial_fontSize": "46px Arial",
  "textboxTutorial_txtColor": "#000000",
  "textboxTutorial_bgColor": "#2d3258",//MTLG.getOptions().bgColor,
  "textboxTutorial_padding": {
    lr: 25, //left-right padding
    tb: 8 //top-bottom padding
  },
  "countdown_color": "#ffffff",
});
