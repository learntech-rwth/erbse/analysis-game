MTLG.loadOptions({
    "width": 1920, //game width in pixels
    "height": 1080, //game height in pixels
    "languages": ["en", "de"], //Supported languages. First language should be the most complete.
    "countdown": 180, //idle time countdown
    "fps": "60", //Frames per second
    "playernumber": 4, //Maximum number of supported players
    "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
    "webgl": false, //Using webgl enables using more graphical effects
    "timerLength": 60,
    "pointsForRightType": 500,
    "pointsForRightTendency": 200,
    "filterPoints": 2000,
    "containerCount": 3,
    "difficulty": "easy",
    "urlTypes": [0, 1, 2],
    "urlMoveArea" : {"xmin":0, "xmax":1920, "ymin":200, "ymax": 750},	//where URLs are generated and allowed to move
    "urlContainerPositionArea": {"xmin":50, "xmax":1850, "ymin":800, "ymax": 1000},	//where DropAreas are generated
    "urlContainerSkipEnabled": true,
    "urlContainerVisualImage": "barrel.png",
    "urlContainerVisualTextRelPosition": [0.50, 0.50],  //[xrel,yrel] calculated from top-left
    "urlContainerVisualTextFont": "bold 28px Arial",
    "infoTextFont": "20px Arial",
    "pointsReactionTextFont": "bold 28px Arial",
    "urlOpenImage": "roll.png",
    "urlClosedImage": "coin.png",
    "urlTextFont": "bold 20px Arial",
    "urlMovementSpeed": 1.5,
    "urlPressmoveMinDistance": 50,
    "speechbubbleDownCenterRelPosition": [0.50, 0.43], //[xrel,yrel] calculated from top-left
    "speechbubbleTextFont": "40px Piazzolla",
    "soundSwitchImage": "ton-an.png",
    "blackBackgroundSoundSwitchImage": "ton-an-white.png",
    "correctClassificationReactionImage": "smiley-good.png",
    "mediocreClassificationReactionImage": "smiley-middle.png",
    "wrongClassificationReactionImage": "smiley-bad.png",
    "reactionImageShowTime" : 2000,
    "backgroundLevel1Image": "background_training.jpg",
    "backgroundEndImage": "background_end.jpg",
    "speechbubbleImage": "speechbubble.png",
    "tutorialFigureImage": "maskottchen.png",
    "signImage": "schild.png",
    "signTextFont": "46px Piazzolla",
    "signTextRelPosition" : [0.50, 0.40], //[xrel,yrel] calculated from top-left
    "allowVoluntaryRepetition" : true,		// if you can repeat previous level
    "continueThreshold": 0.7,	// how much % of assignments need to be correct to unlock next level
    "compareToScore": true, //if filter score needs to be exceeded to unlock next level
    "evaluatePositiveUrls": true,
    "evaluateSkipedUrls": true,
    "logServerBaseUrl" : "",//"https://erbse.elearn.rwth-aachen.de",
    "devMode": false
});
