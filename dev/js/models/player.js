

function Player()
{
  const PARTIAL_CORRECT = 1;
  const CORRECT = 2;
  const WRONG = 0

  this.evaluationStatus = {
    PARTIAL_CORRECT,
    CORRECT,
    WRONG
  };

  let points = 0;
  this.resetPoints = function () {
    points = 0;
  }
  this.resetPoints();

  this.addPoints = function (amount) {
    points = points + amount;
  }

  this.getPoints = function () {
    return points;
  }

  this.setPoints = function (amount) {
    points = amount;
  }

  /**
   * Given Answers
   */
  const amountGivenAnswersTotal = {
    [WRONG]: 0,
    [PARTIAL_CORRECT]: 0,
    [CORRECT]: 0
  };
  let amountGivenAnswers = undefined;

  this.resetAmountGivenAnswers = function () {
    amountGivenAnswers = {
      [WRONG]: 0,
      [PARTIAL_CORRECT]: 0,
      [CORRECT]: 0
    };
  }
  this.resetAmountGivenAnswers();

  this.getAmountGivenAnswers = function () {
    /**
     * Do not return ref to internal object
     */
    return { ...amountGivenAnswers };
  }

  this.getAmountGivenAnswersTotal = function () {
    /**
     * Do not return ref to internal object
     */
    return { ...amountGivenAnswersTotal };
  }

  this.addGivenAnswer = function (quality, amount = 1) {
    amountGivenAnswers[quality] += amount;
    amountGivenAnswersTotal[quality] += amount;
  }

  this.getGivenAnswer = function (quality) {
    if(quality == undefined) {
      return this.getAmountGivenAnswers();
    } else {
      return amountGivenAnswers[quality];
    }
  }

  this.getGivenAnswerTotal = function (quality) {
    if(quality == undefined) {
      return this.getAmountGivenAnswersTotal();
    } else {
      return amountGivenAnswersTotal[quality];
    }
  }

  //stores data of all played level (this session)

  let finishedLevelHistory = [];
  //params = {"levelData": what level generator produced }
  this.addLevelToHistory = function (params) {
    let lastLevel = {};
    lastLevel['levelData'] = params["levelData"];
    lastLevel['assignedUrlContainer'] = this.getAssignedUrlContainer();
    lastLevel['points'] = this.getPoints();
    lastLevel['correctPercentage'] = calculatePercentageCorrectlyAssignedUrls(assignedUrlContainer);

    console.log(lastLevel['correctPercentage']);
    finishedLevelHistory.push(lastLevel);
  }

  this.getLevelHistory = function () {
    return finishedLevelHistory;
  }

  this.getLastLevelHistory = function () {
    return finishedLevelHistory[finishedLevelHistory.length-1];
  }


  let assignedUrlContainer = undefined;
  this.resetAssignedUrlContainer = function () {
    assignedUrlContainer = {0:[],1:[],2:[],3:[]};
  }
  this.resetAssignedUrlContainer();

  this.addAssignedUrl = function (url, evaluationResult) {
    assignedUrlContainer[evaluationResult].push(url);
  }

  this.getAssignedUrlContainer = function () {
    return assignedUrlContainer;
  }


  this.resetPlayerForNextLevel = function () {
    this.resetPoints();
    this.resetAmountGivenAnswers();
    this.resetAssignedUrlContainer();
  }


  function calculatePercentageCorrectlyAssignedUrls(assignedUrl)
  {
    let nrCorrectAnswers = assignedUrl[2].length;
    //right every not correctly assigned url doesnt count for the correct%
    let nrWrongAnswers = assignedUrl[1].length + assignedUrl[0].length;

    let result = 0
    //if no correct answer was given we know that too few urls got assigned correct. We skip the calculation to prevent a /0 error
    if(nrCorrectAnswers > 0)
    {
      result = nrCorrectAnswers/ (nrCorrectAnswers + nrWrongAnswers);
    }
    return result;
  }

}
