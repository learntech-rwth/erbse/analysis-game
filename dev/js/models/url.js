
//model of url
function url(_urlId,  _type, _urlString, _context, _deceptionPart)
{
  let urlId = _urlId
  let urlString = _urlString;
  let context = _context;
  let type = _type;
  let deceptionPart = _deceptionPart;

  //console.log("_urlData " +_urlId+" "+ _urlString+" "+ _type+" "+ _context+" "+ _deceptionPart);


  this.getType = function()
  {
    return type;
  }

  this.getUrlId = function()
  {
    return urlId;
  }

  this.setUrlId = function(_urlId)
  {
    urlId = _urlId;
  }

  this.getUrlString = function()
  {
    return urlString;
  }

  this.setUrlString = function(_urlString)
  {
    urlString = _urlString;
  }

  this.getContext = function()
  {
    return context;
  }

  this.setContext = function(_context)
  {
    context = _context;
  }

  this.returnUrl = function()
  {
    let data = {
      "content": context,
      "type": type,
      "url": urlString,
      "deceptionPart":deceptionPart
    };
    return data;
  }
}


//model of url
function serviceModel(_id, _service)
{
  let id = _id
  this.service = _service

  let url = _service.url;
  let serviceName = _service.service.name;
  let type = _service.type;

  // evaluate knowledge of url:
  let knowledge = "notSet"
  for(serviceStatus of MTLG.getOptions().pi_serviceStatuses){
    console.log(this.service)
    console.log(serviceStatus + " is " + this.service.service[serviceStatus])
    if(serviceStatus in this.service.service && this.service.service[serviceStatus] == true){
      knowledge = serviceStatus
    }
  }

  this.getKnowledge = function()
  {
    return knowledge;
  }
  
  this.getType = function()
  {
    return type;
  }

  this.getUrlId = function()
  {
    return id;
  }

  this.setUrlId = function(_urlId)
  {
    id = _urlId;
  }

  this.getUrlString = function()
  {
    return url;
  }

  this.setUrlString = function(_urlString)
  {
    url = _urlString;
  }

  this.getContext = function()
  {
    return serviceName;
  }

  this.setServiceName = function(_context)
  {
    serviceName = _context;
  }

  this.returnUrl = function()
  {
    let data = {
      "content": serviceName,
      "type": type,
      "url": url,
      "knowledge": serviceStatus
    };
    return data;
  }
}
