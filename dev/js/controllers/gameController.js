/* possible Examples:

gameController.register({
  id: 'test',
  init (gs, { navigate, save }) {
    console.log('draw')

    // save gameState
    save({ ...gs, iterate: !gs.iterate })

    // Both will navigate to 'parent.test' because the first "child" is always the default if no init was supplied
    if (gs.iterate) {
      navigate.finish()
    } else {
      navigate.parent()
    }
  },
  transition: {
    finish: 'parent.test',
    parent: 'parent'
  }
})

gameController.register({ id: 'parent' })
gameController.register({
  id: 'test'
  parent: 'parent',
  init () {
    console.log('draw')
  },
  transition: {
    finish: 'test'
  }
})

*/

gameController = new (function GameController () {

  /**
   * GameController CONSTANTS
   */
  const DELIMITTER = '.'

  /**
   * Internal Storage Structre for Registered Levels
   */
  let defaultLevel = null
  let currentLevel = null
  const registered = []
  const levels = {
    // Get all registered levels.
    get registered () { return [...registered] },
    // just get the ids of all registered levels.
    get keys () { return registered.map(level => level.id) },
    // Get an Level by its ID
    get (id) {
      const index = levels.keys.indexOf(id)
      return index < 0 ? null : registered[index]
    },
    // Get all children of an Level
    getChildren (id) {
      return registered.filter(level => level.id.startsWith(`${id}.`))
    },
    // Get all Levels in a tree structure
    get tree () {
      const sortedObject = registered.reduce((acc, cur) => {
        const key = cur.parent || ''
        return { ...acc, [key]: acc[key] ? [...acc[key], {...cur}] : [{...cur}] }
      }, {})

      /**
       * May perform baaaad?
       */
      let checked = 0
      for (const id in sortedObject) {
        for (const level of sortedObject[id]) {
          if (sortedObject[level.id]) {
            level.children = sortedObject[level.id]
            checked++
          }

          if (checked === Object.keys(sortedObject).length) {
            break;
          }
        }

        if (checked === Object.keys(sortedObject).length) {
          break;
        }
      }

      return sortedObject['']
    },
    isDefault (id) { return defaultLevel && defaultLevel.id === id }
  }
  this.levels = levels

  /**
   * Helper Methods for registered Levels
   */

  /**
   * This functions generates the navigate Object
   * for each registered level.
   */
  const getNavigateObject = function (level, gameState) {
    const transitions = level.transition

    /**
     * Map manual transitions of registered level
     * to an Object of functions. The keys link then to the level specified in the
     * value
     */
    const mapTransitions = function (transition) {
      const _t_key = transition
      const _t_callback = (payload) => {
        _logger.log(`Navigate with transition '${_t_key}'.`)
        let _t_level_id = transitions[transition]

        if ([null, undefined].includes(_t_level_id)) {
          throw new Error(`Level ID for '${_t_key}' is not defined.`)
        }

        if (typeof _t_level_id === 'object') {
          const { id, ...tmp } = _t_level_id;
          _t_level_id = id;
          payload = { ...tmp, ...payload };
        }

        return navigate(_t_level_id, payload)
      }

      return { [_t_key]: (_p = {}) => _t_callback(_p) }
    }

    /**
     * Automatically generate links in the navigation Object for all
     * Children. Manual Transitions therby have a higher priority.
     */
    const mapChildren = function (subLevel) {
      const _t_dotted_key = (`to.${subLevel.id.replace(level.id, '')}`)
      const _t_key = _t_dotted_key.replace(/(\.[a-z])/ig, ($1) => {
        return $1.toUpperCase().replace('.', '')
      }).replace('.', '')

      const _t_callback = (payload) => {
        _logger.log(`Navigate to Children '${subLevel.id}'.`)
        return navigate(subLevel.id, payload)
      }

      return { [_t_key]: (_p = {}) => _t_callback(_p) }
    }

    /**
     * Creates an Object with
     * functions named after the transition which transits to the levelID
     * of the property value
     *
     * Uses:
     * - mapChildren
     * - mapTransition
     */
    const _navigate = [
      ...(levels.getChildren(level.id).map(mapChildren)),
      ...(Object.keys(transitions).map(mapTransitions)),
    ].reduce((a, c) => ({ ...a, ...c }), {})

    /**
     * Define navigation methods for
     * slide functionality.
     * These can not be overriden, but do not have to be used.
     */
    const _next_slide = isNaN(gameState.slide) ? 0 : gameState.slide + 1
    _navigate.nextSlide = (payload = {}) => navigate(level.id, { ...payload, slide: _next_slide })

    const _last_slide = isNaN(gameState.slide) || gameState.slide < 1 ? 0 : gameState.slide - 1
    _navigate.lastSlide = (payload = {}) => navigate(level.id, { ...payload, slide: _last_slide })

    /**
     * The `navigate.toParent` function always exists.
     * But warns if it is called on the root level
     * It enables children to yield to the `coordinator` in case of an mistaken
     * navigation.
     */
    _navigate.toParent = function (payload = {}) {
      if (!level.parent) {
        console.warn('There is no parent for this level.')
        return;
      }
      return navigate(level.parent, payload)
    }

    return _navigate
  }

  /**
   * This Functions Wraps the MTLG check Function.
   * Many Check functions just check for an levelID which
   * is actually part of the gameController logic.
   *
   * This function abstracts the need for the check function in
   * almost any case away.
   */
  const getCheckFunction = function (level) {
    return gameState => {
      const isNextLevel = gameState && gameState.nextLevel && gameState.nextLevel === level.id

      if (!isNextLevel) {
        return 0
      }

      // this is the check against the custom check method
      if (level.check && !level.check(gameState)) {
        /**
         * if the custom check is defined
         * and is `false`. We check wether
         * the defined level is the default Level.
         */
        return isDefault(level.id) ? 1 : 0
      }

      return 2
    }
  }

  /**
   * This Function Wraps the MTLG init function with
   * providing the actual level with more properties.
   * These properties e.g. navigate abstract gameController Logic
   * from the level.
   */
  const getInitFunction = function (level) {
    let background = {}

    return gameState => {
      if (!level.init) {
        /**
         * To Call getInitFunction
         * an `level.init` callback has to be defined.
         * This is REQUIRED
         */
        throw new Error(`This Level can not be initated: ${level}.`)
      }

      const _navigate = getNavigateObject(level, gameState)

      const stage = MTLG.getStageContainer()
      if (!background.container || stage.getChildIndex(background.container) < 0) {
        background = {}
        background.container = new createjs.Container()
      }
      stage.removeAllChildren()
      stage.addChild(background.container)

      /**
       * Save Current Level for `gameController.navigate`
       */
      currentLevel = level

      return level.init(gameState, { navigate: _navigate, save, background })
    }
  }

  /**
   * Published GameController Methods
   * - need to be added to the export at the end
   */

  // Save a Game State
  const save = function (gameState) {
    _saver.save(gameState)
  }
  this.save = save

  // Restore a GameState
  const restore = function (gameState = null) {
    if (!gameState) {
      gameState = _saver.load()
    }
    MTLG.lc.levelFinished(gameState)
  }

  // Navigate to a level by ID with gameState Payload
  const navigate = function (levelID, payload = {}) {
    if (!levels.keys.includes(levelID)) {
      throw new Error(`'${levelID}' is not a registered Level`)
    }

    _logger.log(`Navigate to ${levelID}`)

    const gameState = { ..._saver.load(), ...payload }
    gameState.lastLevel = gameState.nextLevel
    gameState.nextLevel = levelID

    if (payload.slide === undefined && gameState.lastLevel !== gameState.nextLevel && gameState.slide !== undefined) {
      delete gameState.slide
      delete gameState.level
    }

    gameState.slide = isNaN(gameState.slide) ? 0 : gameState.slide
    gameState.level = typeof gameState.level !== 'object' ? {} : gameState.level

    save(gameState)

    /**
     * Only route through MTLG if slide is set to 0 or
     * `currentLevel` is not set with an valid level.
     */
    if (gameState.slide === 0 || !(currentLevel && currentLevel.start)) {
      MTLG.lc.levelFinished(gameState)
    } else {
      /**
       * This is much faster, and prevents flickering in the background
       */
      currentLevel.start(gameState)
    }
  }
  this.navigate = navigate

  // Register a new Level with a required id
  const register = function ({ id, init = null, check = null, transition = {}, parent = null }) {
    const levelIDRegexp = /[a-zA-Z0-9]+/
    const isValidLevelID = id => typeof id ==='string' && id.match(levelIDRegexp)

    if (!isValidLevelID(id)) {
      throw new Error(`'levelID' is not valid: Must be a non-empty String and Matching ${levelIDRegexp}`)
    }

    if (parent) {
      if (!levels.keys.includes(parent)) {
        throw new Error(`'parent' references a level that is currently not registered: ${parent}`)
      }

      id = `${parent}${DELIMITTER}${id}`

      if (!levels.get(parent).init) {
        levels.get(parent).init = () => { navigate(id) }
      }
    }

    const level = { id, check, init, transition, parent }

    /**
     * gameController adds own start and valid methods to the
     * registered level object.
     * Important for gameController.navigate.
     * Usage:
     * level.start(gameState) => start Level without routing through MTLG.
     *                           MTLG is uring this as well.
     * level.checkPriority(gameState) => returns [0,n] for priority.
     */
    level.start = getInitFunction(level)
    level.checkPriority = getCheckFunction(level)

    MTLG.lc.registerLevel(level.start, level.checkPriority)
    registered.push(level)
  }
  this.register = register

  // Register Default Level
  const registerDefault = function ({ id, init = null, check = null, transition = {} }) {
    register({ id, init, check, transition })
    defaultLevel = levels.get(id)
  }
  this.registerDefault = registerDefault

  // initalize
  const init = function () {
    if (!(defaultLevel && defaultLevel.id)) {
      throw new Error('There must be a defaultLevel')
    }

    MTLG.lc.registerMenu(() => this.navigate(defaultLevel.id))
  }
  this.init = init

})()
