function levelController()
{
  let urlIdCounter = 0;

  let dropAreaIdCounter = 0;
  //this contains the modell of an url, using its id as the key
  let urlModels = undefined;
  //this contains the referenc to the view object of the url, using its id as the key
  let urlViews = undefined;

  //this contains the modell of a dropArea, using its id as the key
  let dropAreasModels = undefined;
  //this contains the reference to the view objects of the url, using its id as the key
  let dropAreasViews = undefined;

  //MTLG.getOptions isnt yet available here, so these get set when the controller gets reset
  let areaDropAreas = undefined;
  let areaUrls = undefined;

  //the controller also creates a new player instance
  let player = new Player();

  this.getPlayerPoints = function()
  {
    return player.getPoints();
  }
  this.getPlayer = function()
  {
    return player;
  }

  let gamePaused = false;
  let gamePauseTriggers = 0;

  //resets everything level specific of the controller
  this.reset = function()
  {
    urlModels = {};
    urlViews = {};
    dropAreasModels = {};
    dropAreasViews = {};
    gamePauseTriggers = 0;  //needed if level was canceled during pause
    gamePaused = false;     //needed if level was canceled during pause
    areaDropAreas = MTLG.getOptions().urlContainerPositionArea;
    areaUrls = MTLG.getOptions().urlMoveArea;

    //also reset level specific information of the player
    player.resetPlayerForNextLevel();
  }

  let levelData = require('../json/lvlDataMockupV4.json');

  //loads data for the next level. Checks gamestate to see if we repeat or do a new level
  this.getNewLvlData = function(gameState)
  {
    if(gameState.repeatLevel)
    { //we repeat the level, so the next level will use the levelData of the last one
      levelData = this.getPlayer().getLastLevelHistory().levelData;
      console.log(levelData);
    }
    else {
      //gets new Data from the level generator
      //levelData = function(){return levelData;}();
      let currentLevel = levelStructure.rounds[gameState.round-1].level;
      levelData = levelGeneratorFG.createLevel(levelStructure.levelData[currentLevel]);
      levelData.usedTypes = Object.keys(levelData.typeSettings.types);
      console.log(levelData);
    }
  }

  this.getLastLvlData = function()
  {
    return levelData;
  }

  //returns a (per session) unique number to be used as an urlId
  function _returnNewUrlId()
  {
    urlIdCounter++;
    return urlIdCounter;
  }

  //returns a (per session) unique number to be used as a dropAreaId
  function _returnNewDropAreaId()
  {
    dropAreaIdCounter++;
    return dropAreaIdCounter;
  }

  //pauses the game
  this.pauseGame = function()
  {
    gamePaused = true;
    gamePauseTriggers++;
  }

  //resumes the game if paused
  this.resumeGame = function()
  {
    gamePauseTriggers--;
    if(gamePauseTriggers==0)
    {
      gamePaused = false;
    }
    else if (gamePauseTriggers<0)
    {
      gamePauseTriggers=0;
      gamePaused = false;
      console.log("More resume than pause triggers active");
    }
  }

  this.isGamePaused = function()
  {
    return gamePaused;
  }

  //checks if urlid is contained in the urlModels list
  this.isUrlIdStillActive = function(urlId)
  {
    if(urlModels[urlId])
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  this.getDropAreasViews = function()
  {
    return dropAreasViews;
  }

  this.createStartingUrls = function()
  {
    let amount = levelData["amountActiveURLs"];

    for (let i =0; i<amount; i++)
    {
      this.createNewUrl();
    }

  }

  //create a new url on a random point of the url movement area
  this.createNewUrl = function()
  {
    let urlData = getRandomUrl();
    let newId = _returnNewUrlId();

    // choose type
    let newType = urlData["type"];
    let newUrlString = urlData["url"];
    let newContent = urlData["content"];

    //choose new position
    let x = areaUrls["xmin"]+getRandomInt(areaUrls["xmax"]-areaUrls["xmin"]);
    let y = areaUrls["ymin"]+getRandomInt(areaUrls["ymax"]-areaUrls["ymin"]);

    urlModels[newId] = new serviceModel(newId, urlData);
    urlViews[newId] = new urlView(urlModels[newId], x, y);
  }

  //creates dropAreas equally distributed on a single line in the area specified by topLeftPoint, width and height.
  //the amount and type of dropAreas is in the levelData["usedTypes"] array
  this.createUrlDropAreasAndViews = function()
  {
    let dropAreas = { ...levelData['typeSettings']['types'] };
    let dropAreaKeys = [ ...levelData['usedTypes'] ];

    if (MTLG.getOptions().urlContainerSkipEnabled) {
      const skipKey = 'skip-bucket';
      const skipDropArea = {
        "type": skipKey,
        "numberOfUrls": 0,
        "captionKey": "skip",
        "descriptionKey": "InfoText_Skip",
        "category": 3
      };

      dropAreas[skipKey] = skipDropArea;
      dropAreaKeys.push(skipKey);
    }

    let areasPositionArray = calculatePositionsInArea([areaDropAreas["xmin"], areaDropAreas["ymin"]], areaDropAreas["xmax"]-areaDropAreas["xmin"], areaDropAreas["ymax"]-areaDropAreas["ymin"], dropAreaKeys.length, 1);

    for(let i=0; i<areasPositionArray.length; i++)
    {
      let nextType = dropAreaKeys[i];
      // console.log(nextType);
      let details = dropAreas[nextType];
      // console.log(details);
      this.createNewUrlDropArea(nextType ,areasPositionArray[i][0], areasPositionArray[i][1], details);
    }
  }


  //creates an urlDropArea at position x,y with the specified type
  //nameKey (for the shown Name of the DropArea) and infoKey (for the Infotext) are keys found in the language file
  this.createNewUrlDropArea = function(type, x, y, details)
  {
    let nameKey = details['captionKey'];
    let infoKey = details['descriptionKey'];

    let newId = _returnNewDropAreaId();
    //get name
    let name = MTLG.lang.getString(nameKey);
    //get type Info
    let infoText = MTLG.lang.getString(infoKey);

    dropAreasModels[newId] = new urlDropArea(newId, type);
    dropAreasViews[newId] = new urlDropAreaView(newId, x, y, type, name, infoText );
  }

  //does all the things that need to happen after an url got assigned to a dropArea
  this.processUrlOnDropArea = function(urlId, dropAreaId)
  {
    //evaluate assignment
    let evaluationResult = this.evaluateUrlAndDropArea(urlId, dropAreaId);

    _logger.log(`URL "${urlModels[urlId].getUrlString()}" with status "` + urlModels[urlId].getKnowledge() + `" with context "` + urlModels[urlId].getContext() + `" dropped in "${dropAreasModels[dropAreaId].getType()}" with evaluationResult "${evaluationResult}"`)

    //show smileay-reaction on droparea
    dropAreasViews[dropAreaId].showReaction(evaluationResult);

    //add appropriate amount of points to the player
    this.addPlayerPointsForResult(evaluationResult);

    //update player point display
    if(updatePlayerPoints) updatePlayerPoints();

    //add url to the player data
    rememberPlayerUrlAssigment(urlId, dropAreaId, evaluationResult);
    //count number of given assignments
    player.addGivenAnswer(evaluationResult);

    //remove url
    delete urlModels[urlId];
    urlViews[urlId].remove();

    //erstelle neue URL
    this.createNewUrl();
  }

  //
  this.addPlayerPointsForResult = function(result)
  {
    let amount = 0;
    switch(result)
    {
      case 1:
        amount = MTLG.getOptions().pointsForRightTendency;
        break;
      case 2:
        amount = MTLG.getOptions().pointsForRightType;
        break;
    }
    player.addPoints(amount);

  }

  //returns degree of correctness of the assignment
  //returns: 2 for correct, 1 for partially correct, 0 for wrong
  this.evaluateUrlAndDropArea = function(urlId, dropAreaId)
  {
    let urlType = urlModels[urlId].getType();
    let dropAreaType = dropAreasModels[dropAreaId].getType();

    if (dropAreaType === "skip-bucket")
    {
      return 3;
    }
    else if (urlType == dropAreaType)
    {
      return 2;
    }
    else if ( urlType != "no-Phish" && dropAreaType != "no-Phish")
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }

  //gets a random URL //TODO dont use a URL more than once (as long as its avoidable)
  function getRandomUrl()
  {

    //selects randomly one of the used url types
    let randomType = levelData["usedTypes"][getRandomInt(levelData["usedTypes"].length)];

    //the urls are together in an array
    let randomURL = getRandomInt(levelData["URLs"][randomType].length);

    let choosenURL = levelData["URLs"][randomType][randomURL];
    return levelData["URLs"][randomType][randomURL];
  }

  //adds Url to player data, adds information of how correct/wrong it was assigned
  function rememberPlayerUrlAssigment(urlId, dropAreaId, evaluationResult)
  {
    let url=urlModels[urlId].returnUrl();
    url["evaluationResult"] = evaluationResult;
    url["assignedType"] = dropAreasModels[dropAreaId].getType();

    player.addAssignedUrl(url, evaluationResult)
  }

}


var levelController = new levelController();
