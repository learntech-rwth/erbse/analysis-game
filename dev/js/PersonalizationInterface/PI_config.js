MTLG.loadOptions({

    /**
     * General settings
     */
    "pi_enabled" : false,
    "pi_alreadyPersonalized" : false,
    "pi_serviceSorting": "alphabetical", // valid options: alphabetical, popularity (keeps the order defined in JSON), random and frequency
    "pi_allowSkipping": false, // whether or not it is allowed to skip while no selection was made for the entries
    "pi_showPersonalizationOptions": false, // whether or not to give the players the option to disable personalization
    "pi_logToConsole": false, // whether the console log statements of the PI are to be printed to the console
    /**
     * Service button options
     */

    "pi_displayLogos": true, //Set if logos should be displayed if available
    "pi_serviceStatuses": [ "rejected", "selected", "used"],
    "pi_numberOfButtons": 3, // 2 or 3 buttons, uses the first x keys of serviceStatuses

    /**
     * Dimension settings
     */

    // Service logos
    "pi_imageHeight": 140,
    "pi_imageWidth": 140,
    "pi_imageBorder": 10,
    "pi_imagePadding": 5,
    "pi_logoScale": 1,

    // Container for the logo, title and service status buttons
    "pi_buttonWidth": 900,
    "pi_buttonHeight": 140,
    "pi_buttonPadding": 20,
    "pi_buttonContainerY": 145,
    
    // Settings for the service status buttons
    "pi_smallButtonWidth": 220,
    "pi_smallButtonHeight": 45,
    "pi_smallButtonText": 35,
    "pi_smallText": 28,
    "pi_smallButtonY": 85,
    "pi_smallTextButton": 35, 

    /**
     * Color settings
     */

    // General settings
    "pi_bgColor": "#f8f9fa",
    "pi_background": "#f8f9fa",
    "pi_text": "#212529",

    // Selection button settings
    "pi_buttonDeselected": "#7f7f7f",
    // "pi_usedDeselected": "#859585",
    // "pi_selectedDeselected": "#859585",
    // "pi_rejectedDeselected": "#846770",
    "pi_usedDeselected": "#7f7f7f",
    "pi_selectedDeselected": "#7f7f7f",
    "pi_rejectedDeselected": "#7f7f7f",
    "pi_buttonText": "#f8d7da",
    "pi_usedSelected": "#198754",
    "pi_selectedSelected": "#198754",
    "pi_rejectedSelected": "#dc3545",
    "pi_logoBg": "#f8f9fa",
    "pi_button": "#084298",
    "pi_WebmailButton": "#0d6efd",
    "pi_serviceBox_lineColor": "#f8f9fa", 

    // Navigation button settings
    "pi_navButton": "#0d6efd",
    "pi_navButtonText": "#f8f9fa",
    "pi_navButtonMinWidth": "250",
    "pi_navButtonMinHeight": "65",
})