function mergeServices(collectionInput, browserInput){

    if(MTLG.getOptions().pi_logToConsole) console.log(browserInput);

    var serviceOutput = {};
    var otherOutput = {};
    
    for (element in collectionInput){

        category = collectionInput[element]["category"];
        logo = collectionInput[element]["logoName"];

        if (!(category in serviceOutput)){
            serviceOutput[category] = {};
        }

        serviceOutput[category][element] = collectionInput[element];
        serviceOutput[category][element]["selected"] = false;
        serviceOutput[category][element]["rejected"] = false;
        serviceOutput[category][element]["used"] = false;
    }

    for (element in browserInput){

        // if(MTLG.getOptions().pi_logToConsole) console.log(browserInput[element]);

        let historyHostname = browserInput[element]["hostname"];

        // if(MTLG.getOptions().pi_logToConsole) console.log(historyHostname);

        // if(MTLG.getOptions().pi_logToConsole) console.log(collectionInput);

        if(historyHostname in collectionInput){

            category = collectionInput[historyHostname]["category"];
            logo = collectionInput[historyHostname]["logoName"];

            if (!(category in serviceOutput)){
                serviceOutput[category] = {};
            }


            serviceOutput[category][historyHostname] = browserInput[element];
            serviceOutput[category][historyHostname]["selected"] = true;
            serviceOutput[category][historyHostname]["rejected"] = false;
            serviceOutput[category][historyHostname]["logoName"] = logo;

            // serviceOutput[historyHostname] = browserInput[element];
            // serviceOutput[historyHostname]["selected"] = false;
            // serviceOutput[historyHostname]["rejected"] = false;

            // if(MTLG.getOptions().pi_logToConsole) console.log(serviceOutput);
            
            // delete collectionInput[historyHostname];

        } else {

            //serviceOutput.category = [];

            //if ("category" in browserInput[element]){
            if (!(browserInput[element]["category"] === "")){ 
                category = browserInput[element]["category"];

                if (!(category in serviceOutput)){
                    serviceOutput[category] = {};
                }
            
                serviceOutput[category][historyHostname] = browserInput[element];
                serviceOutput[category][historyHostname]["selected"] = true;
                serviceOutput[category][historyHostname]["rejected"] = false;
                serviceOutput[category][element]["category"] = category;

            } else {
                // category = MTLG.lang.getString('pi_other');
                otherOutput[historyHostname] = browserInput[element];
                otherOutput[historyHostname]["selected"] = true;
                otherOutput[historyHostname]["rejected"] = false;
            }
        }
    }

    serviceOutput[MTLG.lang.getString('pi_other')] = otherOutput;
    return serviceOutput;
}