
// expose framework for debugging
window.MTLG = MTLG;
// ignite the framework
document.addEventListener("DOMContentLoaded", MTLG.init);

// add moduls
require('mtlg-modul-menu');
require('mtlg-moduls-utilities');
require('mtlg-modul-lightparse');

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');
require('../manifest/design.config.js');

// load translations
require('../lang/lang.js');

var enableMouseOver = function(stage){
  if (stage._prevStage) {
    enableMouseOver(stage._prevStage)
  }
  stage.enableMouseOver();
}

levelStructure = require('../json/levelStructure.json');

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function(pOptions){
  //Initialize game

  //Initialize levels and Menus:
  //The MTLG framework uses the lifecycle manager (lc) to handle level progression,
  //resetting levels and to define a starting point for the game.
  //Use the functions registerMenu and registerLevel to define your levels.
  //The starting point of the game will be the registered menu, if one is present,
  //or the game with the highest value when passed the empty game state.
  //Register Menu

  //Register levels
  //Levels consist of two parts: the first parameter is the entrance function,
  //that sets the level up and renders it. The second parameter as a check function,
  //that receives the current gamestate as paramter and returns a value between 0 and 1.
  //The lifecycle manager starts the level with the highest return value in the next step.

  gameController.registerDefault({
    id: 'mainmenu',
    init: drawMainMenu,
    transition: {
      startGame: { id: 'game', round: 0 },
      resumeGame: 'levelselection',
      toOptions: 'options'
    }
  });

  gameController.register({
    id: 'levelselection',
    init: selectionScreen,
    transition: {
      toGameTutorial: 'game.tutorial',
      toGameLevel: 'game.level',
      toMainmenu: 'mainmenu'
    }
  });

  gameController.register({
    id: 'options',
    init: optionsScreen,
    transition: {
      toMainmenu: 'mainmenu'
    }
  });

  gameController.register({
    id: 'game',
    init: globalLevels.initGame,
    transition: {
      startRound: 'game.tutorial',
      finishGame: 'end'
    }
  });
  gameController.register({
    parent: 'game',
    id: 'tutorial',
    init: globalLevels.initTutorial,
    transition: {
      // repeatTutorial: { id: 'game.tutorial', slide: 0 },
      startLevel: 'game.level',
      toMenu: 'levelselection'
    }
  });
  gameController.register({
    parent: 'game',
    id: 'level',
    init: globalLevels.initCoinLevel,
    transition: {
      // repeatTutorial: 'game.tutorial',
      // repeatLevel: 'game.level',
      startEvaluation: 'game.evaluation',
      toMenu: 'levelselection'
    }
  });
  gameController.register({
    parent: 'game',
    id: 'evaluation',
    init: globalLevels.initEvaluationLevel,
    transition: {
      // repeatTutorial: 'game.tutorial',
      repeatLevel: 'game.level',
      // repeatEvaluation: { id: 'game.evaluation', slide: 0 },
      toMenu: 'levelselection'
    }
  });

  gameController.register({
    id: 'end',
    init: globalLevels.initEndLevel,
    transition: {
      toMenu: 'mainmenu',
    }
  });

  gameController.init();

  enableMouseOver(MTLG.getStage());
  //Init is done
  console.log("Game Init function called");

  _logger.log(`Inital Language "${MTLG.lang.getLanguage()}"`);

}

//Register Init function with the MTLG framework
//The function passed to addGameInit will be used to initialize the game.
//Use the initialization to register levels and menus.
MTLG.addGameInit(initGame);

_logger = new logger();
_saver = new saver();
_learnerProfile = new learnerProfile();