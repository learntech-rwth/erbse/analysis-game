function logger() {

  const request = function (method, url, body) {
    const fullUrl = new URL(MTLG.getOptions().logServerBaseUrl || 'http://localhost:8000');
    fullUrl.pathname = fullUrl.pathname.replace(/^\/$/g, '') + url;

    const getResponse = xhr => {
      let data = "";

      try {
        data = JSON.parse(xhr.response);
      } catch {
        data = xhr.response;
      }

      const response = {
        status: xhr.status,
        statusText: xhr.statusText,
        data
      };

      if (xhr.status === 0 || xhr.status >= 400) {
        const error = new Error();
        error.response = response;
        error.status = xhr.status;
        return error;
      }

      return response;
    }

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open(method, fullUrl.toString());

      let payload = null;
      if (body instanceof Object) {
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        payload = JSON.stringify(body);
      }

      xhr.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          resolve(getResponse(xhr));
        } else {
          reject(getResponse(xhr));
        }
      };
      xhr.onerror = function () {
        reject(getResponse(xhr));
      };
      xhr.send(payload);
    });
  }

  request.post = (url, body) => request('POST', url, body);
  request.get = (url, body) => request('GET', url, body);

  // CONFIG
  const SEND_LOG_EVERY = 10;
  const SEND_LOG_TIMEOUT_IF_FAIL = 5 * 1000;
  const RESET_LOG_OFFSET = 2 * 60 * 60 * 1000;

  // LOCAL STORAGE KEYS
  const CONFIG_KEY     = "PHISHING_URL_FILTERING_GAME_CONFIG";
  const LOG_STORE_KEY  = "PHISHING_URL_FILTERING_GAME_LOG_STORE";
  const NEXT_INDEX_KEY = "PHISHING_URL_FILTERING_GAME_NEXT_INDEX";

  // LOGGER FLAGS
  let API_ENABLED = true;
  let SEND_LOG_ENABLED = true;
  let SEND_LOG_RUNNING = false;

  this.reset = () => {
    const remove = key => {
      if (localStorage.getItem(key) !== null) {
        localStorage.removeItem(key);
      }
    }
    remove(CONFIG_KEY);
    remove(LOG_STORE_KEY);
    remove(NEXT_INDEX_KEY);
  }

  const getConfig = () => JSON.parse(localStorage.getItem(CONFIG_KEY)) || {};
  const setConfig = (key, value) => {
    let config = getConfig();
    config[key] = value;
    localStorage.setItem(CONFIG_KEY, JSON.stringify(config));
  };

  const getUUID = () => getConfig().uuid;
  const createUUID = async () => {
    let uuid = undefined;
    if (API_ENABLED) {
      const response = await request.post('/api/uuid/');
      uuid = response.data.uuid;
    } else {
      uuid = "ApiDisabled1";
    }

    if (!uuid) {
      throw new Error('Create UUID failed');
    }

    setConfig('uuid', uuid);
    setConfig('uuid_timestamp', Date.now());
    return uuid;
  }
  this.getUUID = getUUID;
  this.createUUID = createUUID;

  const getNextIndex = () => parseInt(localStorage.getItem(NEXT_INDEX_KEY)) || 0;
  const setNextIndex = index => localStorage.setItem(NEXT_INDEX_KEY, index);

  const sendLogs = async () => {
    if (SEND_LOG_RUNNING) {
      return;
    }
    SEND_LOG_RUNNING = true;
    try {
      if (!(API_ENABLED && SEND_LOG_ENABLED)) {
        return;
      }

      const uuid = getUUID();
      if (!uuid) {
        console.warn('No UUID For Logs created Yet.');
        return;
      }

      const nextIndex = getNextIndex();
      const logdata = this.getLogStore(nextIndex);

      const send = {
        uuid,
        logdata,
      }

      await request.post('/api/logdata/', send);

      setNextIndex(nextIndex + logdata.length);
    } finally {
      SEND_LOG_RUNNING = false;
    }
  }
  this.sendLogs = sendLogs;

  const sendingLogRoutine = async function() {
    if (!API_ENABLED) {
      return;
    }

    const nextIndexStart = getNextIndex();
    const logStore = getLogStore();

    if (logStore.length < nextIndexStart + SEND_LOG_EVERY) {
      return;
    }

    try {
      await sendLogs();
    } catch (error) {
      if (error.status === 0) {
        SEND_LOG_ENABLED = false;
        console.log(`LogServer is not reachable, disable for ${SEND_LOG_TIMEOUT_IF_FAIL/1000}s`);
        setTimeout(() => SEND_LOG_ENABLED = true, SEND_LOG_TIMEOUT_IF_FAIL);
      } else {
        throw error;
      }
    }
  }

  /** Logs xAPI statement */
  this.logX = (verb, object) => {
    // console.log(verb);
    let logStore = this.getLogStore(this.msgLogKey);
    // console.log(gameLog);

    let timestamp = Date.now();
    let actor = {
      "name": "Player",
      "id" : this.guid()
    }

    let logEntry = {
      actor,
      "verb":_verbs[verb],
      object,
      timestamp
    }

    logStore.push(logEntry);

    this.setLogStore(logStore);
  }

  /**
   * Logs msg
   */
  this.log = (...message) => {
    let logStore = this.getLogStore();
    let timestamp = Date.now();

    for (const partMessageIndex in message) {
      const partMessage = message[partMessageIndex];
      if (partMessage instanceof Object) {
        /**
         * Try Block to ensure logging
         */
        try {
          let temporary = JSON.stringify(partMessage);

          /**
           * Escape string again if there is not an object only
           */
          if (message.length > 1) {
            temporary = JSON.stringify(temporary);
          }
          message[partMessageIndex] = temporary;
        } catch (e) { console.error(e.message) }
      }
    }

    const msg = message.join(' ');
    // console.log(msg)

    let logEntry = {
      msg,
      timestamp
    }
    logStore.push(logEntry);

    this.setLogStore(logStore);

    sendingLogRoutine();
  }

  /**
   * Returns LogStore with xAPI statements
   */
  const getLogStore = (from = 0) => {
    if (!this.logStoreExists()) {
      this.createLogStore();
    }
    let logStoreString = window.localStorage.getItem(LOG_STORE_KEY);
    let logStore = JSON.parse(logStoreString);
    return logStore.slice(from);
  }
  this.getLogStore = getLogStore;

  /**
   * Checks if LogStore object exists
   */
  this.logStoreExists = () => {
    if (window.localStorage.hasOwnProperty(LOG_STORE_KEY)) {
      return true;
    }
    return false;
  }

  /**
   * Creates LogStore Object in local storage
   */
  this.createLogStore = () => {
    let logStore = [];

    let logStoreString = JSON.stringify(logStore);

    window.localStorage.setItem(LOG_STORE_KEY, logStoreString);
  }

  /**
   * Sets Game log
   */
  this.setLogStore = (newLogStore) => {
    let newLogStoreString = JSON.stringify(newLogStore);
    window.localStorage.setItem(LOG_STORE_KEY, newLogStoreString);
  }

  /**
   * Generates guid based on browser fingerprint
   */
  this.guid = () => {

    var nav = window.navigator;
    var screen = window.screen;
    var guid = nav.mimeTypes.length;
    guid += nav.userAgent.replace(/\D+/g, '');
    guid += nav.plugins.length;
    guid += screen.height || '';
    guid += screen.width || '';
    guid += screen.pixelDepth || '';

    return guid;
  };

  /** Exports log data of given log */
  this.exportLogData = function(logKey = null) {
    if (logKey == null) {
      logKey = this.msgLogKey;
    }
    let timestamp = Date.now();
    let filename = "logdata"+timestamp+".json";
    let data = this.getLogStore(logKey);
    data = JSON.stringify(data);
    var blob = new Blob([data], {type: 'text/json'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }
  }

  if (!getConfig().uuid_timestamp || getConfig().uuid_timestamp < Date.now() - RESET_LOG_OFFSET) {
    this.reset();
  }

  if (!this.getUUID()) {
    this.createUUID().catch(error => {
      console.error(error)
    });
  }
}
