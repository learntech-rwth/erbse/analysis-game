
//creates dropAreas equally distributed on a single line in the area specified by topLeftPoint, width and height.
//the amount and type of dropAreas is in the typeArray
function createUrlDropAreasAndVisuals(topLeftPoint, width, height, typeArray)
{
  let areasPositionArray = calculatePositionsInArea(topLeftPoint, width, height, typeArray.length, 1);

  //let debugshape= new createjs.Shape(); //hinterlegt den Bereich farblich
  //debugshape.graphics.beginFill("black").drawRect(topLeftPoint[0], topLeftPoint[1], width, height);
  //debugshape.alpha = 0.5;
  //MTLG.getStageContainer().addChild(debugshape);

  for(let i=0; i<areasPositionArray.length; i++)
  {
    // TODO NEEDED? OUTDATED
    levelController.createNewUrlDropArea(typeArray[i],areasPositionArray[i][0], areasPositionArray[i][1]);
  }
}

//get random int between 0 (included) and max (excluded)
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function selectRandom (array) {
  return array[getRandomInt(array.length)];
}

//erstellt die angegebene Anzahl an zufälligen Urls
function createUrls(topLeftPoint, width, height, amount = 1)
{
  //let debugshape= new createjs.Shape(); //hinterlegt den Bereich farblich
  //debugshape.graphics.beginFill("yellow").drawRect(topLeftPoint[0], topLeftPoint[1], width, height);
  //debugshape.alpha = 0.5;
  //MTLG.getStageContainer().addChild(debugshape);

  for(let i=0; i<amount; i++)
  {
    levelController.createNewUrl();
  }
}
