
//calculates evenly distributed positions in an area
function calculatePositionsInArea(topLeftPoint, width, height, amountPoints, horizontalOnly = 1)
{
  const rows = amountPoints < 1 ? 1 : horizontalOnly ? 1 : Math.round(Math.sqrt(amountPoints));
  const positionsPerRow = amountPoints < 1 ? 1 : Math.ceil(amountPoints / rows);
  const heightPerRow = height / rows;
  const widthPerColumn = width / positionsPerRow;

  const startX = topLeftPoint[0];
  const startY = topLeftPoint[1];

  const positions = [];
  for(let k = 1 ; k <= amountPoints; k++) {
    const currentRowIndex = Math.ceil(k / positionsPerRow - 1);
    const currentColumnIndex = (k - 1) % positionsPerRow;

    /**
     * Add 0.5 because of center position of barrel
     */
    const x = startX + (currentColumnIndex + 0.5) * widthPerColumn;
    const y = startY + (currentRowIndex + 0.5) * heightPerRow;

    const position = [x, y];
    positions.push(position);
  }

  return positions;
}

//returns true or false if pointA and pointB are closer than maxDistance
//pointA, pointB - {"x":number, "y":number}
function arePointsCloserThanThat(pointA, pointB, maxDistance)
{
  let differenceX = Math.abs(pointA["x"]-pointB["x"]);
  let differenceY = Math.abs(pointA["y"]-pointB["y"]);
  if(Math.sqrt(differenceX*differenceX+differenceY*differenceY) < maxDistance)
  {
    return true;
  }
  else
  {
    return false;
  }
}


/**
* returns a container with the specified picture. The picture gets scaled to the specified width and height
* the bitmap can have a hitArea as an overlay
* the bitmap is at x=y=0
* the bitmap can be generated invisible
* @param {string} mtlgBitmap - a bitmap returned by MTLG.assets.getBitmap()
* @param {number} width - picture width
* @param {number} height - picture height
* @param {boolean} hitarea - if true, the picture will have a rectangular hitarea with its width and height
* @param {boolean} stealth - if true, the picture will spawn with visible = false
* @param {boolean} regXYOnTop - if true, the center of the picture (for rotation and movement) is on the midpoint of the top border
*/
function createPicture(mtlgBitmap, width=42, height=42, hitarea=false, stealth=false, regXYOnTop=false)
{

  //Bild lässt sich irgendwie nur im Container skalieren und zentrieren, ohne die hitArea falsch zu platzieren
  var picture1 = mtlgBitmap;
  var picture = new createjs.Container();
  picture.addChild(picture1);

  let widthmiddle = width/2;
  let heightmiddle = height/2;

  picture1.scaleX = width/picture1.image.width;
  picture1.scaleY = height/picture1.image.height;

  picture.regX = widthmiddle;
  if(!regXYOnTop)
  {
    picture.regY = heightmiddle;
  }
  else if(regXYOnTop)
  {
    picture.regY = 0;
  }


  if(hitarea)
  {
      var hitBox = new createjs.Shape();
      hitBox.graphics.beginFill("#FFFFFF").drawRect(0,0,width,height);

      //alpha = 0 makes this ignore click events
      // so 0.01 is the least visible but working overlay
      hitBox.alpha = 0.01;
      picture.hitArea = hitBox;

      picture.addChild(hitBox);
  }

  if(stealth)
  {
     picture.visible = false;
  }

  return picture;
}

//this function checks if a dropArea got hit after a url got released at hitPoint {"x": ,"y":}
function isDropAreaHit(hitPoint)
{

  //let debugshape= new createjs.Shape();
  //debugshape.graphics.beginFill("red").drawRect(hitPoint["x"], hitPoint["y"], 15, 15);
  //debugshape.alpha = 0.5;
  //MTLG.getStageContainer().addChild(debugshape);

  let dropAreasViews = levelController.getDropAreasViews();
  for(let i in dropAreasViews)
  {
    //let debugshape2= new createjs.Shape();
    //debugshape2.graphics.beginFill("red").drawCircle(dropAreasViews[i].getPosition()["x"], dropAreasViews[i].getPosition()["y"], 100);
    //debugshape2.alpha = 0.1;
    //MTLG.getStageContainer().addChild(debugshape2);

    if(arePointsCloserThanThat(hitPoint, dropAreasViews[i].getPosition(), 100))
    {
      return i;
    }
  }
  return -1;
}



//returns a url-string split into its parts using functions of check.js
// examle input: "https://www.paypal.com/de/home"
//example output: 'url': {'scheme': "https://", 'host': "paypal.com",'path': "/de/home"},
function splitUrlStringIntoUrlParts(urlString)
{
  let result = {'url':{}, 'url_key_view':{}};

  //contains information to easily check the url for every possible part. To check more parts more lines can be added
  let _urlPartFunctions = {
    'scheme':{'fct': urlUtils.getUrlScheme, "suffix": "//", 'parttype': 'scheme'},
    'host': {'fct': urlUtils.getUrlHostname, 'parttype': 'host'},
    'path': {'fct': urlUtils.getUrlPathname, 'parttype': 'path'},
  };


  let urlPart = "";
  let urlKeyViewPart = "";
  for(let a in _urlPartFunctions)
  {
    urlPart = "";
    urlPart = ((_urlPartFunctions[a]["fct"]).bind(this))(urlString); //without binding this, urlUtils.testUrl isnt available

    if(urlPart == "" || urlPart == "/") //some functions return "" or "/" when url part isnt there
    {
      break;
    }
    //add suffix. If suffix is meant to fix sth that wouldnt be there otherwise this needs to be done before the previous if statement
    if(_urlPartFunctions[a]["suffix"])
    {
      urlPart += _urlPartFunctions[a]["suffix"];
    }

    //url has this part
    result['url'][_urlPartFunctions[a]['parttype']] = urlPart;
    Object.assign(result['url_key_view'], _urlPartFunctions[a]['viewtype']);

  }

  return result;
}


//adds url view parts to an obj containing an url split into its parts. Uses functions of check.js
//input obj can contain any keys as those will be kept in the result. 'key' key is the one this function looks for. 'url_key_view' is the key this function will add
// ouput of splitUrlStringIntoUrlParts is valid input for this funtion
// example input: {'url': {'scheme': "https://", 'host': "evil.com",'path': "/benign/login/secure"}}
//example output: {'url': {'scheme': "https://", 'host': "evil.com",'path': "/benign/login/secure"},
//   url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'host':{'shownName':'url-host', 'shownOn':['host'], 'shownAbove':['host']},'path':{'shownName':'url-path', 'shownOn':['path'],'shownAbove':['path']}}}
function addUrlViewToUrlParts(urlPartsObj)
{
  let result = {...urlPartsObj, 'url_key_view':{}};

  //contains information to easily check the url for every possible part. To check more parts more lines can be added
  let _urlPartFunctions = {
    'scheme':{'parttype': 'scheme', "viewtype": {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}}},
    'host': {'parttype': 'host', "viewtype": {'host':{'shownName':'url-host', 'shownOn':['host'], 'shownAbove':['host']}}},
    'path': {'parttype': 'path', "viewtype": {'path':{'shownName':'url-path', 'shownOn':['path'],'shownAbove':['path']}}},
  };

  let urlKeyViewPart = "";
  for(let a in _urlPartFunctions)
  {
    if(urlPartsObj['url'][_urlPartFunctions[a]['parttype']])
    {
      Object.assign(result['url_key_view'], _urlPartFunctions[a]['viewtype']);
    }

  }

  return result;
}
