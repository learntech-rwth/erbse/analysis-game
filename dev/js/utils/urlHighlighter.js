
//builds a url with highlightable parts
//copied from createngame tutorial and turned into a function
//example: paramObj = {'urlview': {  'url': {'scheme': "https://", 'subdomain':"benign",'domain': "com-evil",'tld': "ml"},
//                        'url_key_view': {'subdomain':{'shownName':'url-subdomain', 'shownOn':['subdomain'],'shownAbove':['subdomain']}, 'domain':{'shownName':'url-rd', 'shownOn':['domain'], 'shownAbove':['domain','tld']}},
//                        'url_explanation_on': ['subdomain']
//                                },
//                    'position': {"centerX": MTLG.getOptions().width * 0.55, "centerY": 700} //where to place the url objects
//                    }
// @return array of the created url objects. Can be used to delete them
function urlHighlighter(paramObj)
{
      let params = paramObj["urlView"];


      // logic URL parts
      this._urlParts =  [];
      this._urlPartsView = [];

      // container for URL-block, URL-keys and explanation
      this._urlPartsViewContainer = new createjs.Container();
      this._urlPartsViewKeyContainer = new createjs.Container();
      this._urlPartsViewExplanationContainer = new createjs.Container();
      this._urlPartsViewExplanationContainer.visible = false;

      // save the length-sum of the URL-parts
      let urlLength = 0;

      // logic url parts
      for(let key in params['url']){
        let newUrlPart = new urlPart(params['url'][key]);
        newUrlPart.key = key;
        this._urlParts.push(newUrlPart);

        // check for tld and later eltd - new slash is assosiacted to path and no longer a single sign
        //if(key == 'tld' || key == 'etld' || key == 'host'){
        //  let newUrlPartSlash = new urlPart("/");
        //  newUrlPartSlash.key = key  + '-slash';
        //  this._urlParts.push(newUrlPartSlash);
        //}

        // Check for domain to add a dot
        if(key == 'domain' || key == 'subdomain'){
          let newUrlPartDot = new urlPart(".");
          newUrlPartDot.key = key + '-dot';
          this._urlParts.push(newUrlPartDot);
        }
      }

      // view of url parts
      for(let urlPart of this._urlParts){
        let newUrlPartView;
        newUrlPartViewTutorial = new urlPartViewTutorial(urlPart, urlPart.key, urlLength, 0);
        urlLength += newUrlPartViewTutorial.width;

        // push and add
        this._urlPartsView.push(newUrlPartViewTutorial);
        this._urlPartsViewContainer.addChild(newUrlPartViewTutorial._view);

      }

      // position all container after the URL-view generated
      //var positionContainer = {x: (MTLG.getOptions().width - urlLength) / 2, y: 800};
      let xPos = (paramObj["position"]["centerX"] ? paramObj["position"]["centerX"]- urlLength*0.5 : (MTLG.getOptions().width - urlLength));
      let yPos = (paramObj["position"]["centerY"] ? paramObj["position"]["centerY"] : 800);

      var positionContainer = {x: xPos, y: yPos};
      this._urlPartsViewContainer.x = positionContainer.x;
      this._urlPartsViewContainer.y = positionContainer.y;
      this._urlPartsViewKeyContainer.x = positionContainer.x;
      this._urlPartsViewKeyContainer.y = positionContainer.y - 80;
      this._urlPartsViewExplanationContainer.x = 0;
      this._urlPartsViewExplanationContainer.y = 0;


      // create the keys shown on hover
      if(params['url_key_view']){
        // first look for keys shownAbove more than one URL-part and how often for every single URL-Part
        var multipleShow = [];
        var singleShow = [];
        var keyCount = {};

        // get URL-parts displayed
        for(let urlPart of this._urlPartsView){
          keyCount[urlPart.key] = 0;
        }

        for(let urlPart of this._urlPartsView){
          // check for hover keys
          if(params['url_key_view'][urlPart.key]){
            urlPart.shownName = MTLG.lang.getString(params['url_key_view'][urlPart.key]['shownName']);
            urlPart.shownOn = params['url_key_view'][urlPart.key]['shownOn'];
            urlPart.shownAbove = params['url_key_view'][urlPart.key]['shownAbove'];

            // sort ULR-Parts after key ist shown on multiple parts or not
            if(urlPart.shownAbove.length >= 2){
              multipleShow.push(urlPart);
            }
            else{
              singleShow.push(urlPart);
            }

            // sum up
            for(let key of urlPart.shownOn){ // old: shownAbove
              keyCount[key] += 1;
            }

          }
        }

        // check for elements more than one key is displayed above
        var maxShownOn = 0;
        var maxMultipleShow = 0;
        for(let key in keyCount){
          if(keyCount[key] > maxShownOn){
            maxShownOn = keyCount[key];
          }
        }
        for(let urlPart of multipleShow){
          if(urlPart.shownAbove.length > maxMultipleShow){
            maxMultipleShow = urlPart.shownAbove.length;
          }
        }

        // check for valid show
        var validConfig = false;
        if(maxShownOn <= 2 && maxMultipleShow <= 2){
          validConfig = true;
        }

        // switch for displaying mode of keys
        switch(validConfig){
          case true:
            keyView();
            break;
          default:
            console.log("Keine zugelassene Konfiguration."+
            "Zu einem URL-Part können maximal 2 Keys gleichzeitig angezeigt werden. Momentanes Maximum: " + maxShownOn + "\n" +
            "Maximal zulässige Anzahl shownAbove sind 2 Keys. Momentanes Maximum: " + maxMultipleShow);
        }

        // generate key view
        function keyView(){
          // keys with shownAbove > 2 go upper URL
          // generate upper keys first
          for(let urlPart of multipleShow){
            // compute position of label and line
            // find first second URL-part for multipleShow - distinct element after validConfig
            let secondPart = this._urlPartsView.find(part => urlPart.shownAbove.indexOf(part.key) != -1 && part.key != urlPart.key);

            let startPartX = 0;
            let distanceParts = 0;

            if(urlPart._view.x <= secondPart._view.x){
              startPartX = urlPart._view.x;
              distanceParts = (secondPart._view.x + secondPart.width) - urlPart._view.x;
            }else{
              startPartX = secondPart._view.x;
              distanceParts = (urlPart._view.x + urlPart.width) - secondPart._view.x;
            }

            let keyView = new urlPartViewKey(urlPart, startPartX, distanceParts, 'top');

            // add to container
            this._urlPartsViewKeyContainer.addChild(keyView._view);
          }


          // now single downside keys
          for(let urlPart of singleShow){
            const keyView = new urlPartViewKey(urlPart, urlPart._view.x, urlPart.width, 'bottom');

            // add to container
            this._urlPartsViewKeyContainer.addChild(keyView._view);
          }

        }


      }

      // Add explanation if one is provided
      if(params['url_explanation_on']){
        this._boxUrlText = createBox(310, 500, 1300, MTLG.getOptions().height * 0.45, {bgColor:"#DCC08B"});
        this._urlPartsViewExplanationContainer.addChild(this._boxUrlText);
        this._urlText = createText(MTLG.lang.getString(params['url-text']), {lineWidth: 1300, txtColor: "#FFFFFF", padding:{lr:20, tb:20}});
        this._urlText.x = 310;
        this._urlText.y = 500;
        this._urlPartsViewExplanationContainer.addChild(this._urlText);

      }


      // Ticker for Hit with URL-parts
      if(this._urlPartsViewKeyContainer.children.length > 0){
        var tickerModeTwo = createjs.Ticker.on("tick", tickMouse.bind(this));
        this.tickerArray.push(tickerModeTwo);
      }

      function tickMouse(event){
        var showExplanation = false;

        const oldVisible = [];

        // first reset visibility of all URL-parts and explanation
        for(urlKeyView of this._urlPartsViewKeyContainer.children){
          if (urlKeyView.visible) {
            oldVisible.push(urlKeyView)
          }
          urlKeyView.visible = false;
        }
        this._urlPartsViewExplanationContainer.visible = false;


        // check for hits
        // has to let, because of constructor with the same name
        for(let urlPartView of this._urlPartsView){
          let point = urlPartView._view.globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY);

          if(urlPartView._view.hitTest(point.x, point.y) && urlPartView.shownOn){
            let associatedParts = this._urlPartsView.filter(part => urlPartView.shownOn.indexOf(part.key) != -1);

            // show the key
            for(part of associatedParts){
              let associatedContainer = this._urlPartsViewKeyContainer.children.filter(keyView => keyView.key == part.key);
              for(container of associatedContainer){
                if (!oldVisible.includes(container)) {
                  _logger.log('UrlPart discovered')
                }
                container.visible = true;
              }
            }

            // show explanation if necessary
            if(params['url_explanation_on']){
              if(showExplanation == false){
                if(params['url_explanation_on'].indexOf(urlPartView.key) != -1){
                  this._urlPartsViewExplanationContainer.visible = true;
                  showExplanation = true;

                  // Explanation has been shown - continue is now allowed //TODO: doesnt exist in this version, but maybe in future version
                  if(continueAllowed === false){
                    continueAllowed = true;
                    allowContinuation();
                  }
                }
              }
            }
          }
        }
      }

      // add container to stage
      this.stage.addChild(this._urlPartsViewContainer);
      this.stage.addChild(this._urlPartsViewKeyContainer);
      this.stage.addChild(this._urlPartsViewExplanationContainer);

      let objArray = [];
      objArray.push(this._urlPartsViewContainer);
      objArray.push(this._urlPartsViewKeyContainer);
      objArray.push(this._urlPartsViewExplanationContainer);

      return objArray;
}
