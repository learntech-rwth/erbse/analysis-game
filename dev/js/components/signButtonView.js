
  /**
  * creates a sign with text and a click function and returns it
  * @param {number} x - x Position
  * @param {number} y - y Position
  * @param {function} clickFunction - the on click function
  * @param {string} text - text on the sign. The texts position relative to the sign picture is specified in game.config
  * @param {number} facing - 0 for right, 1 for left
  * @param {number} width - width of the created sign
  * @param {number} height - height of the created sign
  */
  function signButtonView ({
    dimension: { x, y },
    handler: clickFunction = undefined,
    text = undefined,
    id: identifier = text,
    facing = 0,
    width = 290,
    height = 166,
    add = true
  }) {
    let signcontainer = new createjs.Container();
    let sign = createPicture(MTLG.assets.getBitmap("img/" + MTLG.getOptions().signImage),width, height, true);
    signcontainer.addChild(sign);
    signcontainer.cursor = "pointer"
    sign.x = 0;
    sign.y = 0;
    //container and sign need to have the same center point
    signcontainer.regX = sign.x;
    signcontainer.regY = sign.y;
    signcontainer.x = x;
    signcontainer.y = y;

    if(facing)
    {
      sign.scaleX = -1*sign.scaleX;
    }

    if(clickFunction)
    {
      signcontainer.on("click", function () {
        _logger.log(`Clicked SignButton "${identifier}"`)
        return clickFunction()
      });
    }

    signcontainer.on("mouseover", function () {
      _logger.log(`Mouseover SignButton "${identifier}"`)
    });

    signcontainer.on("mouseout", function () {
      _logger.log(`Mouseout SignButton "${identifier}"`)
    });


    if (text != undefined)
    {
      let label = new createjs.Text(text, MTLG.getOptions().signTextFont, "white");
      label.textAlign = 'center';
      label.textBaseline = "middle";
      let pos = MTLG.getOptions().signTextRelPosition;

      //vertically inverting the sign makes a correction of the x-position needed
      if(facing)
      {
        label.x = (0.5-pos[0])*width;
      }
      else
      {
        label.x = (pos[0]-0.5)*width;
      }

      label.y = (pos[1]-0.5)*height;
      signcontainer.addChild(label);
    }

    if (add) {
      MTLG.getStageContainer().addChild(signcontainer);
    }

    return signcontainer;
  }
