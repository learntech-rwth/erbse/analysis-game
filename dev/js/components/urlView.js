
//erstellt die geschlossene und geöffnete Url als ein sichtbares Objekt.

function urlView (serviceModel, x, y)
{
  let urlId = serviceModel.getUrlId();
  //floating speed of url elements
  let speed = MTLG.getOptions().urlMovementSpeed;
  //flowing accuracy of url elements
  let flowingAccuracy = 10;
  //checks if element is hovered over
  let hovered = false;
  //checks if element is dragged
  let dragged = false;

  //used to determine
  let countTillCheckNeeded = 0;
  let checkNeeded = 500 + getRandomInt(300);

  //used to control movement of the url
  let movingAngle = getRandomInt(359)*Math.PI/180;

  let flowingVar = setInterval(flowing, flowingAccuracy);

  //checks if url is closed
  let closed = true;
  let everOpened = false;

  let urlVisualSize = 100;
  let urlSize = urlVisualSize*0.5;

  //used in movement to check if highest/lowest point is reached
  let floatingEdgeTop = MTLG.getOptions().urlMoveArea["ymin"];
  let floatingEdgeBottom = MTLG.getOptions().urlMoveArea["ymax"];

  let isGamePaused = levelController.isGamePaused;

  //used to check how far a pressmove moved the container
  let pressmoveStartPosition = {};


  //closed url
  let coin = createPicture(MTLG.assets.getBitmap("img/"+ MTLG.getOptions().urlClosedImage), urlVisualSize, urlVisualSize, true)


  let _urlText = serviceModel.getUrlString();
  //url label, text on opened url
  if(_urlText.length > 100){
    _urlText = _urlText.substring(0, 100);
    _urlText += "...";
  }

  let label = new createjs.Text(_urlText + "\n\n" + MTLG.lang.getString("content") + ": " + serviceModel.getContext(), MTLG.getOptions().urlTextFont, "#000000");
  label.textAlign = "center";
  label.y = -30;
  label.visible = false;
  let labelWidth = label.getMeasuredWidth() / 1.2 + 10;

  //opened url
  let roll = createPicture(MTLG.assets.getBitmap("img/"+ MTLG.getOptions().urlOpenImage), labelWidth*1.2, 120, true)
  roll.visible = false;

  //surrounding container
  let container = new createjs.Container();
  container.x = x;
  container.y = y;
  container.cursor = 'pointer';

  container.addChild(coin);
  container.addChild(roll);
  container.addChild(label);

  function isMounted() {
    return MTLG.getStageContainer().getChildIndex(container) >= 0;
  }

  function isActive() {
    return isMounted() && levelController.isUrlIdStillActive(urlId);
  }

  //restarts url movement and ensures that movement prohibiting flags are resetted
  container.restartInterval = function () {
      // hovered = false;
      dragged = false;
      if(flowingVar)
      {
        clearInterval(flowingVar);
      }
      flowingVar = setInterval(flowing, flowingAccuracy);
  };

  //function for moving/flowing
  function flowing() {

    // stage.clear() removes urls from stage, but intervall keeps running which keeps the urls in memory
    // by this urls check regularly if they are still part of active urls and, if not the case, clears the intervall
    // so garbage collection removes them.
    // different timing for each url prevents a check-spike on a single frame
    countTillCheckNeeded++;
    if(countTillCheckNeeded == checkNeeded)
    {
      if(!isActive())
      {
        clearInterval(flowingVar);
      }
      countTillCheckNeeded = 0;
    }

    if (!isGamePaused()) {
      //is turned off while beeing dragged or hovered over
      if(!(dragged||hovered))
      {
          //move into direction
          container.x += Math.cos(movingAngle)*speed;
          container.y += Math.sin(movingAngle)*speed;

        //edge hit, change direction
        if ( container.x - urlSize <= 0) {
            movingAngle = Math.PI-movingAngle;
            container.x +=speed;
            //console.log("x < 0");
        } else if ( container.x + urlSize >= MTLG.getOptions().width) {
            movingAngle = Math.PI-movingAngle;
            container.x -=speed;
            //console.log("x > breite");
        } else if ( container.y - urlSize <= floatingEdgeTop ) {
            movingAngle = 2*Math.PI-movingAngle;
            container.y +=speed;
            //console.log("y > höhe");
        } else if ( container.y + urlSize >= floatingEdgeBottom ) {
            movingAngle = 2*Math.PI-movingAngle;
            container.y -=speed;
            //console.log("y < unten");
        }
      }
    }
  }

  //url has its own function for removal as it needs to clean up its interval
  this.remove = function()
  {
    if(flowingVar)
    {
      clearInterval(flowingVar);
    }
    MTLG.getStageContainer().removeChild(container);
  }


  //element dragged
  container.on("pressmove", function (evt) {
      if (!isGamePaused()) {
          if(!dragged)
          {
            pressmoveStartPosition["x"] = this.x;
            pressmoveStartPosition["y"] = this.y;
          }
          dragged = true;
          // currentTarget will be the container that the event listener was added to:
          let pt = evt.currentTarget.localToLocal(evt.localX, evt.localY, MTLG.getStageContainer());
          evt.currentTarget.set(pt);

      }
  });

  // Stop Coin Flowing
  container.on("rollover", function()
  {
    hovered = true;
    MTLG.getStageContainer().setChildIndex(container, MTLG.getStageContainer().numChildren - 1);
  });

  // Restart Coin Flowing
  container.on("rollout", function()
  {
    hovered = false;
  });

  //element dropped, either after drag or click
  container.on("pressup", function (evt) {
    if(!isGamePaused())
    {
      if(pressmoveStartPosition["x"] != undefined) //did pressmove occur
      {
        if(arePointsCloserThanThat(pressmoveStartPosition, {"x":this.x, "y":this.y}, MTLG.getOptions().urlPressmoveMinDistance)) // did pressmove go not far enough
        {
          clickReaction(evt);
        }
        pressmoveStartPosition["x"] = undefined;
      }
      // hovered = false;
      dragged = false;
      let pt = evt.currentTarget.localToLocal(evt.localX, evt.localY, MTLG.getStageContainer());
      let areaId = isDropAreaHit(pt);

      if (areaId != -1 && isActive())
      {
        if (!everOpened) {
          _logger.log(`URL "${_urlText}" with status "` + serviceModel.getKnowledge() + `" with context "` + serviceModel.getContext() + `" dropped without beeing opened.`);
        }
        levelController.processUrlOnDropArea(urlId, areaId);
      } else if (isActive()) {
        _logger.log(`URL "${_urlText}" with status "` + serviceModel.getKnowledge() + `" with context "` + serviceModel.getContext() + `" dragged and not dropped`);
      }
    }
  });

  //element clicked
  container.on("click", clickReaction );

  //is used to process a click or a short pressmove that should be treated like a click instead
  function clickReaction(evt)
  {
      //game not paused
      if (!isGamePaused())
      {
          //if element got dragged, click action isnt supposed to happen
          if (dragged)
          {
              // hovered = false;
              dragged = false;
          }
          //not dragged, click is supposed to happen
          else
          {
            // hovered = false;
            dragged = false;

            if (closed) //url has to open and stop
            {
              label.visible = true;
              roll.visible = true;
              coin.visible = false;
              clearInterval(flowingVar);
              closed = false;
              everOpened = true;
              _logger.log(`URL "${_urlText}" with status "` + serviceModel.getKnowledge() + `" opened`)
            }
            else  //url has to close and restart moving
            {
              label.visible = false;
              roll.visible = false;
              coin.visible = true;
              container.restartInterval();
              closed = true;
              _logger.log(`URL "${_urlText}" with status "` + serviceModel.getKnowledge() + `" closed`)
            }
          }
      }
  }

  MTLG.getStageContainer().addChild(container);
}
