/**
 * View of generated URL Part
 *
 * Extension of UrlPartView
 */
function urlPartViewTutorial(pUrlPart, key, x = 0, y = 0, options = null) {
  let defaultOptions = {
    bgColor: MTLG.getOptions().bgColor,
    txtColor: "#ffffff",
    padding:{lr:2, tb:6},
    alpha: 0,
    lineWidth: 500,
    lineHeight: 45
  };
  options = Object.assign(defaultOptions, options);

  this.urlPart = pUrlPart;
  this.key = key;
  this._view = createTextBoxTutorial(this.urlPart.label, options);
  this._view.x = x;
  this._view.y = y;
  this._view.key = key;
  this.width = this._view.getBounds().width;
  this.height = this._view.getBounds().height;

  this._view.on("mouseover", function () {
    _logger.log(`Mouseover UrlPart "${pUrlPart.key}" of value "${pUrlPart.label}"; category "${pUrlPart.category}"`)
  });

  this._view.on("mouseout", function () {
    _logger.log(`Mouseout UrlPart "${pUrlPart.key}" of value "${pUrlPart.label}"; category "${pUrlPart.category}"`)
  });

  // reassign text for the SAME element
  this.reassignText = function(urlPart, options){
    options = Object.assign(defaultOptions, options);
    this.urlPart = urlPart;
    this._view = createTextBoxTutorial(this.urlPart.label, options);
    this.width = this._view.getBounds().width;
    this.height = this._view.getBounds().height;
  }

  // give current display text
  this.currentText = function(){
    return this.urlPart.label;
  }

}

function urlPartViewKey(urlPartView, x, distanceLine, allocation = 'top'){

  //generate keyView
  this._view = new createjs.Container();
  this._view.key = urlPartView.key;
  this._view.visible = false;
  this._keyView = createTextBoxTutorial(urlPartView.shownName, {padding:{lr:8, tb:8}, fontSize: "30px Arial", bgColor: "#ffffff", lineWidth: urlPartView.width + 100});
  this._keyView.regX = this._keyView.getBounds().width / 2;

  this._view.addChild(this._keyView);

  // generate line above URL-parts
  this._lineView = new createjs.Shape();
  this._lineView.graphics.beginFill("#ff0000").drawRect(0, 0, distanceLine, 5);

  // compute exact position keyView
  this._keyView.x = distanceLine / 2;
  this._view.x = x;
  this._view.y = 0;

  if(allocation == 'top'){
    this._keyView.y = 15;
    this._lineView.y = 70;
  } else{
    this._keyView.y = 160;
    this._lineView.y = 140;
  }



  this._view.addChild(this._lineView);

}
