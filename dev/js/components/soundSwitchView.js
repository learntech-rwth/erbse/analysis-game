/**
* creates a visible sound on/off controller
* creates a container containing the sound on/off symbol. On-click sound will be turned on/off
* while sound is off, the sound symbol is crossed out
* @param {boolean} blackBackgroundVersion - if true, the sound symbol for black background is used
* @param {number} x - x Position
* @param {number} y - y Position
* @param {number} width - width of the picture
* @param {number} height - height of the picture
*/
function soundSwitchView(blackBackgroundVersion = false , x = 1862, y = 1012, width = 56, height = 56)
{
  let toneOff = new createjs.Shape();
  let c = Math.sqrt(width*width+height*height)

  if(blackBackgroundVersion)
  {
    toneOff.graphics.beginFill("white").drawRect(0, 0, width*0.125, c);
  }
  else
  {
    toneOff.graphics.beginFill("black").drawRect(0, 0, width*0.125, c);
  }
  toneOff.regX = width*0.0625;
  toneOff.regY = c*0.5;
  toneOff.rotation = 45;
  toneOff.visible = false;

  let picture = undefined;
  if(blackBackgroundVersion)
  {
    picture = MTLG.getOptions().blackBackgroundSoundSwitchImage;
  }
  else
  {
    picture = MTLG.getOptions().soundSwitchImage;
  }
  let toneButton = createPicture(MTLG.assets.getBitmap("img/" + picture), width, height, true)
  toneButton.x = x;
  toneButton.y = y;

  toneButton.addChild(toneOff);
  toneOff.x = width*0.5;
  toneOff.y = height*0.5;

  toneButton.addEventListener("click", function () {
      //toggle
      if(MTLG.getOptions().toneOn) {
          _logger.log("Player turned sound off");
          toneOff.visible = true;
          levelController.toneOff();
      }
      else {
          _logger.log("Player turned sound on");
          toneOff.visible = false;
          levelController.toneOn();
      }

  });
  if (!MTLG.getOptions().toneOn) {
      toneOff.visible = true;
  }

  MTLG.getStageContainer().addChild(toneButton);
}
