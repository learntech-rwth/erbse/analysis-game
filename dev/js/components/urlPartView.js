/**
 * View for URL part
 */
function urlPartView (pUrlPart, id, x = 0, y = 0, stage = null, options = null) {

    let defaultOptions = {
      bgColor: "#ffffff",
      txtColor: "#ff7700",
      padding: {
        lr:10,
        tb:6
      }
    }
    options = Object.assign(defaultOptions, options);

    this.urlPart = pUrlPart;
    this._view = createTextBox(this.urlPart.label, options);
    this._view.x = x;
    this._view.y = y;
    this._view.viewObject = id;
    this.width = this._view.getBounds().width;
    this.height = this._view.getBounds().height;

    this.align = (offset) => {
      let urlBarAreaView = _globalController.getUrlBarAreaView();
      let urlBarView = _globalController.getUrlBarView();
      let padding = {
        x: 2,
        y: 4
      };
      if (offset == 0) {
        this._view.x = urlBarAreaView._container.x + urlBarView.x + padding.x;
      } else {
        this._view.x = offset;
      }
      this._view.y = urlBarAreaView._container.y + urlBarView.y + padding.y;
    }

    stage.addChild(this._view);

    this.getX = () => {
      return this._view.x;
    }

    this.getY = () => {
      return this._view.y;
    }

    this.getId = () => {
      return this._view.viewObject;
    };

    this.setCoords = ({x,y}) => {
      this._view.x = x;
      this._view.y = y;
    }
  }
