
// creates a visible timer at the specified position, which counts down timeInSeconds each second
// if timeInSeconds reaches 0, the intervall managing the countdown gets stopped and the callback function is called
// the timer remains visible
// gameTimer.cancel() exists to stop the timer early without calling the callbackfunction
function gameTimer(x, y, timeInSeconds, callbackFunction, fktToCheckIfGamePaused = function() {return false;}, labelFont ="80px Arial", labelColor = "#783E2B")
{

  //label erstellen
  let countdownLabel = new createjs.Text("--:--",labelFont, labelColor);

  countdownLabel.textAlign = "center";
  countdownLabel.x = x;
  countdownLabel.y = y;

  MTLG.getStageContainer().addChild(countdownLabel);

  let countDownInterval = setInterval(function () {

    if(! fktToCheckIfGamePaused())
    {
      timeInSeconds -= 1;
      countdownLabel.text = formatTime(splitTime(timeInSeconds));

      if(timeInSeconds <= 0)
      {
        stopTimer();
        callbackFunction();
      }
    }

  }, 1000)

  this.cancel = function()
  {
    stopTimer();
  }

  function stopTimer()
  {
    clearInterval(countDownInterval);
  }

  // returns an amount of hours, minutes and seconds that are equal to the param totalSeconds
  // minutes and seconds are always returned as double digit numbers
  // hours is always an at least double digit number
  // @return {} - {"h": hours, "m": minutes, "s": seconds}
  function splitTime(totalSeconds)
  {
    let hours = Math.floor(totalSeconds / 3600);
    totalSeconds -= hours*3600;
    if(hours < 10) {hours = "0" + hours}

    let minutes = Math.floor(totalSeconds / 60);
    totalSeconds -= minutes*60;
    if(minutes < 10) {minutes = "0" + minutes}

    let seconds = totalSeconds;
    if(seconds < 10) {seconds = "0" + seconds}

    return {"h": hours, "m": minutes, "s": seconds};

  }

  // connects the output of function splitTime together in a single string. hh is only shown when different to 0, mm:ss are aleays shown
  // return string timeString - "00:00"
  function formatTime(splittedTime)
  {
    let timeString = "";
    if (splittedTime["h"] != "00")
    {
      timeString += splittedTime["h"] + ":";
    }

    timeString += splittedTime["m"] + ":";
    timeString += splittedTime["s"];

    return timeString;
  }




}
