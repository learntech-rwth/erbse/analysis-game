//creates the background
function backgroundView(container) {
  let width = MTLG.getOptions().width;
  let height = MTLG.getOptions().height;

  let background_image = createPicture(MTLG.assets.getBitmap("img/background_training.jpg"), width*1.5, height*1.5);  //it doesnt fit without the factor, even though it should

  //TODO: this other background is not needed atm, but may be used in the future
  //background_image = createPicture(MTLG.assets.getBitmap("img/background_end.jpg"),1920, 1285)

  background_image.x = height/2;
  background_image.y = width/2;

  container.addChild(background_image);

  let topRowBackgroundWidth = width*0.2;
  let topRowBackgroundHeight = height*0.2;

  for (let i=0;i<9;i++)
  {
    let displayBackground = createPicture(MTLG.assets.getBitmap("img/wall.jpg"),topRowBackgroundWidth, topRowBackgroundHeight)
    displayBackground.x = topRowBackgroundWidth/2 + i*topRowBackgroundWidth*0.53125;
    displayBackground.y = topRowBackgroundHeight/2;
    container.addChild(displayBackground);
  }

}
