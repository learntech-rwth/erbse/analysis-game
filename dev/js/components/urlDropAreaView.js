
  /**
   * Creates drop area with helper
   *
   * @param {*} _dropAreaId
   * @param {*} x
   * @param {*} y
   * @param {*} type
   * @param {*} barrelName
   * @param {*} infoString
   * @param {*} barrelwidth
   * @param {*} barrelheight
   * @param {*} infodiameter
   */
  function urlDropAreaView(_dropAreaId, x, y, type, barrelName = 'MISSING NAME', infoString = "MISSING INFO", barrelwidth = 144, barrelheight = 158, infodiameter = 20 )
  {
    let dropAreaId = _dropAreaId;

    /**
     * Returns ID of drop area
     */
    this.getDropAreaId = function(){
      return _dropAreaId;
    }

    /**
     * Returns position of drop area
     */
    this.getPosition = function() {
      return {
        "x": visualscontainer.x,
        "y": visualscontainer.y
      };
    }

    let visualscontainer = new createjs.Container();
    visualscontainer.x = x;
    visualscontainer.y = y;

    let barrel = createPicture(MTLG.assets.getBitmap("img/" + MTLG.getOptions().urlContainerVisualImage), barrelwidth, barrelheight, true);
    barrel.x = 0;
    barrel.y = 0;
    visualscontainer.addChild(barrel);

    //Beschriftung der barrel
    let label = new createjs.Text(barrelName, MTLG.getOptions().urlContainerVisualTextFont, "#FFFFFF");
    label.textAlign = "center";
    label.textBaseline = "top";
    let pos = MTLG.getOptions().urlContainerVisualTextRelPosition;
    let labelHeight = label.getMeasuredHeight();
    label.x = 0;
    label.y = -labelHeight*0.5;
    visualscontainer.addChild(label);

    // Create helper icon and text box
    let helper = new urlDropAreaHelper(barrelName, -0.875*barrelwidth, 0.625*barrelheight, barrelwidth*1.75, infodiameter, infoString);
    visualscontainer.addChild(helper);

    const moveUp = function (element, length, time, step) {
      const opacityThreshold = 0.05;
      const numberOfSteps = time / step;
      const opacityStep = (element.alpha - opacityThreshold) / numberOfSteps;
      const movingStep = length / numberOfSteps;

      let timeout = null
      const iterate = function () {
        if (timeout) {
          clearTimeout(timeout);
        }
        setTimeout(move, step);
      }
      const move = function () {
        element.alpha = element.alpha - opacityStep;
        element.y -= movingStep;

        if (element.alpha <= opacityThreshold) {
          visualscontainer.removeChild(element);
        } else {
          iterate();
        }
      }

      iterate();
    }

    const bubbeling = function (color) {
      const numberOfBubbles = 20;
      const timeOfBubble = 700; // ms
      const timeStep = 50; // ms

      const createBubble = function (x = 0) {
        const length = barrelheight * 0.1 + getRandomInt(Math.ceil(0.45 * barrelheight));

        const bubble = new createjs.Shape();
        bubble.graphics.beginFill(color).drawCircle(x * barrelwidth, -barrelheight/2, barrelwidth * 0.05);
        bubble.alpha = 1;

        visualscontainer.addChild(bubble);
        moveUp(bubble, length, timeOfBubble, timeStep);
      }

      for (let i = 0; i < numberOfBubbles; i++) {
        const x = (-40 + getRandomInt(80)) / 100;
        createBubble(x)
      }
    }

    const showPoints = function (amount, color) {
      const timeOfPoints = 1200; // ms
      const timeStep = 50; // ms
      const length = barrelheight * 0.2 + getRandomInt(Math.ceil(0.6 * barrelheight));

      let infoText = new createjs.Text(`+${amount}`, MTLG.getOptions().pointsReactionTextFont, color);
      infoText.alpha = 1;
      infoText.x = barrelwidth * 0.4;
      infoText.y = -barrelheight * 0.4;

      visualscontainer.addChild(infoText);
      moveUp(infoText, length, timeOfPoints, timeStep);
    }

    //visualscontainer.showSmiley = 0;
    this.showReaction = function (reaction = 0)
    {
      const colors = {
        0: '#ff0000',
        1: '#ffaf00',
        2: '#08c712',
        3: '#333333'
      }

      bubbeling(colors[reaction]);
      switch(reaction)
      {
        case 1:
          amount = MTLG.getOptions().pointsForRightTendency;
          showPoints(amount, colors[reaction]);
          break;
        case 2:
          amount = MTLG.getOptions().pointsForRightType;
          showPoints(amount, colors[reaction]);
          break;
        case 0:
          showPoints(0, colors[reaction]);
          break;
      }
    }

    visualscontainer.on("mouseover", function () {
      _logger.log(`Mouseover UrlDropAreaView "${barrelName}"`)
    });

    visualscontainer.on("mouseout", function () {
      _logger.log(`Mouseout UrlDropAreaView "${barrelName}"`)
    });

    if (type == "skip-bucket") {
      visualscontainer.alpha = 1;
      let matrix = new createjs.ColorMatrix();
      matrix.adjustSaturation(-85);
      matrix.adjustBrightness(-25);
      matrix.adjustContrast(-15);
      barrel.filters = [
        new createjs.ColorMatrixFilter(matrix)
      ];
      barrel.cache(0,0, barrelwidth, barrelheight)
    }

    MTLG.getStageContainer().addChild(visualscontainer);

  }



  /**
   * Creates a helper icon and text box
   *
   * @param {number} x
   * @param {number} y
   * @param {number} width
   * @param {number} diameter
   * @param {string} infoString
   */
  function urlDropAreaHelper(barrelName, x,y, width, diameter, infoString) {

    console.log(width);

    let container = new createjs.Container();

    container.x = x;
    container.y = y;

    // Set measurement variables
    let lineWidth = width * 0.9;

    // Create circle with ? marker inside
    let infoCircle = new createjs.Shape();
    infoCircle.graphics.beginStroke("black").beginFill("yellow").drawCircle(0, 0, diameter);
    infoCircle.cursor = "pointer";

    let infoLabel = new createjs.Text("?", MTLG.getOptions().infoTextFont, "black");
    infoLabel.text.bold(); //das tuts nicht
    infoLabel.textAlign = "center";
    infoLabel.textBaseline = "middle";
    infoLabel.cursor = "pointer";

    // Create helper text
    let infoText = new createjs.Text(infoString, MTLG.getOptions().infoTextFont, "black");
    infoText.textAlign = "start";
    infoText.textBaseline = "top";
    infoText.lineWidth = lineWidth;

    // Compute height of helper text
    infoText.mHeight = infoText.getMeasuredHeight();
    let boxHeight = infoText.mHeight*1.2;

    // Create box for helper text
    let infoBlock = new createjs.Shape();
    infoBlock.graphics.beginStroke("black").beginFill("white").drawRect(0, 0, width, boxHeight);
    infoBlock.x = 0;
    infoBlock.y = -(boxHeight+2*diameter);

    // Set position of helper text
    infoText.x = infoBlock.x+0.05*width;
    infoText.y = infoBlock.y+0.1*infoText.mHeight;

    // Set isOpen to false (helper text is not shown)
    infoCircle.isOpen = false;

    // Set visibility of infoText and infoBlock false because isOpen is false
    infoText.visible = false;
    infoBlock.visible = false;

    // Set onClick method
    infoCircle.on("click", function () {
      if (infoBlock.visible) {
        infoLabel.text = "?";
        infoBlock.visible = false;
        infoText.visible = false;
        _logger.log(`Player closed information (i) for type "${type}"`);
        levelController.resumeGame();
      } else {
        infoLabel.text = "X";
        infoBlock.visible = true;
        infoText.visible = true;
        levelController.pauseGame();
        _logger.log(`Player opens information (i) for type "${type}"`);
      }
    });

    infoCircle.on("mouseover", function () {
      _logger.log(`Mouseover InfoCircle "${barrelName}"`)
    });

    infoCircle.on("mouseout", function () {
      _logger.log(`Mouseout InfoCircle "${barrelName}"`)
    });

    container.addChild(infoCircle);
    container.addChild(infoLabel);
    container.addChild(infoBlock);
    container.addChild(infoText);

    return container;
  }
