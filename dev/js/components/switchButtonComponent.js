function switchButtonComponent (text, leftOption, rightOption, { x = 0, y = 0 } = {}) {
  const switchContainer = new createjs.Container();
  switchContainer.x = 0;
  switchContainer.y = 50;

  let left;
  let right;

  const sizeX = MTLG.getOptions().width * 0.1;
  const sizeY = 70;

  const _button = op => {
    const option = {
      text: MTLG.lang.getString(op.text),
      size: { width: sizeX, height: sizeY },
      handler: () => {
        op.callback();
        update();
      }
    };

    if (op.isSelected()) {
      option.background = 'red';
    }

    return buttonComponent(option);
  }

  const update = () => {
    switchContainer.removeAllChildren();

    left = _button(leftOption);
    left.x = -(sizeX);
    switchContainer.addChild(left);

    right = _button(rightOption);
    right.x = sizeX;
    switchContainer.addChild(right);
  }

  /**
   * Wrapper for Switch
   */
  const container = new createjs.Container();
  container.x = x;
  container.y = y;

  container.addChild(switchContainer);

  /**
   * Text Object
   */
  textObject = MTLG.utils.uiElements.addText({
    color: "white",
    font: "40px bold Arial",
    text
  });
  textObject.x = -textObject.getBounds().width /2;
  textObject.y = -10;
  container.addChild(textObject);

  update();
  container.update = update;

  return container;
}