function buttonComponent ({
    text,
    size: { width, height },
    background: backgroundColor = 'white',
    color: textColor = 'black',
    handler
  }) {

  if (!handler) {
    handler = () => console.warn('No Handler for Button defined');
  }

  const x = -width / 2;
  const y = -height / 2;

  const container = new createjs.Container();
  const background = new createjs.Shape();
  const label = new createjs.Text(text, '25px Arial', textColor);

  background.x = x;
  background.y = y;
  background.width = width;
  background.height = height;

  background.graphics.beginFill(backgroundColor);
  background.graphics.drawRect(0, 0, width, height);
  background.graphics.endFill();

  label.textAlign = "center";
  label.textBaseline = "middle";
  label.x = 0;
  label.y = 2;

  container.addChild(background);
  container.addChild(label);

  container.setChildIndex(background, 2);
  container.setChildIndex(label, 1);

  container.cursor = 'pointer';
  container.on('click', handler);

  return container;
}