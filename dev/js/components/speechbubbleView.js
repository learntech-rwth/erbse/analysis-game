//creates a speechbubble containing the specified text, centered on its position
//params = {"width":, "height": ,"x":, "y":, }
function speechbubbleView(contentText, params)
{

  let text = new createjs.Text(contentText, MTLG.getOptions().speechbubbleTextFont, "black");
  text.textAlign = "center";
  text.textBaseline = "middle";
  text.lineWidth = params["width"]*0.9;
  let textHeight = text.getMeasuredHeight();

  let speechbubbleHeight = undefined;
  if(params["height"])
  {
    //console.log("textHeight " + textHeight +" vs params[height] " + params["height"]);
    if(textHeight * 1.5 > params["height"])
    {
      speechbubbleHeight = textHeight * 1.5
      console.log("speechbubble too small, adjusting height");
    }
    else {
      speechbubbleHeight = params["height"];
    }
  }
  else {
    speechbubbleHeight = textHeight * 1.5;
  }

  let speechbubbleWidth = params["width"];

  //correct center position offset of the speechbubble picture
  text.x = params["x"] + speechbubbleHeight* (MTLG.getOptions().speechbubbleDownCenterRelPosition[0]-0.5);

  //correct text height and correct center position offset of the speechbubble picture
  text.y = params["y"] - textHeight*0.5 + speechbubbleHeight* (MTLG.getOptions().speechbubbleDownCenterRelPosition[1]-0.5);

  let speechbubble = createPicture(MTLG.assets.getBitmap("img/speechbubble.png"),speechbubbleWidth, speechbubbleHeight);
  speechbubble.x = params["x"];
  speechbubble.y = params["y"];

  let container = new createjs.Container();
  container.addChild(speechbubble);
  container.addChild(text);

  MTLG.getStageContainer().addChild(container);

  return container;
}
