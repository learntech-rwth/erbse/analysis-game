/**
 * DOCUMENTED BASIC LEVEL EXAMPLE
 *
 *
 * Each Leve
 */
const payload = {}
const exampleLevel = {
    // REQUIRED Level ID
    id: 'exampleLevel',

    // OPTIONIAL Parent Level ID
    // If defined the official Level ID is `parentId.id`
    parent: null,

    // REQUIRED Method
    init: null, // defined below

    // OPTIONAL Method if additional constraints need to be met
    check: null, // defined below

    transitions: {      // Explicit Level Transitions

        // [transitionName]: Destination Level
        transitionName: 'exampleLevel',

        // if for this transition e.g. something needs to be reset
        alternativeTransition: { id: 'exampleLevel', ...payload }
    }
}

exampleLevel.init = function( gameState, { navigate, save, background }) {
    /**
     * navigate: @type object
     * save: @type function
     * background: @type createjs.Container
     */

    /**
     * OBJECT: navigate
     * navigate holds all defined transitions for this level
     * this means explicit defined transitions AND Auto transitions
     *
     * Auto transitions are
     * navigate.toParent(): navigate to Parent Level (if defined)
     * navigate.toChildId(): navigate to Child Level (if defined)
     * navigate.nextSlide(): navigate to same Level but with incremented slide property
     * navigate.lastSlide(): navigate to same Level but with decremented slide property
     *
     * Explicit transitions would be using the defined example
     * navigate.transitionName()
     * navigate.alternativeTransition()
     *
     * A transition function can get an payload that will be
     * added and saved to the gamestate during the transition.
     * Example:
     * navigate.transitionName({ lol: true })
     *
     * Important Note:
     * Ensure to leave the Level Init Function when transition with navigate.[x]()
     * e.g. `return navigate.toParent()`
     */

    /**
     * FUNCTION: save
     * saves a submitted gameState as the current gameState
     *
     * Example:
     * save(gameState)
     */

    /**
     * CONTAINER: background
     * a conainer added to the current stage, that gets persisted during slide transitions.
     * this container will be RESET after each REAL level transition
     *
     * A REAL level transition is defined as `lastLevel.id !== nextLevel.id || slide === 0`
     *
     */

    const { slide, level } = gameState;
    /**
     * NUMBER: slide
     * slide indicate on what `slide` a level currently should be displayed
     * This CAN be used if multiple pages need to be displayed
     * If slide is set to 0 the level should be considered as fresh restarted
     *
     * This number gets actively set to `0` by the gameController with
     * any REAL level transition.
     */

     /**
      * OBJECT: level
      * Object to store level specific information
      * Because no `Global Variables` per Level are possible using slides,
      * Level Specific Information should be stored here.
      *
      * This Object gets reset after each REAL level transition
      */
}

exampleLevel.check = function(gameState) {
    /**
     * This Method gets only evaluated to a boolean by the gameController
     */

    /**
     * If special constraint are met
     */
    return true;

    /**
     * Else
     */
    return false;
}