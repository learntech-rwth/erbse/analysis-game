if (typeof globalLevels === 'undefined') {
  globalLevels = {}
}

globalLevels.initCoinLevel = function (gameState, { navigate, save, background }) {
  levelController.reset();

  const stage = MTLG.getStageContainer();
  const options = MTLG.getOptions();

  stage.mouseMoveOutside = true;

  /**
   * Functions ans important objects
   */
  const levelFinished = () => {
    const payload = {};
    payload.player = levelController.getPlayer()
    //OLD CODE - BEGIN
    payload.player.addLevelToHistory({ "levelData": levelController.getLastLvlData() });
    payload.timesLevelPlayed = gameState.timesLevelPlayed == undefined ? 1 : gameState.timesLevelPlayed + 1;

    payload.lastLevel = payload.player.getLastLevelHistory();
    payload.scoreReached = MTLG.getOptions().compareToScore == true ? (payload.lastLevel.points > MTLG.getOptions().filterPoints) : true;

    if (payload.lastLevel.correctPercentage >= MTLG.getOptions().continueThreshold && payload.scoreReached) {
      payload.continueAllowed = true;
      payload.showRepeatLevel = MTLG.getOptions().allowVoluntaryRepetition;
    } else {
      payload.continueAllowed = false;
      payload.showRepeatLevel = true;
    }

    navigate.startEvaluation(payload);
  }

  /**
   * UI Elements
   */
  const _background = new backgroundView(background.container);

  const timerPosition = { x: MTLG.getOptions().width * 0.5, y: 125 };
  const timerStart = MTLG.getOptions().timerLength;
  const timer = new gameTimer(
    timerPosition.x,
    timerPosition.y,
    timerStart,
    () => {
      _logger.log("Time is up. Player continues to evaluation.");
      levelFinished();
    },
    levelController.isGamePaused
  );

  const signOptions = {
    dimension: {
      x: 192,
      y: 114
    },
    text: MTLG.lang.getString("backToMenu"),
    id: 'backToMenu',
    facing: 1,
    handler () {
      _logger.log("Player clicked on menu button");
      timer.cancel();
      navigate.toMenu();
    }
  };
  const sign = new signButtonView(signOptions);

  const score = new _score_box(options.filterPoints);

  /**
   * Legacy Level Controller in use.
   */

  updatePlayerPoints = score.update;

  //gets new data from the lvl generator or repeat level as stated in gameState.repeatLevel
  levelController.getNewLvlData(gameState);
  levelController.createUrlDropAreasAndViews();
  levelController.createStartingUrls();
}

function _score_box (computerScore) {
  const stage = MTLG.getStageContainer();

  const generateCoinText = (string, points) => string + ': ' + points;
  const playerCoinText = points => generateCoinText(MTLG.lang.getString("you"), points);
  const computerCoinText = () => generateCoinText(MTLG.lang.getString("filter"), computerScore);

  const createTextElement = function ({ x, y }, textGenerator) {
    this.element = new createjs.Text(
       textGenerator(),
      "40px Arial",
      "#783E2B"
    );

    this.element.x = x;
    this.element.y = y;
    this.element.textAlign = "center";

    stage.addChild(this.element);

    this.update = () => {
      this.element.text = textGenerator();
    }
  }

  dimension = {
    y: 150,
    player: MTLG.getOptions().width * 0.2,
    computer: MTLG.getOptions().width * 0.8
  }

  this.playerElement = new createTextElement({ x: dimension.player, y: dimension.y }, () => playerCoinText(levelController.getPlayerPoints()));
  this.computerElement = new createTextElement({ x: dimension.computer, y: dimension.y }, () => computerCoinText());

  this.update = () => {
    this.playerElement.update();
  }
}
