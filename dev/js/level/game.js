if (typeof globalLevels === 'undefined') {
    globalLevels = {}
}

globalLevels.initGame = function (gameState, { navigate, save }) {
    /* Define Variables and Constants */
    const rounds = levelStructure.rounds
    const maxAmountRounds = rounds.length + 1;

    /* Define actual Level Logic */
    if (!gameState.round) {
        gameState.round = 0;
    }

    // Continue with next Round
    let round = gameState.round + 1;
    if (round > rounds.length) {
        gameState.round = rounds.length;
    } else {
        gameState.round = round;
    }

    //gameState.round++;
    //gameState.round = Math.min(gameState.round, maxAmountRounds);

    gameState.bestRound = Math.max(gameState.bestRound, gameState.round);
    gameState.bestRound = Math.min(gameState.bestRound, rounds.length);

    save(gameState);

    if (round > rounds.length) {
         // Game Finished! Good Job
         
        _logger.log(`Filtering Game Finished: ${gameState.round - 1}`);
         return navigate.finishGame();
    }

    _logger.log(`Start Game in Round: ${gameState.round}`);
    navigate.startRound();
}