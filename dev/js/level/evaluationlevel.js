if (typeof globalLevels === 'undefined') {
  globalLevels = {}
}

const LOST_BY_SCORE = 'evaluation_1_lost';
const LOST_BY_PERCENTAGE = 'evaluation_1_lost_percentage';
const WON = 'evaluation_1_won';

globalLevels.initEvaluationLevel = function (gameState, { navigate, save, background }) {
  let { round = 0, slide = 0 } = gameState;

  /**
   * Create Level structure for Evaluation
   * This gets reset when a different level is called,
   * but is persited as long as `nextSlide` or `lastSlide` is called
   */
  if (!gameState.level.sections) {
    lastLevel = gameState.player.getLastLevelHistory();

    gameState.level = {
      sections: generateEvaluationSections(lastLevel),
      lastLevel
    };
  }

  const sections = gameState.level.sections.slice(0, slide + 1);

  if (sections.length <= slide) {
    /**
     * Level Finished
     */

    _logger.log(`Evaluation finished: ${gameState.round}`);
    return navigate.toParent();
  }

  /**
   * Methods of Evaluation
   */
  const draw = (text, url, repeat = false, next = true) => {
    const _navigate = {
      menu: navigate.toMenu
    };

    if (repeat) {
      _navigate.repeat = navigate.repeatLevel;
    }

    if (next) {
      _navigate.next = navigate.nextSlide;
    }

    if (slide > 0) {
      _navigate.previous = navigate.lastSlide;
    }

    drawExplanationSlide({
      textID: text,
      url,
      navigate: _navigate,
      background
    });
  }

  function isUrlEvaluated (url) {
    return gameState.level.evaluatedUrls.includes(url);
  }

  /**
   * Drawing respective slide
   */

  const section = sections[sections.length-1];

  const repeat = sections.length == gameState.level.sections.length && gameState.showRepeatLevel;
  const next = sections.length < gameState.level.sections.length || gameState.continueAllowed;

  draw(section.text, section.url, repeat, next);
}

function generateEvaluationSections(lastLevel)
{
  //IDEE: falls eine besondere nachricht gezeigt werden soll wenn die punktzahl erreicht wurde, müssen sowohl gameState.scoreReached als
  // auch MTLG.getOptions().compareToScore abgefragt werden, da das erste immer true ist wenn das zweite false ist

  let sections = [];

  const points = lastLevel.points;
  const filterPoints = MTLG.getOptions().filterPoints;
  const percentage = lastLevel.correctPercentage;
  const threshold = MTLG.getOptions().continueThreshold;

  {
    let text = LOST_BY_SCORE;

    if (!points || points <= filterPoints) {
      text = LOST_BY_SCORE;
    } else if (percentage < threshold) {
      text = LOST_BY_PERCENTAGE;
    } else {
      text = WON;
    }

    sections.push({ text });
  }

  //add default section that is a intro to the evaluation
  sections.push({ 'text': 'evaluation_5' });

  let playerAssignedUrls = levelController.getPlayer().getAssignedUrlContainer();

  const getCategories = urlArray => urlArray.map(x => x.type)
    .filter((value, index, self) => self.indexOf(value) === index);

  let urlEvaluated = [];

  function isUrlEvaluated (url) {
    return urlEvaluated.includes(url);
  }

  function evaluateUrl (urlObject) {
    const url = urlObject.url;
    const evaluationResult = urlObject.evaluationResult;
    const type = urlObject.type;
    const assignedType = urlObject.assignedType;

    //split the url
    let temp = splitUrlStringIntoUrlParts(url);
    //and add the url view parts
    temp = addUrlViewToUrlParts(temp);

    let nextSection = {}

    function sectionText (wrongMessage, matchMessage, skipMessage) {
      if (assignedType == 'skip-bucket') {
        // Skipped
        return { ...nextSection, "text": skipMessage }
      }
      else if (assignedType === type) {
        // Match
        return { ...nextSection, "text": matchMessage }
      }
      else {
        // Wrong
        return { ...nextSection, "text": wrongMessage }
      }
    }

    switch (type) {
      case "random":
        nextSection = sectionText('eval_random_wrong', 'eval_random_correct', 'eval_random_skip');
        break;
      case "ip":
        nextSection = sectionText('eval_ip_wrong', 'eval_ip_correct', 'eval_ip_skip');
        break;
      case "path":
        nextSection = sectionText('eval_path_wrong', 'eval_path_correct', 'eval_path_skip');
        break;
      case "regDomain":
        nextSection = sectionText('eval_regdomain_wrong', 'eval_regdomain_correct', 'eval_regdomain_skip');
        break;
      case "subDomain":
        nextSection = sectionText('eval_subdomain_wrong', 'eval_subdomain_correct', 'eval_subdomain_skip');
        break;
      case "no-Phish":
        nextSection = sectionText('eval_nophish_wrong', 'eval_nophish_correct', 'eval_nophish_skip');
        break;
      default:
        console.log(`${type} did not match anything`);
    }
    nextSection.url = temp;

    sections.push(nextSection);

    urlEvaluated.push(url);
  }

  function evaluateUrls (urls, count = 1) {
    const categories = getCategories(urls);
    const filteredUrls = categories.reduce((acc, cat) => ({
      ...acc,
      [cat]: urls.filter(url => url.type === cat)
    }), {});

    for (let cat of categories) {
      for (let i = 0; i < count; i++) {
        const openUrls = filteredUrls[cat].filter(x => !isUrlEvaluated(x.url))
        if (openUrls.length < 1) {
          continue;
        }

        const url = selectRandom(filteredUrls[cat]);
        evaluateUrl(url);
      }
    }
  }

  // Explain completely False URLs
  { // local scope
    const urls = playerAssignedUrls[2];
    evaluateUrls(urls);
  }

  // Explain partly False URLs
  { // local scope
    const urls = playerAssignedUrls[1];
    evaluateUrls(urls);
  }

  // Explain URLs that were skipped
  if (MTLG.getOptions().evaluateSkipedUrls) {
    const urls = playerAssignedUrls[3];
    evaluateUrls(urls);
  }

  // Explain URLs that were determined the right way
  if (MTLG.getOptions().evaluatePositiveUrls) {
    const urls = playerAssignedUrls[0];
    evaluateUrls(urls);
  }

  return sections;
}