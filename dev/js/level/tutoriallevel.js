if (typeof globalLevels === 'undefined') {
    globalLevels = {}
}

globalLevels.initTutorial = function (gameState, { navigate, save, background }) {
  let { round = 0, slide = 0 } = gameState;

  const rounds = levelStructure.rounds;

  if (!(Array.isArray(rounds) && round <= rounds.length)) {
    navigate.toParent();
  }

  const roundData = rounds[round - 1] ? rounds[round - 1].tutorial : undefined;
  if (!roundData) {
    return navigate.toParent();
  }

  const slides = roundData.slides;

  if (!(Array.isArray(slides) && slide < slides.length)) {
    // All Explanation Slides are done now.
    return navigate.startLevel();
  }
  const slideData = slides[slide];

  const _navigate = {
    next: navigate.nextSlide,
    menu: navigate.toMenu
  }

  if (slide > 0) {
    _navigate.previous = navigate.lastSlide;
  }

  drawExplanationSlide({
    ...slideData,
    textID: slideData.text,
    navigate: _navigate,
    background
  });
}
