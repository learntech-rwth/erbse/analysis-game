if (typeof globalLevels === 'undefined') {
  globalLevels = {}
}

globalLevels.initEndLevel = function (gameState, { navigate, save }) {
  const stage = MTLG.getStageContainer();

  /**
   * Background Image
   */
  const backgroundImage = MTLG.assets.getBitmap("img/background_training.jpg");
  stage.addChild(backgroundImage);

  /**
   * Border
   */
  const border = MTLG.assets.getBitmap("img/border.png");
  border.scaleX = 1.15;
  border.scaleY = 0.7;
  border.x = 250;
  border.y = 130;
  stage.addChild(border);

  /**
   * Menu Button
   */
  const menuButtonOption = {
    dimension: {
      x: MTLG.getOptions().width / 2,
      y: 800
    },
    text: MTLG.lang.getString("backToMenu"),
    id: 'backToMenu',
    facing: 1,
    handler () {
      _logger.log("Player clicked on menu button");
      navigate.toMenu();
    }
  };
  const menuButton = new signButtonView(menuButtonOption);

  /**
   * End Text
   */
  const generateEndText = function (text, { x, y } = {}) {
    const element = new createjs.Text(MTLG.lang.getString(text), '28px Arial', 'black');
    element.x = x;
    element.y = y;
    element.textAlign = 'center';
    element.lineWidth = 1000;

    stage.addChild(element);

    return element;
  }

  const endtext = {};
  endtext[1] = generateEndText('end_1', { x: MTLG.getOptions().width / 2, y: 200 });
  endtext[2] = generateEndText('end_2', { x: endtext[1].x - 600, y: 450 });
  endtext[3] = generateEndText('end_3', { x: endtext[2].x + 200, y: endtext[2].y });

  /**
   * Final GameState reached. Send Logs
   */
  _logger.log("Reached the end of the game!")
  _logger.sendLogs();
}