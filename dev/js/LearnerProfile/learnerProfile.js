function learnerProfile() {
    const urlGenerator = require('url-generator');

    this.browserServices = [];
    this.personalizedServices = [];
    this.defaultServices = [
        {
            "name": "Google.com",
            "url": "https://www.google.com/"
        },
        {
            "name": "Youtube.com",
            "url": "https://www.youtube.com/"
        },
        {
            "name": "Google.de",
            "url": "https://www.google.de/"
        },
        {
            "name": "Amazon.de",
            "url": "https://www.amazon.de/"
        },
        {
            "name": "Wikipedia",
            "url": "https://en.wikipedia.org/"
        },
        {
            "name": "eBay Kleinanzeigen",
            "url": "https://www.ebay-kleinanzeigen.de/"
        },
        {
            "name": "Facebook.com",
            "url": "https://www.facebook.com/"
        },
        {
            "name": "Twitch.tv",
            "url": "https://www.twitch.tv/"
        },
        {
            "name": "Gmx.net",
            "url": "https://www.gmx.net/"
        },
        {
            "name": "Web.net",
            "url": "https://web.de/"
        },
        {
            "name": "Vk.com",
            "url": "https://www.vk.com/"
        },
        {
            "name": "Yahoo.com",
            "url": "https://www.yahoo.com/"
        },
        {
            "name": "Netflix.com",
            "url": "https://www.netflix.com/"
        },
        {
            "name": "T-online.de",
            "url": "https://www.telekom.com/"
        },
        {
            "name": "Live.com",
            "url": "https://login.live.com/"
        },
        {
            "name": "Bild.de",
            "url": "https://secure.mypass.de/"
        },
        {
            "name": "Bild.de",
            "url": "https://bild.de/"
        },
        {
            "name": "Spiegel.de",
            "url": "https://www.spiegel.de/"
        },
        {
            "name": "Microsoft.com",
            "url": "https://www.microsoft.com/"
        },
        {
            "name": "Otto.de",
            "url": "https://www.otto.de/"
        },
        {
            "name": "Wetter.com",
            "url": "https://www.wetter.com/"
        },
        {
            "name": "Wetter.com",
            "url": "https://interaction.7pass.de/"
        },
        {
            "name": "Mobile.de",
            "url": "https://login.mobile.de/"
        },
        {
            "name": "Zdf.de",
            "url": "https://www.zdf.de/"
        },
        {
            "name": "Fandom.com",
            "url": "https://www.fandom.com/"
        },
        {
            "name": "Amazon.com",
            "url": "https://www.amazon.com/"
        },
        {
            "name": "Idealo.de",
            "url": "https://www.idealo.de/"
        },
        {
            "name": "Focus.de",
            "url": "https://www.focus.de/"
        },
        {
            "name": "Zeit.de",
            "url": "https://meine.zeit.de/"
        },
        {
            "name": "PayPal",
            "url": "https://www.paypal.com/"
        },
        {
            "name": "runescape 3979",
            "url": "https://secure.runescape.com/"
        },
        {
            "name": "steam",
            "url": "https://store.steampowered.com/"
        },
        {
            "name": "deutsche-bank.de",
            "url": "https://meine.deutsche-bank.de/"
        },
        {
            "name": "commerzbank.de",
            "url": "https://www.commerzbank.de/"
        },
        {
            "name": "dropbox.com",
            "url": "https://www.dropbox.com/"
        },
        {
            "name": "icloud.com",
            "url": "https://www.icloud.com/"
        },
        {
            "name": "onedrive",
            "url": "https://onedrive.live.com/"
        }
    ];
    this.probablyUnknown = [

    ];

    this.baseSets = [
        {"entries": this.defaultServices, "generatedEntries": {}, "gameStateEntry": "generatedDefaultServices"},
        {"entries": this.browserServices, "generatedEntries": {}, "gameStateEntry": "generatedBrowserServices"},
        {"entries": this.probablyUnknown, "generatedEntries": {}, "gameStateEntry": "generatedProbUnknownServices"}
    ]

    this.personalizedEntries = [
        {"setName": "knownServices", "entries": [], "flags": ["selected", "used"], "generatedEntries": {}, "gameStateEntry": "generatedPersonalizedUrls"},
        {"setName": "unknownServices", "entries": [], "flags": ["rejected"], "generatedEntries": {}, "gameStateEntry": "generatedUnknownUrls"}
    ]

    this.generatePersonalizedUrls = () => {
        addEntryToGamestate("personalizationResult", this.personalizedServices);
        for(entry of this.personalizedEntries){
            entry.generatedEntries = this.generatePersonalizedUrlsForFlag(entry.flags, entry.setName, entry.gameStateEntry);
        }
        if(window.hasOwnProperty("_saver")){
            _saver.save();
        }
    }

    this.generateBaseUrls = () => {
        for(entry of this.baseSets){
            let entries = entry.entries;
            if(entries != null && entries != [] && entries.length > 0){
                entry.generatedEntries = generateUrls(entry.entries, entry.gameStateEntry);
            }
        }
    }

    this.init = () => {
        this.generateBaseUrls();
    }

    this.restoreLearnerProfile = () => {
        this.personalizedServices = getEntryFromGameState("personalizationResult");
        for(entry of this.baseSets){
            entry.generatedEntries = getEntryFromGameState(entry.gameStateEntry);
        }
        for(entry of this.personalizedEntries){
            entry.entries = getEntryFromGameState(entry.setName);
            entry.generatedEntries = getEntryFromGameState(entry.gameStateEntry);
        }
    }

    function getEntryFromGameState(entryName){
        if(entryName in gameState){
            return gameState[entryName];
        }
    }

    this.restoreGameState = () => {
        if(!window.hasOwnProperty("gameState")){
            gameState = {}
        }

        addEntryToGamestate("personalizationResult", this.personalizedServices);
        for(entry of this.baseSets){
            addEntryToGamestate(entry.gameStateEntry, entry.generatedEntries);
        }
        for(entry of this.personalizedEntries){
            addEntryToGamestate(entry.setName, entry.entries);
            addEntryToGamestate(entry.gameStateEntry, entry.generatedEntries);
        }
    }

    /**
     * Sets the personalized services to the passed services.
     * @param {service[]} services
     */
    this.setServices = (services) => {
        this.personalizedServices = services;
        this.generatePersonalizedUrls();
    }

    /**
     * Adds an entry to the list of personalized services.
     * @param {service} service
     */
    this.addService = (service) => {
        this.personalizedServices.push(service);
        this.generatePersonalizedUrls();
    }

    /**
     * Resets the list of personalized services.
     */
    this.resetServices = () => {
        this.personalizedServices = [];
    }

    /**
     * Check if the learner profile contains personalized data.
     * @returns {boolean} - True if personalized services were entered.
     */
    this.wasPersonalized = () => {
        return (this.personalizedServices != null && this.personalizedServices.length > 0);
    }

    /**
    * Randomly selects a service from the learner profile / the default values and returns it.
    * @returns {service} - The selected service.
    */
    this.getSingleService = () => {
        if(wasPersonalized()){
            return this.personalizedServices[randomIntBetween(0, this.personalizedServices.length-1)];
        }
        else {
            return this.defaultServices[randomIntBetween(0, this.defaultServices.length-1)];
        }
    }

    /**
     * Used to get the stored personalized services.
     * @returns {service[]} - The personalized services.
     */
    this.getPersonalizedServices = () => {
        return this.personalizedServices;
    }

    /**
     * Used to get personalized services with a flag set to true.
     * @param {string} flag - The flag which is checked.
     * @returns {service[]} - List of personalized services with the flag.
     */
    this.getServicesWithFlag = (flag) => {
        let result = this.personalizedServices.filter(service => (service[flag] == true));
        return result;
    }

    /**
     * Used to get personalized services with a flag set to value.
     * @param {string} flag - The flag which is checked.
     * @param {*} value - The value the flag has to have.
     * @returns {service[]} - List of personalized services with the flag set to value.
     */
    this.getServicesWithFlagValue = (flag, value) => {
        let result = this.personalizedServices.filter(service => (service[flag] == value));
        return result;
    }

    function randomIntBetween(min, max){
        let res = Math.random() * (max-min);
        res += min;
        res = Math.round(res);
        return res;
    }

    this.generatePersonalizedUrlsForFlag = (flags, setName, entryName) => {
        let filteredServices = []
        for(flag of flags){
            let flagServices = this.getServicesWithFlag(flag);
            if(filteredServices == []){
                filteredServices = flagServices;
            }
            else {
                for(service of flagServices){
                    if(!filteredServices.includes(service)){
                        filteredServices.push(service);
                    }
                }
            }
        }

        addEntryToGamestate(setName, filteredServices);
        return generateUrls(filteredServices, entryName);
    }

    function generateUrls(services, gameStateKey){
        let generatedUrls = {};

        //console.log(services);
        generatedUrls = generateForServices(services);

        // Add key to gamestate => allow for use in urlSelector, storage with the saver!
        addEntryToGamestate(gameStateKey, generatedUrls);
        return generatedUrls;
    }

    function generateForServices(services, passedConfig = null) {
        let defaultConfig = {
            benign: 6,
            path: 3,
            subdomain: 3,
            regdomain: 4,
            ip: 5,
            random: 5
        }
        let usedConfig = passedConfig == null ? defaultConfig : passedConfig;
        const generated = {
            config: usedConfig,
            benign: [],
            phishing: [],
            services
        }

        const generateBenign = function (service) {
            return urlGenerator(service, { length: generated.config.benign, category: urlGenerator.categoriesDictionary.NO_PHISH })
        }

        const generatePhishing = function (service) {
            return [
                ...urlGenerator(service, { length: generated.config.path, category: urlGenerator.categoriesDictionary.PATH }),
                ...urlGenerator(service, { length: generated.config.subdomain, category: urlGenerator.categoriesDictionary.SUB_DOMAIN }),
                ...urlGenerator(service, { length: generated.config.regdomain, category: urlGenerator.categoriesDictionary.REGISTRABLE_DOMAIN }),
                ...urlGenerator(service, { length: generated.config.ip, category: urlGenerator.categoriesDictionary.IP_ADDRESS }),
                ...urlGenerator(service, { length: generated.config.random, category: urlGenerator.categoriesDictionary.RANDOM })
            ]
        }

        for (const service of services) {
            //console.log(service);
            generated.benign = [...generated.benign, generateBenign(service)]
            generated.phishing = [...generated.phishing, generatePhishing(service)]
        }

        return generated;
    }

    function addEntryToGamestate(key, value){
        if(!window.hasOwnProperty("gameState")){
            gameState = {}
            gameState[key] = value;
        }
        else {
            gameState[key] = value;
        }
    }

    this.init();

    // return {
    //     init: init,

    //     resetServices: resetServices,
    //     setServices: setServices,
    //     addService: addService,

    //     getSingleService : getSingleService,
    //     getPersonalizedServices: getPersonalizedServices,
    //     getServicesWithFlag: getServicesWithFlag,
    //     getServicesWithFlagValue: getServicesWithFlagValue,
    //     wasPersonalized: wasPersonalized,

    //     restoreLearnerProfile: restoreFromGameState,
    //     restoreGameState: restoreGameState,

    //     personalizedServices: this.personalizedServices,
    //     browserServices: this.browserServices,
    //     baseSets: this.baseSets,
    //     personalizedEntries: this.personalizedEntries
    // }
};
