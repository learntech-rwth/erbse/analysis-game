function drawMainMenu(gameState, { navigate, save }) {

    /**
     * View Specific Constants
     */
    const signDimension = { width: 384, height: 228 }
    const stage = MTLG.getStageContainer();
    const menu_container = new createjs.Container();
    stage.addChild(menu_container);

    /**
     * View specific Functions
     */
    const createSign = ({ title, position: { length, index }, dimensions: { width, height }, callback }) => {
        const padding = 50;
        const factor = 2 * (index + 1) - length - 1;
        const offset = ((factor + 1) * padding + factor * width) / 2;

        const signOptions = {
            dimension: { x: offset, y: 0 },
            text: MTLG.lang.getString(title),
            id: title,
            handler: callback,
            width,
            height,
            facing: 0,
            add: false
        };
        return new signButtonView(signOptions);
    }
    const addRow = (signs, container) => signs.map((sign, index, array) => container.addChild(createSign({ ...sign, position: { length: array.length, index } })));

    /**
     * MTLG Loading stuff
     */

    /** OK WHAT IS THIS? maybe explain - cheers
    // log in demo player if not yet present
    if (!MTLG.getPlayerNumber()) {
        MTLG.menu.addSubMenu(function () {
            MTLG.lc.levelFinished({
                nextLevel: "developerLevel"
            });
        });

        //console.log("Logging in");
        //MTLG.lc.goToLogin(); //Leave Main Menu and go to login
    }

    MTLG.menu.start(); */

    /**
     * Define View Objects
     */

    //set background-color to black
    MTLG.setBackgroundColor("black");

    // new uuidView(_logger.getUUID());
    new uuidView(_logger.getUUID());

    //show name of the game
    let gameName = new createjs.Text(MTLG.lang.getString("gameName"), "55px Piazzolla", "white");
    gameName.x = 800;
    gameName.y = 465;

    // Different Sign Rows

    const signContainers = { top: new createjs.Container(), bottom: new createjs.Container() }
    signContainers.top.x = 972;
    signContainers.top.y = 314;
    signContainers.bottom.x = signContainers.top.x;
    signContainers.bottom.y = 664;

    // Define Signs
    const startGameSign = {
        title: 'startGame',
        dimensions: signDimension,
        callback () {
            _logger.log(`Player started filtering game.`);
            _learnerProfile.restoreLearnerProfile();
            if(_learnerProfile.wasPersonalized() || !MTLG.getOptions().pi_enabled){
                navigate.startGame();
            }
            else {
                PersonalizationInterface.init(menu_container, true, navigate);
            }
        }
    };
    const resumeGameSign = {
        title: 'resumeGame',
        dimensions: signDimension,
        callback () {
            _logger.log(`Player resumed filtering game.`);
            navigate.resumeGame();
            _learnerProfile.restoreLearnerProfile();
        }
    };
    const optionsSign = {
        title: 'options',
        dimensions: signDimension,
        callback () {
            _logger.log(`Player opened settings.`);
            navigate.toOptions();
        }
    }

    const topSigns = [ startGameSign ];
    const bottomSigns = [ optionsSign ];

    if ( (gameState.bestRound > 0 && (!MTLG.getOptions().pi_enabled || gameState.generatedPersonalizedUrls)) || MTLG.getOptions().devMode ) {
        startGameSign.title = 'startNewGame';
        topSigns.push(resumeGameSign);
    }

    // add Signs to SignRows
    addRow(topSigns, signContainers.top);
    addRow(bottomSigns, signContainers.bottom);

    // add objects to stage
    menu_container.addChild(gameName);
    Object.values(signContainers).map(container => menu_container.addChild(container));
}
