function optionsScreen(gameState, { navigate, save }) {
  const stage = MTLG.getStageContainer();

  const _set_language = lang => {
    _logger.log(`Set Language: '${lang}'`);
    MTLG.lang.setLanguage(lang);
    save(gameState);
    navigate.nextSlide();
  }

  const _set_difficulty = difficulty => {
    _logger.log(`Set Difficulty: '${difficulty}'`);
    MTLG.loadOptions({ difficulty });
    save(gameState);
  }

  const _set_timer = time => {
    _logger.log(`Set Timer: '${time}'`);
    MTLG.loadOptions({"timerLength": time});
    save(gameState);
  }

  const _set_filter_points = points => {
    _logger.log(`Set filterPoints: '${points}'`);
    MTLG.loadOptions({"filterPoints": points});
    save(gameState);
  }

  const _set_count = value => {
    const urlTypes = MTLG.getOptions().urlTypes;
    let i=0;
    //remove containers from urlTypes
    if(MTLG.getOptions().containerCount > value){
        while (urlTypes.length > value){
            urlTypes.pop();
        }
    }
    //add containers to urlTypes
    else if(MTLG.getOptions().containerCount < value){
        while (urlTypes.length < value){
            //if type not found in urlTypes
            if(urlTypes.indexOf(i) === -1){
                let j;
                urlTypes.push(0);
                for(j = urlTypes.length - 1; j > i; j--){
                    urlTypes[j] = urlTypes[j-1];
                }
                urlTypes[i] = i;
            }
            i++;
        }
    }
    _logger.log(`Set containerCount: '${value}'`);
    MTLG.loadOptions({"containerCount": value});
  }

  const _create_slider = ({ x, y }, options, callback) => {
    const container = new createjs.Container();
    container.x = MTLG.getOptions().width /2;
    container.y = y;

    const slider = MTLG.utils.uiElements.addSlider({
        ...options,
        firstColor: "white",
        secondColor: "red",
        sizeY: 38
      },
      callback
    );
    slider.x = x;
    container.addChild(slider);

    return container
  }

  /**
   * Language Switch
   */
  const languageContainer = switchButtonComponent(
    MTLG.lang.getString("language") + ":",
    {
      text: 'german',
      isSelected: () => MTLG.lang.getLanguage() === 'de',
      callback () {
        _set_language('de');
      }
    },
    {
      text: 'english',
      isSelected: () => MTLG.lang.getLanguage() === 'en',
      callback () {
        _set_language('en');
      }
    },
    { x: MTLG.getOptions().width /2, y: 200 }
  );
  stage.addChild(languageContainer);

  const personalizationContainer = switchButtonComponent(
    MTLG.lang.getString("pi_headline") + ":",
    {
      text: 'pi_enabled',
      isSelected: () => MTLG.getOptions().pi_enabled == true,
      callback () {
        MTLG.getOptions().pi_enabled = true
      }
    },
    {
      text: 'pi_disabled',
      isSelected: () => MTLG.getOptions().pi_enabled == false,
      callback () {
        MTLG.getOptions().pi_enabled = false
      }
    },
    { x: MTLG.getOptions().width /2, y: 350 }
  );

  xBound = -personalizationContainer.getBounds().width /2;

  if(MTLG.getOptions().pi_showPersonalizationOptions){
    stage.addChild(personalizationContainer);
  }
  
  /**
   * Coinlevel Timer Slider
   */
  const timerContainer = _create_slider({ x: xBound, y: 500}, {
    min: 10,
    max: 130,
    stepSize: 30,
    defaultVal: MTLG.getOptions().timerLength,
    description: "Timer:"
  }, _set_timer);

  if (MTLG.getOptions().devMode) {
    stage.addChild(timerContainer);
  }

  /**
   * Coinlevel Filter Points Slider
   */
  const filterContainer = _create_slider({ x: xBound, y: 650 }, {
    min: MTLG.getOptions().pointsForRightTendency * 2,
    max: MTLG.getOptions().pointsForRightType * 15,
    stepSize: MTLG.getOptions().pointsForRightTendency,
    defaultVal: MTLG.getOptions().filterPoints,
    description: MTLG.lang.getString("filterPoints") + ":"
  }, _set_filter_points);

  if (MTLG.getOptions().devMode) {
    stage.addChild(filterContainer);
  }

  /**
   * Coinlevel Count Slider
   */
  const countContainer = _create_slider({x: xBound, y: 800 }, {
    min: 2,
    max: 6,
    stepSize: 1,
    defaultVal: MTLG.getOptions().containerCount,
    description: MTLG.lang.getString("containerCount") + ":"
  }, _set_count);

  if (MTLG.getOptions().devMode) {
    stage.addChild(countContainer);
  }

  /**
   * Difficulty Switch
   */
  const difficultyContainer = switchButtonComponent(
    MTLG.lang.getString("difficulty") + ":",
    {
      text: 'easy',
      isSelected: () => MTLG.getOptions().difficulty === 'easy',
      callback () {
        _set_difficulty('easy');
      }
    },
    {
      text: 'hard',
      isSelected: () => MTLG.getOptions().difficulty === 'hard',
      callback () {
        _set_difficulty('hard');
      }
    },
    { x: MTLG.getOptions().width /2, y: 950 }
  );
  if (MTLG.getOptions().devMode) {
    stage.addChild(difficultyContainer);
  }

  /**
  * Menu Button
  */
  const menuButtonOption = {
    dimension: { x: 192, y: 114 },
    text: MTLG.lang.getString("backToMenu"),
    id: 'backToMenu',
    facing: 1,
    handler () {
      _logger.log("Player clicked on menu button");
      navigate.toMainmenu();
    }
  };
  const menuButton = new signButtonView(menuButtonOption);
}
