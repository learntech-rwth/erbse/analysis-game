function selectionScreen(gameState, { navigate, save }) {
    /**
     * View Specific Constants
     */
    const signCenter = { x: 760, y: 465 };
    const signPadding = { x: 100, y: 80 };
    const signDimension = { width: 240, height: 140 };
    const stage = MTLG.getStageContainer();

    /**
     * View specific Functions
     */
    const createSign = ({ title, position: { length, index } = {}, dimensions: { width, height } = {}, callback }) => {
        if (!title) {
            return new createjs.Container();
        }

        const padding = signPadding.x;
        const factor = 2 * (index + 1) - length - 1;
        const offset = ((factor + 1) * padding + factor * width) / 2;

        const signOptions = {
            dimension: { x: offset, y: 0 },
            text: MTLG.lang.getString(title),
            id: title,
            handler: callback,
            width,
            height,
            add: false
        };
        return new signButtonView(signOptions);
    }
    const transition = (transition) => function () {
        _logger.log("Selection screen: clicked on button for " + transition.toString());
        transition()
    }

    /**
     * Define View Objects
     */

    new uuidView(_logger.getUUID());

    MTLG.setBackgroundColor('black');

    const mainMenuSign = {
        title: 'backToMainMenu',
        dimensions: signDimension,
        callback: transition(() => navigate.toMainmenu())
    }

    const buttonRows = [
        [mainMenuSign, {}]
    ];

    const bestRound = MTLG.getOptions().devMode ? levelStructure.rounds.length : (isNaN(gameState.bestRound) ? 1 : Math.max(gameState.bestRound, 1));
    for (let i = 1; i <= bestRound; i++) {
        const tutorialSign = {
            title: `Tutorial ${i}`,
            dimensions: signDimension,
            callback: transition(() => navigate.toGameTutorial({ round: i }))
        }
        const row = [ tutorialSign ];

        const levelSign = {
            title: `Level ${i}`,
            dimensions: signDimension,
            callback: transition(() => navigate.toGameLevel({ round: i }))
        }
        // TODO: Only push Level Button if Evaluation was fully visited?
        // TODO: If then  must be added an empty sign
        row.push(levelSign);

        buttonRows.push(row);
    }

    const buttonContainer = new createjs.Container();
    buttonContainer.x = signCenter.x
    buttonContainer.y = signCenter.y

    buttonRows.map((row, index, array) => {
        const length = array.length;
        const height = Math.max(...row.map(sign => sign.dimensions ? sign.dimensions.height : 0));

        const padding = signPadding.y;
        const factor = 2 * (index + 1) - length - 1;
        const offset = ((factor + 1) * padding + factor * height) / 2;

        const container = new createjs.Container();
        container.x = 0
        container.y = offset

        row.map((sign, index, array) => container.addChild(createSign({ ...sign, position: { length: array.length, index } })));

        buttonContainer.addChild(container);
    });

    stage.addChild(buttonContainer);
  }

function checkSelection(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel === "levelmenu") {
        return 1;
    }
    return 0;
}
