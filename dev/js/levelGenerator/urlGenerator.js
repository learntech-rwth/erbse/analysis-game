/**
 * Module for generating phishing URLs.
 * Expected functionality: Has to have a function generateUrl(type, url) like specified below.
 * @module urlGenerator
 */

let urlGeneratorFG = (function(){
    // Variables and settings for the filtering game
    let defaultSubdomains = ["shopping", "trade", "shopping-cart", "secure", "login", "authenticate", "account", "finance", "customer-service", "customers", "account-safety", "safety", "transaction"];
    let defaultRegDomains = ["adncard", "amznon", "finaag", "random-part", "12part3"];
    let defaultRegDomainParts = ["shop", "login", "service", "secure", "profile", "security", "authentication", "data", "customers", "shopping-cart"];
    let defaultTlds = [".com", ".de", ".co.uk", ".tv", ".org", ".us", ".eu", ".xyz",".gdn"];
    let defaultPathParts = ["shopping", "trade", "shopping-cart", "secure", "login", "authenticate", "account", "finance", "customer-service", "customers", "account-safety", "safety", "transaction"];

    /**
     * Generates and returns URL based on the given type and base URL.
     * 
     * @param {String} type - The type the generated (non-)phishing url is supposed to have.
     * @param {String} url - The base url from which the (non-)phishing url will be created.
     * @returns {String} - The generated URL.
     * @memberof module:urlGenerator  
     */
    function generateUrl(type, url){
        let generatedUrl = "";
        switch(type) {
            case "random":
                generatedUrl = generateRandomPhish(url);
                break;
            case "ip":
                generatedUrl = generateIpPhish(url);
                break;
            case "path":
                generatedUrl = generatePathPhish(url);
                break;
            case "regDomain":
                generatedUrl = generateRegDomainPhish(url);
                break;
            case "subDomain":
                generatedUrl = generateSubDomainPhish(url);
                break;
            case "no-Phish": 
                generatedUrl = generateNoPhishUrl(url);
                break;
            default: 
                return "Error: type does not exist";
        }

        return generatedUrl;
    }

    /**
     * @returns {String} - The protocol to be used, either http or https.
     */
    function generateProtocol(){
        return (Math.random() > 0.4) ? "https://" : "http://";
    }

    /**
     * @returns {String} - A randomly generated IP address.
     */
    function generateIpAddress(){
        let ipAddress = "";
        for(var i = 0; i < 4; i++){
            ipAddress += utils.generateRandomIntBetween(0,255);
            if(i != 3){
                ipAddress += ".";
            }
        }
        return ipAddress;
    }
    
    /**
     * Generates a random path from the defined path parts.
     * 
     * @param {Integer} minLength - The minimum number of path parts.
     * @param {Integer} maxLength - The maximum number of path parts.
     * @returns {String} - The generated path
     */
    function generatePath(minLength, maxLength){
        var numberOfPathParts = utils.generateRandomIntBetween(minLength, maxLength);
        let path = "";
        for(var i = 0; i < numberOfPathParts; i++){
            path += defaultPathParts[utils.generateRandomIntBetween(0,defaultPathParts.length-1)] + "/";
        }
        return path;
    }

    /**
     * Generates a random subdomain from the defined subdomain parts.
     * @param {*} minLength - The minimum number of subdomain parts.
     * @param {*} maxLength - The maximum number of subdomain parts.
     * @returns {String} - The generated subdomains.
     */
    function generateSubdomains(minLength, maxLength){
        var numberOfSubdomains = utils.generateRandomIntBetween(minLength, maxLength);
        let subdomains = "";
        for(var i = 0; i < numberOfSubdomains; i++){
            subdomains += defaultSubdomains[utils.generateRandomIntBetween(0,defaultSubdomains.length-1)] + ".";
        }
        return subdomains;
    }

    /**
     * Used to randomly select a registrable domain.
     * @returns - A randomly selected defined registrable domain.
     */
    function generateDomain(){
        return defaultRegDomains[utils.generateRandomIntBetween(0,defaultRegDomains.length-1)];
    }

    /**
     * Used to randomly select a TLD.
     * @returns - A randomly selected defined TLD.
     */
    function generateTld(){
        return defaultTlds[utils.generateRandomIntBetween(0,defaultTlds.length-1)] + "/";
    }

    /**
     * Creates an IP-phishing URL.
     * @param {String} url - The URL the output is based on.
     * @returns {String} - The generated URL.
     */
    function generateIpPhish(url){
        let phishingUrl = generateProtocol();
        phishingUrl += generateIpAddress() + "/";
        phishingUrl += generatePath(2,5);
        return phishingUrl;
    }

    /**
     * Creates an path-phishing URL.
     * @param {String} url - The URL the output is based on.
     * @returns {String} - The generated URL.
     */
    function generatePathPhish(url){
        let phishingUrl = generateProtocol();
        phishingUrl += generateSubdomains(0,2);
        phishingUrl += generateDomain();
        phishingUrl += generateTld();

        var numberOfPathParts = utils.generateRandomIntBetween(2, 4);
        var positionOfUrl = utils.generateRandomIntBetween(0, numberOfPathParts-1);

        for(var i = 0; i < numberOfPathParts; i++){
            if(i == positionOfUrl){
                phishingUrl += getOnlyName(url) + "/";
            }
            else {
                phishingUrl += defaultPathParts[utils.generateRandomIntBetween(0,defaultPathParts.length-1)] + "/";
            }
        }

        return phishingUrl;
    }
    
    /**
     * Creates an random-phishing URL.
     * @param {String} url - The URL the output is based on.
     * @returns {String} - The generated URL.
     */
    function generateRandomPhish(url){
        let phishingUrl = generateProtocol() + "www.";
        for(var i = 0; i < utils.generateRandomIntBetween(0,2); i++){
            phishingUrl += utils.generateRandomCharSequence() + ".";
        }
        phishingUrl += utils.generateRandomCharSequence();
        phishingUrl += defaultTlds[utils.generateRandomIntBetween(0,defaultTlds.length-1)] + "/";
        phishingUrl += generatePath(0, 2);
        return phishingUrl;
    }
    
    /**
     * Creates an regdomain-phishing URL.
     * @param {String} url - The URL the output is based on.
     * @returns {String} - The generated URL.
     */
    function generateRegDomainPhish(url){
        let phishingUrl = generateProtocol() + "www.";
        phishingUrl += generateSubdomains(0,2);
        phishingUrl += getOnlyName(url);
        phishingUrl += (Math.random() < 0.5) ? "-" : "";
        phishingUrl += defaultRegDomainParts[utils.generateRandomIntBetween(0, defaultRegDomainParts.length - 1)];
        phishingUrl += defaultTlds[utils.generateRandomIntBetween(0,defaultTlds.length-1)] + "/";
        phishingUrl += generatePath(0, 2);

        return phishingUrl;
    }

    /**
     * Creates an subdomain-phishing URL.
     * @param {String} url - The URL the output is based on.
     * @returns {String} - The generated URL.
     */
    function generateSubDomainPhish(url){
        let phishingUrl = generateProtocol() + "www.";

        var numberOfSubdomainParts = utils.generateRandomIntBetween(2, 3);
        var positionOfUrl = utils.generateRandomIntBetween(0, numberOfSubdomainParts-1);

        for(var i = 0; i < numberOfSubdomainParts; i++){
            if(i == positionOfUrl){
                phishingUrl += getOnlyName(url) + ".";
            }
            else {
                phishingUrl += defaultSubdomains[utils.generateRandomIntBetween(0,defaultSubdomains.length-1)] + ".";
            }
        }

        phishingUrl += generateDomain();
        phishingUrl += generateTld();
        phishingUrl += generatePath(0,2);
        
        return phishingUrl;
    }

    /**
     * Creates a no-phish URL by only adding subdomain, path and protocol info.
     * @param {String} url - The URL the output is based on.
     * @returns {String} - The generated URL.
     */
    function generateNoPhishUrl(url){
        let generatedUrl = generateProtocol() + "www.";
        generatedUrl += generateSubdomains(0,2);
        generatedUrl += getOnlyBaseUrl(url) + "/";
        generatedUrl += generatePath(0,2);
        return generatedUrl;
    }

    /**
     * Gets the registrable domain of an input URL.
     * @param {String} urlString - The input URL.
     * @returns {String} - The registrable domain of the URL.
     */
    function getOnlyBaseUrl(urlString){
        var urlParts = urlString.split("//");
        var domainParts = urlParts[urlParts.length-1].split(".");
        domainParts[domainParts.length-1] = domainParts[domainParts.length-1].replace("/", "");
        return domainParts[domainParts.length-2] + "." + domainParts[domainParts.length-1];
    }

    /**
     * Gets the registrable domain without the TLD of an input URL.
     * @param {String} urlString - The input URL.
     * @returns {String} - The registrable domain without the TLD of the URL.
     */
    function getOnlyName(urlString){
        var urlParts = urlString.split("//");
        var domainParts = urlParts[urlParts.length-1].split(".");
        return domainParts[domainParts.length-2];
    }

    return {
        generateUrl : generateUrl
    }
})();

if (typeof exports !== 'undefined') {
    module.exports = urlGeneratorFG;
}
