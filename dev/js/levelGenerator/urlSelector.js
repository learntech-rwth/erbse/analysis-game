/**
 * Module for returning predifined phishing URLs for comparability in studies.
 * Expected functionality: Has to have a function generateUrl(type, url) like specified below akin to the urlGenerator module.
 * @module urlSelector
 */

let urlSelector = (function(){
    // CURRENTLY UNUSED: DOES NOT MAKE A LOT OF SENSE WITH THE CURRENT IMPLEMENTATION
    let selector_minSelectedPersonalized = 10; // minimum amount of different services to be selected 

    let selector_sourceSets = [
        {"setName" : "generatedDefaultServices", "setChance" : 0},
        {"setName" : "generatedProbUnknownServices", "setChance" : 0},
        {"setName" : "generatedUnknownUrls", "setChance" : 0.2},
        {"setName" : "generatedPersonalizedUrls"},
    ]
    let selector_usePredefined = false;

    let predefinedUrls = require('../json/generatedUrls.json');

    function getUrlForType(type){
        let subgroup = "phishing"
        if(type == "no-Phish"){
            subgroup = "benign";
        }

        let convType = convertType(type);
        if(convType == ""){
            return "Error: passed Type does not exist."
        }

        let possibleObjects = [];

        if(usePersonalizedData()){    
            let possibleSets = []; // can only select from sets which actually have an entry for the wanted type!
            for(entry of selector_sourceSets){
                if(entry.setName in gameState){
                    let possibleElements = getPossibleElementsFromSet(gameState[entry.setName], subgroup, convType)
                    //console.log("Possible elements of " + entry.setName);
                    //console.log(possibleElements);
                    if(possibleElements != null && possibleElements != [] && possibleElements.length > 0){
                        possibleSets.push([entry, possibleElements]);
                    }
                }
            }
            //console.log(possibleSets);

            let setChosen = false;
            let personalizedContained = false;
            for(set of possibleSets){
                if(set[0].setName == "generatedPersonalizedUrls"){
                    personalizedContained = true;
                    // handle this set last if a set still has to be chosen!
                }
                else {
                    if(Math.random() < set[0].setChance && !setChosen){ // select set if chance condition is met and none was selected previously
                        //console.log("Choosing from " + set[0].setName);
                        setChosen = true;
                        possibleObjects = set[1];
                    }
                }
            }   
            if(!setChosen && personalizedContained){
                //console.log("Choosing from personalized URLs")
                for(set of possibleSets){
                    if(set[0].setName == "generatedPersonalizedUrls"){
                        possibleObjects = set[1];
                    }
                }
            }
        }

        if (possibleObjects.length == 0) {
            console.log("Getting from predefined")
            possibleObjects = getPossibleElementsFromSet(predefinedUrls, subgroup, convType);
        }
 
        return possibleObjects[utils.generateRandomIntBetween(0, possibleObjects.length-1)];
    }

    function usePersonalizedData(){
        return !selector_usePredefined && window.hasOwnProperty("gameState") && MTLG.getOptions().pi_enabled;
    }

    function getPossibleElementsFromSet(set, subgroup, type){
        let possibleObjects = [];

        if(set == null || !(subgroup in set)){
            console.log("null or subgroup missing")
            return possibleObjects;
        }

        console.log("set[subgroup]:")
        console.log(set[subgroup])

        for(elem of set[subgroup]){
            for(innerElem of elem){
                var appliedRulesCategories = new Array();
                for(rule of innerElem.appliedRules){
                    appliedRulesCategories.push(rule.category)
                }
                if(appliedRulesCategories.includes(type)){
                    possibleObjects.push(innerElem);
                }
            }
        }
        
        return possibleObjects;
    }

    function convertType(type){
        if(type == "no-Phish"){
            return "no_phish";
        }
        if(type == "subDomain"){
            return "sub_domain"
        }
        if(type == "regDomain"){
            return "registrable_domain";
        }
        if(type == "path"){
            return "path";
        }   
        if(type == "ip"){
            return "ip_address";
        }
        if(type == "random"){
            return "random";
        }
        return "";
    }

    return {
        // generateUrl : generateUrl,
        getUrlForType : getUrlForType
    }
})();

