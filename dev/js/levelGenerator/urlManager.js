/**
 * Module for generating phishing URLs.
 * Expected functionality: Has to have a function generateUrl(type, url) like specified below.
 * @module urlManager
 */

 /**
 * An object containing URL data. May have additional properties.
 * 
 * @typedef {Object} urlObject
 * 
 * @property {String} name - Name of the service associated with the URL.
 * @property {String} url - URL under which the service can be accessed.
 */

let urlManager = (function(){
    // Variables and settings for the Creation Game
    let defaultUrls = [
        {
            "name": "Google.com",
            "url": "https://www.google.com/"
          },
          {
            "name": "Youtube.com",
            "url": "https://www.youtube.com/"
          },
          {
            "name": "Google.de",
            "url": "https://www.google.de/"
          },
          {
            "name": "Amazon.de",
            "url": "https://www.amazon.de/"
          },
          {
            "name": "Wikipedia",
            "url": "https://en.wikipedia.org/"
          },
          {
            "name": "eBay Kleinanzeigen",
            "url": "https://www.ebay-kleinanzeigen.de/"
          },
          {
            "name": "Facebook.com",
            "url": "https://www.facebook.com/"
          },
          {
            "name": "Twitch.tv",
            "url": "https://www.twitch.tv/"
          },
          {
            "name": "Gmx.net",
            "url": "https://www.gmx.net/"
          },
          {
            "name": "Web.net",
            "url": "https://web.de/"
          },
          {
            "name": "Vk.com",
            "url": "https://www.vk.com/"
          },
          {
            "name": "Yahoo.com",
            "url": "https://www.yahoo.com/"
          },
          {
            "name": "Netflix.com",
            "url": "https://www.netflix.com/"
          },
          {
            "name": "T-online.de",
            "url": "https://www.telekom.com/"
          },
          {
            "name": "Live.com",
            "url": "https://login.live.com/"
          },
          {
            "name": "Bild.de",
            "url": "https://secure.mypass.de/"
          },
          {
            "name": "Bild.de",
            "url": "https://bild.de/"
          },
          {
            "name": "Spiegel.de",
            "url": "https://www.spiegel.de/"
          },
          {
            "name": "Microsoft.com",
            "url": "https://www.microsoft.com/"
          },
          {
            "name": "Otto.de",
            "url": "https://www.otto.de/"
          },
          {
            "name": "Wetter.com",
            "url": "https://www.wetter.com/"
          },
          {
            "name": "Wetter.com",
            "url": "https://interaction.7pass.de/"
          },
          {
            "name": "Mobile.de",
            "url": "https://login.mobile.de/"
          },
          {
            "name": "Zdf.de",
            "url": "https://www.zdf.de/"
          },
          {
            "name": "Fandom.com",
            "url": "https://www.fandom.com/"
          },
          {
            "name": "Amazon.com",
            "url": "https://www.amazon.com/"
          },
          {
            "name": "Idealo.de",
            "url": "https://www.idealo.de/"
          },
          {
            "name": "Focus.de",
            "url": "https://www.focus.de/"
          },
          {
            "name": "Zeit.de",
            "url": "https://meine.zeit.de/"
          },
          {
            "name": "PayPal",
            "url": "https://www.paypal.com/"
          },
          {
            "name": "runescape 3979",
            "url": "https://secure.runescape.com/"
          },
          {
            "name": "steam",
            "url": "https://store.steampowered.com/"
          },
          {
            "name": "deutsche-bank.de",
            "url": "https://meine.deutsche-bank.de/"
          },
          {
            "name": "commerzbank.de",
            "url": "https://www.commerzbank.de/"
          },
          {
            "name": "dropbox.com",
            "url": "https://www.dropbox.com/"
          },
          {
            "name": "icloud.com",
            "url": "https://www.icloud.com/"
          },
          {
            "name": "onedrive",
            "url": "https://onedrive.live.com/"
          }
   ];
   let personalizedUrls = [];

   function resetUrls(){
       personalizedUrls = [];
   }

   // Warnungen / Fehler zurückgeben 
   // Function to set the array containing the personalized URLs
   // Resets the array and rewrites it to contain the new data (if fitting information is contained the passed userInfo-object)
   // return value: true if the array was rewritten to now contain the new data
   function personalizeUrls(websiteData){
       var personalized = false;
       if(Array.isArray(websiteData) && websiteData.length > 0) {
           for(let page of userInfo.websites){
               // check if the contained element has all the required parameters
               if('name' in page && 'url' in page){
                   // if fitting data is found, set return value to true and empty the array 
                   if(!personalized){
                       personalized = true;
                       resetUrls();
                   }
                   // add the found data object to the array
                   personalizedUrls.push(page);
               }
           }
       }
   }

   function wasPersonalized(){
       return personalizedUrls.length > 0;
   }

   /**
    * Randomly selects a service from the learner profile / the default values and returns it. 
    * 
    * @memberof module:urlManager
    * @returns {urlObject} - The selected service. 
    */
   function getUrl(){
       if(wasPersonalized()){
           return personalizedUrls[utils.generateRandomIntBetween(0,personalizedUrls.length-1)];
       }
       else {
           return defaultUrls[utils.generateRandomIntBetween(0,defaultUrls.length-1)];
       }
   }

   return {
       getUrl : getUrl,
       personalizeUrls : personalizeUrls,
       resetUrls : resetUrls
   }
})();

if (typeof exports !== 'undefined') {
    module.exports = urlManager;
}
