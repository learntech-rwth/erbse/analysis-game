function romanFigure({ width = 300, height = 706, x = 410, y = 700 } = {}) {
    const figure = createPicture(
      MTLG.assets.getBitmap("img/roman_noshield_nogrin.png"),
      width,
      height
    );

    figure.x = x;
    figure.y = y;
    figure.scaleX = -figure.scaleX;

    return figure;
}

function generalFrameSettings( { background, navigate } ) {
  this.background = new backgroundView(background.container);

  const menuButtonOption = {
    dimension: { x: 192, y: 114 },
    text: MTLG.lang.getString("backToMenu"), id: 'backToMenu',
    facing: 1, add: false,
    handler () {
      _logger.log("Player clicked on menu button");
      navigate.menu();
    }
  };
  this.sign = new signButtonView(menuButtonOption);
  background.container.addChild(this.sign);
}

function drawExplanationSlide({ textID, url, navigate: { next, previous, repeat, menu } = {}, background } = {}) {
  const identifier = textID;
  const text = MTLG.lang.getString(textID);

  if (!background.frame) {
    background.frame = new generalFrameSettings( { background, navigate: { menu } } );
  }

  const stage = MTLG.getStageContainer();
  this.stage = stage;
  _logger.log(`DrawExplanationSlide: Showing PresentationMode1 with text "${identifier}"`)

  const speechbubbleDimensions = { width: 960, height: 574, x: 960, y:343 };
  const speechbubble = new speechbubbleView(text, speechbubbleDimensions);

  speechbubble.on("mouseover", function () {
    _logger.log(`Mouseover Speechbubble "${identifier}"`)
  });

  speechbubble.on("mouseout", function () {
    _logger.log(`Mouseout Speechbubble "${identifier}"`)
  });

  const roman = romanFigure();
  stage.addChild(roman);

  this.sectionSpecificChildren = [
    speechbubble,
    roman
  ]

  // add array to save wrappers of eventlisteners on createjs.Ticker
  this.tickerArray = [];
  // function to empty ticker array
  function removeListener(){
    for(wrapper of this.tickerArray){
      createjs.Ticker.off("tick", wrapper);
    }
    this.tickerArray = [];
  }

  if (url) {
    _logger.log(`BridgeLevel: Showing url`, url.url);
    const urlHighlight = urlHighlighter.bind(this)({
      urlView: { ...url },
      position: { centerX: MTLG.getOptions().width * 0.55, centerY: 700 }
    });
    this.sectionSpecificChildren = this.sectionSpecificChildren.concat(urlHighlight);
  } else {
    console.log("No URL to display!");
  }

  if (next) {
    const button = new signButtonView({
      dimension: { x: 1670, y: 983 },
      text: MTLG.lang.getString("continue"), id: "continue",
      handler () {
        removeListener();
        next();
      }
    });
    this.sectionSpecificChildren.push(button);
  }

  if (previous) {
    const button = new signButtonView({
      dimension: { x: 165, y: 983 },
      text: MTLG.lang.getString("back"), id: "back",
      facing: 1,
      handler () {
        removeListener();
        previous();
      }
    });
    this.sectionSpecificChildren.push(button);
  }

  //repeat button, only visible at the end of evaluation
  if (repeat) {
    const button = new signButtonView({
      dimension: { x: 1200, y: 983 },
      text: MTLG.lang.getString("repeatLevel"), id: "repeatLevel",
      facing: 1,
      handler () {
        removeListener();
        repeat();
      }
    });
    this.sectionSpecificChildren.push(button);
  }
}
